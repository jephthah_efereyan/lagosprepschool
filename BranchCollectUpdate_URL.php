<?php
session_start();
ob_start();
$transact_no = $_REQUEST['transact_no']; //transaction no
$rcd = $_REQUEST['rcd']; //response code
$final_rcd = $_REQUEST['final_rcd']; //final response code (for successful transactions returns- 100)
$message = $_REQUEST['message']; //response code description
$final_message = $_REQUEST['final_message']; // final response code description (for successful transactions returns- Payment Successfully Made)
$pay_ref = $_REQUEST['pay_ref']; //payment reference no
$bank_name = $_REQUEST['bank_name']; // bank name where payment was made or is to be made
$bctoken = $_REQUEST['bctoken']; // use for transaction validation
$bc_url = $_REQUEST['bc_url']; // BranchCollect redirect url

//connects to your database
require("admin_online1/config.php"); // this is your database connection file, you can manipulate

if ($final_rcd == "100") {
    //confirm if the transaction is coming from PayChoice before updating the DB
    $TransactionID = $transact_no;
    $MerchantID = '113';
    $MerchantCode = "UPLD";
    $salt = "$MerchantID|$MerchantCode|$TransactionID";
    $MAC = hash('sha512', $salt);
    $string = <<<XML
<?xml version="1.0"  encoding="UTF-8"?> 
<BranchCollectRequest>
<RequestDetails>
     <Merchant MerchantID="$MerchantID" MerchantCode="$MerchantCode" TransactionID="$TransactionID" MAC="$MAC" />
</RequestDetails>
</BranchCollectRequest>
XML;

    $client = new SoapClient(NULL, array('location' => "http://www.branchcollect.com/pay/", 'uri' => "http://branchcollect.com/pay"));
    $params = array('XMLRequest' => $string);
    $returnedXML = $client->__soapCall("PaidTransactionDetails", $params);
    $xmlObject = simplexml_load_string($returnedXML);
    $respCode = $xmlObject->RespCode;
    if ($respCode == "00") {//update your database here
        $client_resp = 1;
        $sql = "UPDATE transactiontotal SET trans_custom1 = 'Paid' WHERE trans_no = '{$transact_no}'";
        if (mysql_query($sql)) {
            $sql = "UPDATE transaction SET trans_custom1 = 'Paid' WHERE trans_no = '{$transact_no}'";
            mysql_query($sql);
        }
        $client_resp = 1; //Successful
    }
    else
        $client_resp = 0;
    header("Refresh: 0; URL=$bc_url?transact_no=$transact_no&rcd=$rcd&final_rcd=$final_rcd&client_resp=$client_resp");
    exit;
}
