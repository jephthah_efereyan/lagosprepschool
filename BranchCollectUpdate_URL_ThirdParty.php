<?php
$transact_no = $_REQUEST['transact_no']; //transaction no
$rcd = $_REQUEST['rcd']; //response code
$final_rcd = $_REQUEST['final_rcd']; //final response code (for successful transactions returns- 100)
$message = $_REQUEST['message']; //response code description
$final_message = $_REQUEST['final_message']; // final response code description (for successful transactions returns- Payment Successfully Made)
$pay_ref = $_REQUEST['pay_ref']; //payment reference no
$bank_name = $_REQUEST['bank_name']; // bank name where payment was made or is to be made
$bctoken = $_REQUEST['bctoken']; // use for transaction validation
$ref_token = $_REQUEST['ref_token'];// ref_code;md5 of the transaction Id
$bc_url = $_REQUEST['bc_url']; // BranchCollect redirect url
$request_ip = $_SERVER['REMOTE_ADDR'];

//do database connection here
require("admin/config.php");

//before you allow access to this script, check the remote server address, is it coming from branchcollect?
$TransactionID = $transact_no;
$MerchantID = '120';
$MerchantCode = "LPS";
$salt = "$MerchantID|$MerchantCode|$TransactionID";
$MAC = hash('sha512', $salt);
$string = <<<XML
<?xml version="1.0"  encoding="UTF-8"?> 
<BranchCollectRequest>
<RequestDetails>
     <Merchant MerchantID="$MerchantID" MerchantCode="$MerchantCode" TransactionID="$TransactionID" MAC="$MAC" />
</RequestDetails>
</BranchCollectRequest>
XML;

$client = new SoapClient(NULL, array('location' => "http://www.branchcollect.com/pay/", 'uri' => "http://branchcollect.com/pay"));
$params = array('XMLRequest' => $string);
$returnedXML = $client->__soapCall("OnePaidTransaction", $params);
$xmlObject = simplexml_load_string($returnedXML);
$respCode = $xmlObject->RespCode;
if ($respCode == "00") {//update your database here
    $sql = "UPDATE transactions SET trans_status = 'Paid' WHERE trans_no = '{$transact_no}'";
    mysql_query($sql);
    $client_resp = 1;
}
else
    $client_resp = 0;

print $client_resp;
