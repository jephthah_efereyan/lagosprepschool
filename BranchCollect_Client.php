<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 10/27/2015
 * Time: 7:59 AM
 */

class BranchCollect_Client {
    protected $client;
    static protected $environment = "live";//test | live
    public $paymentURL;
    protected $testURL = "http://www.branchcollect.com/branchcollect_test/pay/?wsdl";
    protected $liveURL = "http://www.branchcollect.com/pay/?wsdl";
    //protected $liveURL="http://192.163.192.234/pay/?wsdl";
    protected $TransactionID;
    public $RespCode;
    public $Message;
    protected $error = FALSE;
    protected $errorInfo;


    function __construct()
    {
        $this->paymentURL = self::$environment == "test" ? $this->testURL : $this->liveURL;
        try {
            $this->client = new SoapClient($this->paymentURL);
        } catch (Exception $e) {
            $this->error = TRUE;
            $this->errorInfo = $e->getMessage();
            die("Connect Error -> " . $this->errorInfo);
        }
    }


    function logPayment($string)
    {
        $params = array('XMLRequest' => $string);
        try {
            $objectresult = $this->client->__soapCall("GenerateTransactionTP", $params);
            $result = new SimpleXMLElement($objectresult);
            $this->TransactionID = $result->TransactionID;
            $this->RespCode = $result->RespCode;
            $this->Message = $result->Message;

            return $result;
        } catch (Exception $e) {
            $this->error = TRUE;
            $this->errorInfo = $e->getMessage();
            die("Log Error -> " . $this->errorInfo);
        }
    }
}