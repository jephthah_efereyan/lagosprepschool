<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Selfmade Celeb</title>
<link rel="icon" href="images/icon.jpg" />
<link rel="stylesheet" type="text/css" href="selfmade.css"/>
<link rel="stylesheet" type="text/css" href="slicebox.css"/>
<link rel="stylesheet" type="text/css" href="custom.css"/>
<script type="text/javascript" src="modernizr.custom.46884.js"></script>

<!--<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
<script type="text/javascript" src="bootstrap.min.js"></script>-->
<script type="text/javascript" src="jquery.js"></script>

<script type="text/javascript" src="jquery.carouFredSel-6.1.0-packed.js"></script>

<script type="text/javascript">
		$(function() {

			$('#carousel').carouFredSel({
				width: 670,
				items: 4,
				scroll: 1,
				auto: {
					
					duration: 5000,
					timeoutDuration: 2500
				},
				prev: '#prev',
				next: '#next',
				pagination: '#pager'
			});

		});
</script>
</head>

<body>
	<div class="top-head">
    	<header>
        	<img src="images/SelfMadeCeleb_logo.png" />
           <!-- <div id="newsletter">
            <h4><small>SUBSCRIBE TO NEWSLETTER</small></h4> 
            	<form>
	                <input type="text" placeholder="enter your email" />
                    <input type="submit" value="GO" />
                </form>
            </div>-->
<!--            <img src="images/fb.jpg" width="36" height="36" /><img src="images/twt.jpg" width="36" height="36" />
-->        </header>
    </div>
    <div class="nav-bg">
    	<nav>
        	<ul>
            	<li><a href="#">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Partners</a></li>
                <li><a href="gallery.php">Gallery</a></li>
                <li><a href="event.php">Events</a></li>   
                <li><a href="#">Contact</a></li>

            </ul>
        </nav>
    </div>

    <div class="container">
        <div class="row-1">
        
        	<div class="wrapper">
            	<ul id="sb-slider" class="sb-slider">
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
						<!--<div class="sb-description">
							<h3>Creative Lifesaver</h3>
						</div>-->
					</li>
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
					</li>
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
					</li>
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
					</li>
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
					</li>
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
					</li>
					<li>
						<a href="#" target="_blank"><img src="images/slide_img.jpg" /></a>
					</li>
				</ul>


				<!--<div id="nav-arrows" class="nav-arrows">
					<a href="#">Next</a>
					<a href="#">Previous</a>
				</div>-->

				<div id="nav-dots" class="nav-dots">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
            </div>        
                
            <div class="partners">
            	<div id="carousel">
                	<img src="images/hypertek.png" />
                    <img src="images/guiness.png" />
                    <img src="images/pz.png" />
                    <img src="images/total.png" />
                    <img src="images/hypertek.png" />
                    <img src="images/total.png" />
				</div>
                <a id="prev" href="#"></a>
                <a id="next" href="#"></a>
            </div>
        </div>
        <img id="shadow" src="images/shadow-border.jpg" />
        
        <div class="row-2">
            <h2><small>ACTIVITY</small></h2>
                <div class="activity">
                    <h3><small>SELFMADE EDUCATION SUPPORT PROJECT</small></h3>
                	<div id="news">
                            <div id="edu-project">
                                <img src="images/SelfMadeCeleb_edu_project.png" /> 
                                <p>In publishing and graphic design, lorem ipsum is a filler text commonly used to demonstrate the graphic elements of a  document or visual presentation. Replacing meaningful content that could be distracting with placeholder text may  ...</p>
                             </div>
                             <div id="edu-project">
                                <img src="images/hypertek.png" /> 
                                <p>In publishing and graphic design, lorem ipsum is a filler text commonly used to demonstrate the graphic elements of a  document or visual presentation. Replacing meaningful content that could be distracting with placeholder text may  ...</p>
                             </div>
                            
<!--end of news div--> </div>    
                </div><!--end of activity div-->
                            
            
            <div class="ceo">
            <img src="images/ceo.png" /> <p>In publishing and graphic design, lorem ipsum is a filler text commonly used to demonstrate the graphic elements of a  document or visual presentation. Replacing meaningful content that could be distracting with placeholder text may  ...</p>
            </div>
        </div><!--end of row-2 div-->
    </div><!--end of container div-->
    <footer><p>Copyright &copy; 2015</p></footer>
    
    
    
    <!----************************this script here handles the activity div of the website*************-->
    <script type="text/javascript">
			$(function() {
				$('#news').carouFredSel({
					items: 1,
					scroll: {
						duration: 700,
						timeoutDuration: 2000,
				}
				});
			});
		</script>
        
        <script type="text/javascript" src="jquery.slicebox.js"></script>
		<script type="text/javascript">
			$(function() {

				var Page = (function() {

					var $navArrows = $( '#nav-arrows' ).hide(),
						$navDots = $( '#nav-dots' ).hide(),
						$nav = $navDots.children( 'span' ),
						$shadow = $( '#shadow' ).hide(),
						slicebox = $( '#sb-slider' ).slicebox( {
							onReady : function() {

								$navArrows.show();
								$navDots.show();
								$shadow.show();

							},
							onBeforeChange : function( pos ) {

								$nav.removeClass( 'nav-dot-current' );
								$nav.eq( pos ).addClass( 'nav-dot-current' );

							}
						} ),
						
						init = function() {

							initEvents();
							
						},
						initEvents = function() {

							// add navigation events
							$navArrows.children( ':first' ).on( 'click', function() {

								slicebox.next();
								return false;

							} );

							$navArrows.children( ':last' ).on( 'click', function() {
								
								slicebox.previous();
								return false;

							} );

							$nav.each( function( i ) {
							
								$( this ).on( 'click', function( event ) {
									
									var $dot = $( this );
									
									if( !slicebox.isActive() ) {

										$nav.removeClass( 'nav-dot-current' );
										$dot.addClass( 'nav-dot-current' );
									
									}
									
									slicebox.jump( i + 1 );
									return false;
								
								} );
								
							} );

						};

						return { init : init };

				})();

				Page.init();

			});
		</script>
</body>
</html>