<?php
include("header.php");
?>

    <nav>
        <ul class="nav nav-justified">
            <li><a href="index.php">Home</a></li>
            <li class="active"><a href="about.php">About</a></li>
            <li><a href="#">Courses</a></li>
            <li><a href="gallery.php">Gallery</a></li>
            <li><a href="#">Academic Staff</a></li>
            <li><a href="#">Students</a></li>
        </ul>
    </nav>


    <div class="inside-bg">
        <h3>About Lagos Preparatory School</h3>
        <hr/>
        <p><img src="imgs/watch-us.png"/>In publishing and graphic design, lorem ipsum is a filler text commonly used to
            demonstrate the graphic elements of a document or visual presentation. Replacing meaningful content that
            could be distracting with placeholder text may allow viewers to focus on graphic aspects such as font,
            typography, and page layout. It also reduces the need for the designer to come up with meaningful text, as
            they can instead use hastily generated lorem ipsum text.<br/><br/>

            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
            nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
            anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit
            esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
            qui officia deserunt mollit anim id est laborum.</p>
    </div>

    <!--<div class="row" id="footer-bar">
        <div class="col-sm-3 col-md-3">
            <div class="external-links">
                <h3>External Links</h3>
                <ul>
                    <li><a href="#">Abuja Portal</a></li>
                    <li><a href="#">Nigerian law School, Abuja</a></li>
                    <li><a href="#">Law School Alumni</a></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="academic">
                <h3>Academic</h3>
                <ul>
                    <li><a href="#">Academic Calendar</a></li>
                    <li><a href="#">Course Catalogue</a></li>
                    <li><a href="#">Curriculum</a></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="student">
                <h3>Student</h3>
                <ul>
                    <li><a href="#">Student Associations</a></li>
                    <li><a href="#">Departments</a></li>
                    <li><a href="#">Student Journals</a></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="apply">
                <h3>Apply</h3>
                <ul>
                    <li><a href="#">Register Online</a></li>
                    <li><a href="#">Bar I Application</a></li>
                    <li><a href="#">Bar II Application</a></li>
                </ul>
            </div>
        </div>
    </div>-->

    <footer>
        <div class="row foot">
            <div class="col-sm-8 col-md-8"><p>&copy;Lagos Preparatory School, Ikoyi, Lagos. All rights reserved</p></div>
            <div class="col-sm-4 col-md-4"><p>Designed by Upperlink</p></div>
        </div>
    </footer>

<?php
include("footer.php");
?>