<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 6/7/2016
 * Time: 6:12 PM
 */

require("header_leftnav.inc.php");
include_once '../autoload.php';

$msg = "";

if ($_POST) {
    try {
        $_SESSION['profile'] = $_POST;

        if (empty($_POST['id'])) {
            $admin = Administrator::create($connect, $_POST);
            if ($admin) {
                unset($_SESSION['profile']);
                $msg = "Administrator added successfully";
            }
        } else {
            if (!empty($_POST['new_password'])) {
                if ($_POST['new_password'] != $_POST['conf_new_password'])
                    throw new  Exception("New passwords do not match");

                $_POST['new_password'] = md5($_POST['new_password']);
            }

            $admin = Administrator::updateAdmin($connect, $_POST);
            if ($admin) {
                unset($_SESSION['profile']);
                $msg = "Administrator updated successfully";
            }
        }
    } catch(Exception $e) {
        $msg = $e->getMessage();
    }
}

if (!empty($_GET['edit'])) {
    $id = intval($_GET['edit']);
    $admin = Administrator::get($connect, $id);
}
?>
<script type="text/javascript" src="../js/jquery.validation/jquery.validate.min.js"></script>
<style>
    form.cmxform label.error, label.error {
        /* remove the next line when you have trouble in IE6 with labels in list */
        color: red;
        font-style: italic
    }

    div.error {
        display: none;
    }

    input:focus {
        border: 1px dotted black;
    }

    input.error {
        border: 1px dotted red;
    }

    form.cmxform .gray * {
        color: gray;
    }

    input:disabled {
        border: 1px dotted orange;
        color: yellow;
    }

    .text-error {
        patding: 5px;
        font-size: 14px;
        color: white;
        background-color: red;
        margin: 10px;
    }

    .text-success {
        patding: 5px;
        font-size: 14px;
        color: white;
        background-color: green;
        margin: 10px;
    }
</style>
<td valign="top" class="page-content">
    <h1 class="title">Administrator Profile</h1>

    <?php
    if (!empty($msg))
        echo "<div class='msg'><p>{$msg}<p></div>";
    ?>

    <form method="post" name="profile">
        <table class="list">
            <tr>
                <th>Surname</th>
                <td><input name="surname" required="required"
                           value="<?php print @$admin->surname ?>"></td>
            </tr>
            <tr>
                <th>First Name</th>
                <td><input name="firstname" required="required"
                           value="<?php print @$admin->firstname ?>"></td>
            </tr>
            <tr>
                <th>Email</th>
                <td><input type="email" name="email" required="required"
                           value="<?php print @$admin->email ?>"></td>
            </tr>
            <?php if (empty($_GET['edit'])): ?>
                <tr>
                    <th>Password</th>
                    <td>Same as email</td>
                </tr>
            <?php else: ?>
                <tr>
                    <th colspan="2">
                        <br>Reset Password
                        <input type="hidden" name="id" value="<?php print$_GET['edit'] ?> ">
                    </th>
                </tr>
                <tr>
                    <th>New Password</th>
                    <td><input type="password" name="new_password"></td>
                </tr>
                <tr>
                    <th>Confirm New Password</th>
                    <td><input type="password" name="conf_new_password"></td>
                </tr>
            <?php endif; ?>
        </table>
        <br><br>
        <button type="submit">Save</button>
    </form>
</td>
<?php require("footer.php"); ?>

<script>
    $(function () {
        $(profile).validate(
            {
                rules: {
                    "email": {
                        required: false,
                        email: true
                    },
                    "surname": {
                        required: true
                    },
                    "firstname": {
                        required: true
                    }
                }
            });
    })
</script>

</body>
</html>
