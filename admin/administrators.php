<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 6/7/2016
 * Time: 2:11 PM
 */

require("header_leftnav.inc.php");
include_once '../autoload.php';

if (!empty($_GET['activate'])) {
    $_GET['activate'] = intval($_GET['activate']);
    Administrator::activateAdmin($connect, $_GET['activate']);
}

$administrators = Administrator::getAll($connect);
?>

<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">
<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>
<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<style>
    #myAdmins a {
        color: blue;
    }
</style>
<td valign="top" class="page-content">
    <h1 class="title">Administrators</h1>

    <a href="administrator-add.php" style="color:blue">Add Administrator</a>
    <br><br>

    <table id="myAdmins" style="background-color:transparent">
        <thead>
        <tr>
            <th align="right">#</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Active</th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
        foreach ($administrators as $i => $administrator):
            $edit_link = '<a href="administrator-add.php?edit=' . $administrator->id . '">Edit</a>';
            
            $activate_link = '';
            if ($administrator->role_id > 1) {
                $activate_link = ' :: <a href="?activate=' . $administrator->id . '">' . ($administrator->active ? 'Deactivate' : 'Activate') . '</a>';
            }
            ?>
            <tr>
                <td align="right"><?php print $i + 1 ?></td>
                <td><?php print $administrator->surname . ", " . $administrator->firstname ?></td>
                <td><?php print $administrator->email ?></td>
                <td><?php print $administrator->role ?></td>
                <td><?php print $administrator->active? 'Yes' : 'No' ?></td>
                <td><?php print $edit_link . $activate_link ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
<script>
    $(document).ready(function () {
        $('#myAdmins').dataTable({
            //dom: 'Bfrtip', buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print' ]
        });
    });
</script>
</body>
</html>
