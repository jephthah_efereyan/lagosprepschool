<?php
include_once 'config.php';
if($_POST){
	include_once '../autoload.php';
	$students		= Student::getAll($connect,$_POST['termID']);
	$billingItems	= Bills::listItems($connect);
	
	if($students and $billingItems){
		
		//var_dump($billingItems);
		
		error_reporting(E_ALL);
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);
		date_default_timezone_set('Europe/London');
		
		if (PHP_SAPI == 'cli')
			die('This example should only be run from a Web Browser');
		
		
		require_once dirname(__FILE__) . '/../PHPExcel/Classes/PHPExcel.php';
		
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Upperlink Limited")
		->setLastModifiedBy("Maarten Balliauw")
		->setTitle("Office 2007 XLSX Test Document");
		
			
	
		//do first row /headers
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueByColumnAndRow( 0,1, 'Admission No');
		$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueByColumnAndRow( 1,1, 'Name');
		$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('B')->setAutoSize(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueByColumnAndRow( 2,1, 'Invoice No');
		$objPHPExcel->setActiveSheetIndex(0)->getColumnDimension('C')->setAutoSize(true);
		
		for ($i = 0; $i<count($billingItems); $i++){
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow( $i+3,1, $billingItems[$i]['bi_name']);
			$objPHPExcel->setActiveSheetIndex(0)->getColumnDimensionByColumn($i+3)->setAutoSize(true);
		}
		
		//do contents
		for ($i =  0; $i<count($students); $i++){
			$name = "{$students[$i]['lastname']} {$students[$i]['firstname']} {$students[$i]['othernames']}";
			$admission_no = $students[$i]['admission_no'];
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueExplicitByColumnAndRow( 0,$i+2, $admission_no,PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow( 1,$i+2, $name);
				
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Students List');
		
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Student-bills.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		//header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit; 
		
	}
}

