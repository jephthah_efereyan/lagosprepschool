<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 11:10 AM
 */

require("header_leftnav.inc.php");

$bi_name = "";
$label = "Add Item";
$cancel = "";
$action = "";

if (isset($_POST['save_billing_item'])) {
    $_POST['bi_name'] = trim($_POST['bi_name']);
    $_SESSION['billing_items'] = $_POST;

    $sql = "SELECT * FROM billing_items WHERE bi_name = '{$_POST['bi_name']}'";
    if (!empty($_GET['biID']))
        $sql .= " AND bi_id != " . (int)$_GET['biID'];
    $resource = mysql_query($sql);
    if (mysql_num_rows($resource) > 0)
        $msg = "<b>{$_POST['bi_name']}</b> already exists";
    else {
        if (isset($_GET['action']) && $_GET['action'] == "edit_bi" && !empty($_GET['biID'])) {
            $resource = mysql_query("SELECT bi_name FROM billing_items WHERE bi_id = " . (int)$_GET['biID']);
            $old_bi_name = mysql_result($resource, 0);

            $sql = "UPDATE billing_items SET bi_name = '{$_POST['bi_name']}' WHERE bi_id = " . (int)$_GET['biID'];
        }
        else
            $sql = "INSERT INTO billing_items (bi_name, bi_added_on, bi_is_enabled) VALUE ('{$_POST['bi_name']}', NOW(), TRUE)";
        mysql_query($sql) or die(mysql_error());
        if (mysql_affected_rows() > 0) {
            unset($_SESSION['billing_items']);
            if (isset($_GET['action']) && $_GET['action'] == "edit_bi")
                $msg = "<b>{$old_bi_name}</b> changed to <b>{$_POST['bi_name']}</b> successfully";
            else
                $msg = "<b>{$_POST['bi_name']}</b> added successfully";
        }
    }
}

    if (isset($_GET['action']) && $_GET['action'] == 'enable_bi' && !empty($_GET['biID'])) {
    $sql = "UPDATE billing_items SET bi_is_enabled = IF (bi_is_enabled = TRUE, FALSE, TRUE) WHERE bi_id = " . (int)$_GET['biID'];
    mysql_query($sql);
    if (mysql_affected_rows() > 0)
        $msg = "Billing Item status changed successfully";
}

if (isset($_GET['action']) && $_GET['action'] == "edit_bi" && !empty($_GET['biID'])) {
    $resource = mysql_query("SELECT * FROM billing_items WHERE bi_id = {$_GET['biID']}");
    $this_billing_items = mysql_fetch_assoc($resource);
    $bi_name = $this_billing_items['bi_name'];
    $action = "action=edit_bi&biID=" . $this_billing_items['bi_id'];
    $label = "Change Item Name";
    $cancel = "<a href='billing_items.php'  class='a'>Cancel</a>";
}

$sql = "SELECT * FROM billing_items ORDER BY bi_name";
$resource = mysql_query($sql) or die(mysql_error());

if (!empty($_SESSION['billing_items'])) {
    extract($_SESSION['billing_items']);
    unset($_SESSION['billing_items']);
}
?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Billing Items</h1>

                <form method="post" class='form' action="?<?php echo $action ?>">
                    <?php echo $label?>
                    <input type="text" name="bi_name" value="<?php echo $bi_name ?>" required="required" size="50">
                    <button type="submit" name="save_billing_item">Save</button>
                    <?php echo $cancel ?>
                </form>

                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";

                if (mysql_num_rows($resource) > 0) {
                    ?>
                    <table border=0 align=left cellpadding=5 cellspacing=0 class="list">
                        <tr>
                            <th>#</th>
                            <th>Billing Item Name</th>
                            <th>Enabled</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $index = 0;
                        while ($billing_items = mysql_fetch_assoc($resource)) {
                            $bi_is_enabled = $billing_items['bi_is_enabled'] ? "Yes" : "No";
                            $bi_is_enabled_class = $billing_items['bi_is_enabled'] ? "label-success" : "label-danger";
                            $enable = $billing_items['bi_is_enabled'] ? "Disable" : "Enable";
                            ?>
                            <tr>
                                <td><?php echo ++ $index ?></td>
                                <td><?php echo $billing_items["bi_name"] ?></td>
                                <td><span class="label <?php echo $bi_is_enabled_class ?>"><?php echo $bi_is_enabled ?></span></td>
                                <td>
                                    <a href="?action=edit_bi&biID=<?php echo $billing_items['bi_id'] ?>" class="a">Edit</a>
                                    ::
                                    <a href="?action=enable_bi&biID=<?php echo $billing_items['bi_id'] ?>" class="a"><?php echo $enable ?></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php }
                else
                    echo "<p>No billing item found</p>";
                ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>
