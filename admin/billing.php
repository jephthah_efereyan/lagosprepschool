<?php require("header_leftnav.inc.php"); ?>

<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">
	<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
	<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>
	<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="../js/jquery-2.2.3.min.js"></script> -->
<script type="text/javascript" src="../js/jquery.validation/jquery.validate.min.js"></script>
<style>
.text-error{
	padding: 5px;
	font-size: 14px;
	color: white;
	background-color: red;
	margin: 10px;
}
.text-success{
	padding: 5px;
	font-size: 14px;
	color: white;
	background-color: green;
	margin: 10px;
}



form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	color: red;
	font-style: italic
}
div.error { display: none; }
/*input {	border: 1px solid black; }*/
input:focus { border: 1px dotted black; }
input.error { border: 1px dotted red; }
form.cmxform .gray * { color: gray; }
input:disabled {	border: 1px dotted orange; color: yellow; }

	#myTable a{
		color: blue;
	}
</style>
	<td valign="top" class="page-content">
  <a href="pupils-all.php" style="background: blue; padding: 7px;margin:3px; border: gray solid thin">&#60;&#60;&#60; Back to Student List</a>
  <h1>Students' Billing Information</h1>
    <!-- <p>Welcome, please use the navigation links to your left to perform necessary action</p> -->
    
<?php
include_once '../autoload.php';
$student = new Student($connect, $_GET['admissionNo']);

//var_dump(Bills::listItems($connect));

if($student->student_id){
$currentSession		= Sessions::current($connect);
$currentSessionTerm	= Terms::currentSessionTerm($connect);
$session			= @$_GET['session'] | $currentSession;
$SessionTerm		= @$_GET['session_terms'] | $currentSessionTerm;
@$teacherID			=  $_SESSION['user']['id'];
//get bill if session_term_id available
//
if($_POST and !empty($_POST['itemID']) and !empty($_POST['amount']) and  $currentSessionTerm == $SessionTerm){

//var_dump($_POST['itemID'], $_POST['amount']);

	if(Bills::create($connect, $student->student_id, $SessionTerm, $teacherID, $_POST['itemID'], $_POST['amount']))
	{
		print '<div class="text-success">Bill was added  to the database</div>';
	}
	else 
	{
		print '<div class="text-error">Bill was not added</div>';
	} 
}






$sessions		= $student->listSession();
//var_dump($student->listTerm(1));

?>
<p><u>Name</u><br>
<?php print "{$student->lastname} {$student->firstname} {$student->othernames}" ?><br><br>
<u>Class</u><br>
<?php print "{$student->faculties_name} {$student->departments_name} {$student->programme_name}" ?><br>
<u>Current Invoice No</u><br>
<?php print $student->billing_invoice_no? : 'N/A'?>
<br>
</p>
<form name="sessionForm" style="display:inline">
	<input type="hidden" name="admissionNo" value="<?php print @$_GET['admissionNo']; ?>" >
	<label>
		Select Session
		<select name="session" onchange="sessionForm.submit()" >
			<option value="<?php print $currentSession?>">Current Session</option>
			<?php foreach ($sessions as $row):
			$select = (@$_GET['session'] == $row['session_id'])?'selected':'';
			?>
			<option value="<?php print $row['session_id']?>" <?php print $select?>><?php print $row['session_name']?></option>
			<?php endforeach; ?>
		</select>
	</label>
</form>
<form style="display:inline">
<input type="hidden" name="admissionNo" value="<?php print @$_GET['admissionNo']; ?>" >
<input type="hidden" name="session" value="<?php print $session ?>" >
	
		<label><b>Term:</b> 
		<select name="session_terms" >
			<option value="<?php print $currentSessionTerm?>">Current Term</option>
			<?php 
			$terms = $student->listTerm($session);

			foreach ($terms as $row):
			$select = (@$_GET['session_terms'] == $row['session_term_id'])?'selected':'';
			?>
			<option value="<?php print $row['session_term_id']?>" <?php print $select?>><?php print $row['term_fullname']?></option>
			<?php endforeach;?>
		</select>
	 
		
		<button type="submit">Show Bill</button>
		
	</label>
</form>

<?php 
$bills = Bills::studentBills($connect, $student->student_id, $SessionTerm);
?>
<form action="" method="post" name="billing">
<p></p>
<table border="1" cellspacing="0">
	<thead>
		<tr>
			<th>#</th>
			<th><input type="checkbox"></th>
			<th>ITEM NAME</th>
			<th>AMOUNT (&#x20a6;)</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	$total = 0;
	$emptyBill = Bills::listItems($connect);
	//$billData = ($bills)? :$emptyBill;
	$disabled = ($currentSessionTerm == $SessionTerm)? '': 'disabled';
	foreach ($emptyBill as $i =>$row):
	
		/* if(!isset($row['amount']) and $bills)
			continue; */
	$amount = Bills::getAmount($row['bi_id'], $bills);
	$checked =(empty($amount))? $checked = '':$checked = 'checked';
		
	$total += $amount; 

			
		
?>
		<tr>
			<td><?php print $i+1 ?></td>
			<td><input type="checkbox" name="itemID[<?php print $i?>]" value="<?php print $row['bi_id'] ?>" <?php print $checked?> <?php print $disabled?>></td>
			<td><?php print $row['bi_name'] ?></td>
			<td><input type="text" name="amount[]" size="10" onchange="checkThis2(this)" value="<?php print $amount?>"  <?php print $disabled?>></td>
		</tr>
	<?php endforeach;?>
	</tbody>
	<tfoot>
		<tr>
			<th align="right" colspan="3">Total</th>
			<th id="total" align="left"><?php print $total ;?></th>
		</tr>
	</tfoot>

</table>
<p></p>
	<?php if($currentSessionTerm == $SessionTerm):?><button type="submit">Save</button><?php endif;?>

</form>
<?php 
	$totalBill		= Bills::studentTotalBills($connect,$student->student_id);
	$totalPayment	= Bills::studentTotalPayment($connect, $student->student_id);
	$balance		= $totalBill - $totalPayment;
	

?>
<h3>Balance:
		<?php print ($balance > 0)? 
			'<span style="color:red">(&#x20a6;'.$balance.')</span>':
			'<span style="color:green">&#x20a6;'.$balance.'</span>';
		?>
</h3>
		<?php 
	 }
else{
	print '<div class="error">No Data available</div>';
}?>
</td>
</tr>
</table>

<script type="text/javascript">
<!--
function checkThis($obj){
	
	$($obj).parent().parent().find('input:checkbox').attr({checked:'checked'});
	
}
function checkThis2($obj){
	var $tr = $($obj).parent().parent();
	var checkbox	= $tr.find('input:checkbox');//;
	if($obj.value == '')
	{
		checkbox.removeAttr('checked');
	}
	else
	{
		checkbox.attr({checked:'checked'});
	}

	$tbody = $tr.parent();
	var totalValue = 0;
	var boxes = $tbody.find('input:text');
	boxes.each(function(){
		var val = $(this).val();
		//alert(parseInt(val));
		if($(this).val() != '')
			totalValue += parseFloat(val);
	})
	
	$("#total").html(totalValue);
}


$(function(){
	jQuery.validator.addMethod(
		    "money",
		    function(value, element) {
		        var isValidMoney = /^\d{0,10}(\.\d{0,2})?$/.test(value);
		        return this.optional(element) || isValidMoney;
		    },
		    "Please enter a valid currency amount"
		);
	$(billing).validate(
			{
		rules: {
			"amount[]": {
				required: false,
				money: true
			}	
		}
		
	
	})
})
//-->
</script>

<?php require("footer.php"); ?>
</body>
</html>
