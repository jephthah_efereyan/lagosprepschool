<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 5/23/2016
 * Time: 9:59 AM
 */

require("header_leftnav.inc.php");
include_once '../autoload.php';

$response = array('status' => false);

if (isset($_POST['upload'])) {
    try {
        if ($_FILES['upload_file']['error'] != 0) {
            throw new Exception('Something wrong with the upload');
        }

        $new_filename = time() . '_bills';
        list($name, $ext) = explode('.', basename($_FILES['upload_file']['name']));
        move_uploaded_file($_FILES['upload_file']['tmp_name'], "uploads/{$new_filename}.{$ext}");

        //  Include PHPExcel_IOFactory
        include '../PHPExcel/Classes/PHPExcel/IOFactory.php';

        $inputFileName = "uploads/{$new_filename}.{$ext}";

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        //get the data in the file
        $dataArray = $objPHPExcel->getActiveSheet()->toArray();

        //get the column headers
        $col_headers = array_shift($dataArray);

        //validate each cell in each row
        $validation_errors = array();

        $billing_items = array();

        //this will generate variables with the names of the column headers and set their values to the column indices
        foreach ($col_headers as $col_index => $col_name) {
            $col_name = strtolower(trim($col_name));
            //$col_headers[$col_index] = $col_name;

            //get the item details from db if the col header is an item
            $item = Bills::getItemByName($connect, $col_name);
            if ($item) {
                $billing_items[] = array('index' => $col_index, 'item_id' => $item->bi_id);
            }

            $col_name_ = str_replace(' ', '__', $col_name);
            ${$col_name_} = $col_index;
        }

        $bills_data = array();
        $invoice_no_data = array();

        //Loop through the bills
        $i = 1;
        foreach ($dataArray as $studentBill) {
            $studentProfile = new Student($connect, $studentBill[$admission__no]);
            if ($studentProfile->student_id) {
                //get the invoice numbers for the students for updating their records
                if (!empty($studentBill[$invoice__no])) {
                    $invoice_no_data[$studentProfile->student_id] = $studentBill[$invoice__no];
                }

                foreach ($billing_items as $item) {
                    $item_amount = trim($studentBill[$item['index']]);
                    if (intval($item_amount) > 0) {
                        $bills_data[] = "({$studentProfile->student_id}, {$_SESSION['current_session_term']['session_term_id']}, {$item['item_id']}, '{$item_amount}', NOW(), {$_SESSION['user']['id']}, {$_SESSION['user']['id']})";
                    }
                }
            }
        }

        $response['status'] = true;

        //save the bills
        if (!empty($bills_data)) {
            $sql = "INSERT IGNORE INTO bills_students (student_id, session_term_id, item_id, amount, added_on, added_by, modified_by) VALUES " . implode(', ', $bills_data);
            mysql_query($sql, $connect) or die(mysql_error());
            $records = mysql_affected_rows();

            if ($records)
                $msg = $records . " records created successfully.";
        }

        //save the invoice numbers
        if (!empty($invoice_no_data)) {
            $sql = "UPDATE studentprofile_extra SET billing_invoice_no = CASE ";
            foreach ($invoice_no_data as $student_id => $invoice_no) {
                $sql .= " WHEN student_id = {$student_id} THEN '{$invoice_no}'";
            }
            $sql .= " END";
            $sql .= " WHERE session_term_id = {$_SESSION['current_session_term']['session_term_id']}";
            mysql_query($sql, $connect) or die(mysql_error() . "<br>" . $sql);
        }

    } catch (Exception $e) {
        if (file_exists($inputFileName))
            unlink($inputFileName);
    }
}

?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Bills - Bulk Upload</h1>
                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";
                ?>

                <form method="post" class='form' action="" enctype="multipart/form-data">
                    <input type="file" name="upload_file">
                    <button type="submit" name="upload">Upload</button>
                </form>
            </td>
        </tr>
    </table>
</td>
</tr>

<?php require("footer.php"); ?>

</body>
</html>

