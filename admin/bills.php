<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 12:26 PM
 */

require("header_leftnav.inc.php");

$bills = array();
$_session_term_id = $_session_id = $_term_id = $_faculty_id = $_department_id = 0;

if (isset($_POST['save_bill'])) {
    $data = array();
    foreach ($_POST['amount'] as $item_id => $item_amount) {
        if (empty($item_amount))
            continue;
        $data[] = "({$_POST['session_term_id']}, {$_POST['faculty_id']}, {$_POST['department_id']}, {$item_id}, '{$item_amount}', NOW(), {$_SESSION['user']['id']}, NOW(), {$_SESSION['user']['id']})";
    }
    $sql = "INSERT INTO bills (session_term_id, faculty_id, department_id, item_id, amount, bill_added_on, bill_added_by, bill_modified_on, bill_modified_by)
            VALUES " . implode(', ', $data) . " ON DUPLICATE KEY UPDATE amount = VALUES(amount)";

    mysql_query($sql);
    if (mysql_affected_rows() > 0)
        $msg = "Bill configured successfully";

    $_session_term_id = $_POST['session_term_id'];
    $session_term = GetSessionTermByID($_session_term_id);
    $_session_id = $session_term['session_id'];
    $_term_id = $session_term['term_id'];
    $_faculty_id = $_POST['faculty_id'];
    $_department_id = $_POST['department_id'];
}

if (isset($_POST['fetch_bill'])) {
    $_session_id = $_POST['session_id'];
    $_term_id = $_POST['term_id'];
    $_faculty_id = $_POST['faculty_id'];
    $_department_id = $_POST['department_id'];

    $session_term = GetSessionTerm($_POST['session_id'], $_POST['term_id']);
    if ($session_term)
        $_session_term_id = $session_term['session_term_id'];
}
if (!empty($_session_term_id))
    $bills = GetBills($_session_term_id, $_faculty_id, $_department_id);
?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Bills</h1>
                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";
                ?>

                <form method="post" class='form' action="">
                    Session:
                    <select name="session_id">
                        <option value="">--Select--</option>
                        <?php
                        $sessions = GetSessions();
                        if (!empty($sessions)) {
                            foreach ($sessions as $session) {
                                echo '<option value="' . $session['session_id'] . '"';
                                echo !empty($_session_id) && $_session_id == $session['session_id'] ? ' selected' : '';
                                echo '>' . $session['session_fullname'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    &nbsp;&nbsp;
                    Term:
                    <select name="term_id">
                        <option value="">--Select--</option>
                        <?php
                        $terms = GetTerms();
                        if (!empty($terms)) {
                            foreach ($terms as $term) {
                                echo '<option value="' . $term['term_id'] . '"';
                                echo !empty($_term_id) && $_term_id == $term['term_id'] ? ' selected' : '';
                                echo '>' . $term['term_fullname'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    &nbsp;&nbsp;
                    Class:
                    <select name="faculty_id">
                        <option value="">--Select--</option>
                        <?php
                        $faculties = GetFaculties();
                        if (!empty($faculties)) {
                            foreach ($faculties as $faculty) {
                                echo '<option value="' . $faculty['faculties_id'] . '"';
                                echo !empty($_faculty_id) && $_faculty_id == $faculty['faculties_id'] ? ' selected' : '';
                                echo '>' . $faculty['faculties_name'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    &nbsp;&nbsp;
                    Level:
                    <select name="department_id">
                        <option value="">--Select--</option>
                        <?php
                        $departments = GetDepartments();
                        if (!empty($departments)) {
                            foreach ($departments as $department) {
                                echo '<option value="' . $department['departments_id'] . '"';
                                echo !empty($_department_id) && $_department_id == $department['departments_id'] ? ' selected' : '';
                                echo '>' . $department['departments_name'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                    &nbsp;&nbsp;
                    <button type="submit" name="fetch_bill">Fetch Bill</button>
                </form>

                <?php
                if (!empty($bills)) {
                    ?>
                    <form name="" method="post">
                        <input type="hidden" name="session_term_id" value="<?php echo $_session_term_id ?>">
                        <input type="hidden" name="faculty_id" value="<?php echo $_faculty_id ?>">
                        <input type="hidden" name="department_id" value="<?php echo $_department_id ?>">
                        <table border=0 align=left cellpadding=5 cellspacing=0 class="list">
                            <tr>
                                <th>#</th>
                                <th>Billing Item Name</th>
                                <th class="num">Amount (&#8358;)</th>
                                <!--<th>Action</th>-->
                            </tr>
                            <?php
                            $index = 0;
                            $total_amount = 0;
                            foreach ($bills as $bill) {
                                /*$bi_is_enabled = $bill['bi_is_enabled'] ? "Yes" : "No";
                                $bi_is_enabled_class = $bill['bi_is_enabled'] ? "label-success" : "label-danger";
                                $enable = $bill['bi_is_enabled'] ? "Disable" : "Enable";*/
                                $total_amount += $bill["amount"];
                                ?>
                                <tr>
                                    <td><?php echo ++$index ?></td>
                                    <td><?php echo $bill["bi_name"] ?></td>
                                    <td><input type="text" name="amount[<?php echo $bill['bi_id'] ?>]"
                                               value="<?php echo $bill["amount"] ?>" class="num"></td>
                                    <!--<td><span class="label <?php /*echo $bi_is_enabled_class */ ?>"><?php /*echo $bi_is_enabled */ ?></span></td>-->
                                    <!--<td>
                                    <a href="?action=edit_bi&biID=<?php /*echo $bill['bi_id'] */ ?>" class="a">Edit</a>
                                    ::
                                    <a href="?action=enable_bi&biID=<?php /*echo $bill['bi_id'] */ ?>" class="a"><?php /*echo $enable */ ?></a>
                                </td>-->
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td>&nbsp;</td>
                                <th>TOTAL</th>
                                <th class="num"><?php echo number_format($total_amount, 2) ?></th>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: center"><button name="save_bill" value="Save Bill">Save Bill</button></td>
                            </tr>
                        </table>
                    </form>
                <?php } else
                    //echo "<p>Bill not configured yet for this class in the specified session and term.</p>";
                ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>
