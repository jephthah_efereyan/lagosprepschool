<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 6/7/2016
 * Time: 4:35 PM
 */

require("header_leftnav.inc.php");
include_once '../autoload.php';

$msg = "";

$administrator = Administrator::get($connect, $_SESSION['user']['id']);

if (!empty($_POST)) {
    try {
        $_SESSION['change_password'] = $_POST;
        if (empty($_POST['old_password']) || empty($_POST['new_password']) || empty($_POST['conf_new_password']))
            throw new Exception('All fields must be filled.');

        if ($_POST['new_password'] !== $_POST['conf_new_password'])
            throw new Exception('New passwords do not match');

        $old_password_hashed = md5($_POST['old_password']);
        $new_password_hashed = md5($_POST['new_password']);

        if (Administrator::changePassword($connect, $new_password_hashed, $old_password_hashed, $_SESSION['user']['id'])) {
            unset($_SESSION['change_password']);
            $msg = "Password changed successfully";
        } else {
            if (! Administrator::validPassword($connect, $old_password_hashed, $_SESSION['user']['id']))
                throw new Exception('Current Password Incorrect');

            throw new Exception('Password change NOT successful');
        }
    } catch (Exception $e) {
        $msg = $e->getMessage();
    }
}

if (!empty($_SESSION['change_password'])) {
    extract($_SESSION['change_password']);
    unset($_SESSION['change_password']);
}
?>

<td valign="top" class="page-content">
    <h1 class="title">Change Password</h1>

    <?php
    if (!empty($msg))
        echo "<div class='msg'><p>{$msg}<p></div>";
    ?>

    <form action="" method="post">
        <table class="list">
            <tr>
                <th>Current Password</th>
                <td><input type="password" name="old_password"
                           value="<?php print @$old_password ?>" required="required"></td>
            </tr>
            <tr>
                <th>New Password</th>
                <td><input type="password" name="new_password"
                           value="<?php print @$new_password ?>" required="required"></td>
            </tr>
            <tr>
                <th>Confirm New Password</th>
                <td><input type="password" name="conf_new_password"
                           value="<?php print @$conf_new_password ?>" required="required"></td>
            </tr>
        </table>
        <br>

        <div>
            <button type="submit" name="change">Change</button>
        </div>
    </form>
</td>
<?php require("footer.php"); ?>

</body>
</html>
