<?php
require("header_leftnav.inc.php");

$msg = "";
$_status = isset($_GET['_status']) ? $_GET['_status'] : 99;

if (!empty($_GET['action'])) {
    //require 'dovalidate.php';
}

$query = "SELECT * FROM classteacher WHERE type != 3";
$query .= $_status != 99 ? " AND status = " . (int)$_status : "";
$query .= " ORDER BY teacherlastname ASC";

$teachers = mysql_query($query);
?>

<td valign="top" class="page-content">
    <h3 class='page-title'>CLASS / DORM TEACHERS</h3>
    <?php
    if (!empty($msg))
        echo "<div class='msg'><p>" . $msg . "</p></div>";
    ?>
    <p><a href="classteacher_edit.php" class="blue">Add Teacher</a></p>

    <table border="0" cellspacing="0" cellpadding="5" class="list">
        <thead>
        <tr bgcolor=#CCCCCC>
            <th>#</th>
            <th>Surname</th>
            <th>First Name</th>
            <th>Class</th>
            <th>
                <select onchange='filter_teachers(this.value)'>
                    <option value='99'>All</option>
                    <?php
                    $valid_status = array("Pending", "Validated");
                    foreach ($valid_status as $key => $value) {
                        echo "<option value='{$key}'";
                        echo $_status == $key ? " selected" : "";
                        echo ">{$value}";
                    }
                    ?>
                </select>
            </th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if (!empty($teachers)) {
            $i = 0;
            while ($teacher = mysql_fetch_assoc($teachers)) {
                $i ++;
                $row_style = $i % 2 == 0 ? 'even' : "odd";
                $teacherclass_level = $teacher["teacherclass_level"];
                $teacherclass_no = $teacher["teacherclass_no"];
                $teacherclass_letter = $teacher["teacherclass_letter"];
                $teacherclass = $teacherclass_level . " " . $teacherclass_no . " " . $teacherclass_letter;
                $status = $teacher["status"] == 0 ? "<span class='red'>Pending</span>" : "<span class='green'>Validated</span>";
                $text = $teacher["status"] == 0 ? "Validate" : "Unvalidate";
                $action = "<a href='?id={$teacher['classteacher_id']}&action=" . strtolower($text) . "' class='blue' onClick=\"return confirmLink(this, '" . strtoupper($text) . " this?')\">{$text}</a> |
                                            <a href='classteacher_edit.php?id={$teacher['classteacher_id']}' class='blue'>Edit</a>";
                echo "<tr class='" . $row_style . "'>
                                        <td>" . $i . "</td>
                                        <td>" . stripslashes($teacher["teacherlastname"]) . "</td>
                                        <td>" . stripslashes($teacher["teacherfirstname"]) . "</td>
                                        <td>" . $teacherclass . "</td>
                                        <td>" . $status . "</td>
                                        <td>" . $action . "</td>
                                    </tr>";
            }
        }
        ?>
        </tbody>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>

<script>
    function filter_teachers(a)
        location.href = "<?php echo $_SERVER['PHP_SELF'] ?>" + "?_status=" + a;
</script>