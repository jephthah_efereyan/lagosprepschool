<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/5/2015
 * Time: 4:25 PM
 */
require("header_leftnav.inc.php");

$teacher_details = array("teacherclass_level", "teacherclass_no", "teacherclass_letter",
                            "teachertitle", "teacherlastname", "teacherfirstname", "teacherothernames",
                            "teacherusername", "teacherpassword", "teacherpassword2", "teacher_email", "teacher_email2",
                            "teacher_phone", "teacher_address", "teacher_city");
foreach ($teacher_details as $detail)
    ${$detail} = "";
$action = "add";

if (isset($_GET['id'])) {
    $teacher_id = $_GET['id'];
    $sql = "SELECT * FROM classteacher WHERE classteacher_id = {$teacher_id}";
    $resource = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($resource) > 0) {
        $teacher = mysql_fetch_assoc($resource);
        extract($teacher);
        $teacherpassword2 = $teacherpassword;
        $teacher_email2 = $teacher_email;
        $action = "edit&id=" . $classteacher_id;
    }
}

$st = isset($_SESSION['st']) ? $_SESSION['st'] : "";
unset($_SESSION['st']);

$msg = $st == "00" ? "Operation successful" : ($st == "1" ? "Operation NOT successful" : "");

?>

<td valign="top" class="page-content">
    <h3 class='page-title'>ADD / EDIT CLASS TEACHER</h3>

    <form name="form" method="post" action="process.php?module=classteacher&action=<?php print $action ?>" id="edit_teacher"
          onsubmit="return ch()">
        <table cellpadding="5" cellspacing="0" class="list">
            <tr>
                <td colspan=2><?php print !empty($msg) ? "<div style='background:#EFEFEF; padding:5px'>{$msg}</div>" : ""; ?></td>
            </tr>

            <tr>
                <td>Class</td>
                <td>
                    <select name='teacherclass_level' id='teacherclass_level'>
                        <option value="">--Select--</option>
                        <?php
                        $query = "SELECT * FROM faculties";
                        $faculties = mysql_query($query) or die(mysql_error());
                        while ($faculty = mysql_fetch_assoc($faculties)) {
                            print "<option value='" . $faculty["faculties_name"] . "'";
                            print $faculty['faculties_name'] == $teacherclass_level ? " selected" : "";
                            print ">" . $faculty["faculties_name"] . "</option>";
                        }
                        ?>
                    </select>

                    <select name='teacherclass_no' id='teacherclass_no'>
                        <option value="">--Select--</option>
                        <?php
                        $query = "SELECT * FROM departments";
                        $departments = mysql_query($query) or die(mysql_error());
                        while ($department = mysql_fetch_assoc($departments)) {
                            print "<option value='" . $department["departments_name"] . "'";
                            print $department["departments_name"] == $teacherclass_no ? " selected" : "";
                            print ">" . $department["departments_name"] . "</option>";
                        }
                        ?>
                    </select>

                    <select name='teacherclass_letter' id='teacherclass_letter'>
                        <option value="">--Select--</option>
                        <?php
                        $query = "SELECT * FROM programme ORDER BY programme_name";
                        $programmes = mysql_query($query) or die(mysql_error());
                        while ($programme = mysql_fetch_assoc($programmes)) {
                            print "<option value='" . $programme["programme_name"] . "'";
                            print $programme["programme_name"] == $teacherclass_letter ? " selected" : "";
                            print ">" . $programme["programme_name"] . "</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Title</td>
                <td>
                    <select name="teachertitle" id="teachertitle">
                        <?php
                        $titles = array("Mr", "Ms", "Mrs");
                        foreach ($titles as $title) {
                            print "<option value='$title'";
                            print $teachertitle == $title ? " selected" : "";
                            print ">{$title}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Surname</td>
                <td><input type="text" name="teacherlastname" id="teacherlastname" size="35" required="required"
                           value="<?php print $teacherlastname ?>"></td>
            </tr>
            <tr>
                <td>First Name</td>
                <td><input type="text" name="teacherfirstname" id="teacherfirstname" size="35" required="required"
                           value="<?php print $teacherfirstname ?>"></td>
            </tr>
            <tr>
                <td>Other Name(s)</td>
                <td><input type="text" name="teacherothernames" id="teacherothernames" size="35"
                           value="<?php print $teacherothernames ?>"></td>
            </tr>
            <tr>
                <td>Username</td>
                <td><input type="text" name="teacherusername" id="teacherusername" size="35" required="required"
                           value="<?php print $teacherusername ?>"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="teacherpassword" id="teacherpassword" size="35" required="required"
                           value="<?php print $teacherpassword ?>"></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="password" name="teacherpassword2" id="teacherpassword2" size="35" required="required"
                           value="<?php print $teacherpassword2 ?>"></td>
            </tr>
            <tr>
                <td>E-mail</td>
                <td><input type="email" name="teacher_email" id="teacher_email" size="35" required="required"
                           value="<?php print $teacher_email ?>"></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td><input type="text" name="teacher_phone" id="teacher_phone" size="35"
                           value="<?php print $teacher_phone ?>"></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><textarea name="teacher_address" id="teacher_address" cols="30" rows="2"><?php print $teacher_address ?></textarea></td>
            </tr>
            <tr>
                <td>City</td>
                <td><input type="text" name="teacher_city" id="teacher_city" size="35"
                           value="<?php print $teacher_city ?>"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="Submit" value="Save"></td>
            </tr>
        </table>
    </form>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>

<script>
    function ch() {
        if ($("#teacherpassword").val() != $("#teacherpassword2").val()) {
            alert("Your Passwords do not match");
            $("#teacherpassword").focus();
            return false;
        } else {
            return true;
        }
    }

</script>