<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 10:58 AM
 */
require("header_leftnav.inc.php");

$department_name = "";
$label = "Add Level";
$cancel = "";
$action = "";

if (isset($_POST['save_department'])) {
    $_POST['department_name'] = trim($_POST['department_name']);
    $_SESSION['departments'] = $_POST;

    $sql = "SELECT * FROM departments WHERE departments_name = '{$_POST['department_name']}'";
    if (!empty($_GET['departmentID']))
        $sql .= " AND departments_id != " . (int)$_GET['departmentID'];
    $resource = mysql_query($sql);
    if (mysql_num_rows($resource) > 0)
        $msg = "<b>{$_POST['department_name']}</b> already exists";
    else {
        if (isset($_GET['action']) && $_GET['action'] == "edit_department" && !empty($_GET['departmentID'])) {
            $resource = mysql_query("SELECT departments_name FROM departments WHERE departments_id = " . (int)$_GET['departmentID']);
            $old_department_name = mysql_result($resource, 0);

            $sql = "UPDATE departments SET departments_name = '{$_POST['department_name']}' WHERE departments_id = " . (int)$_GET['departmentID'];
        }
        else
            $sql = "INSERT INTO departments (departments_name, added_on, is_enabled) VALUE ('{$_POST['department_name']}', NOW(), TRUE)";
        mysql_query($sql) or die(mysql_error());
        if (mysql_affected_rows() > 0) {
            unset($_SESSION['departments']);
            if (isset($_GET['action']) && $_GET['action'] == "edit_department")
                $msg = "<b>{$old_department_name}</b> changed to <b>{$_POST['department_name']}</b> successfully";
            else
                $msg = "<b>{$_POST['department_name']}</b> added successfully";
        }
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'enable_department' && !empty($_GET['departmentID'])) {
    $sql = "UPDATE departments SET is_enabled = IF (is_enabled = TRUE, FALSE, TRUE) WHERE departments_id = " . (int)$_GET['departmentID'];
    mysql_query($sql);
    if (mysql_affected_rows() > 0)
        $msg = "Level status changed successfully";
}

if (isset($_GET['action']) && $_GET['action'] == "edit_department" && !empty($_GET['departmentID'])) {
    $resource = mysql_query("SELECT * FROM departments WHERE departments_id = {$_GET['departmentID']}");
    $this_department = mysql_fetch_assoc($resource);
    $department_name = $this_department['departments_name'];
    $action = "action=edit_department&departmentID=" . $this_department['departments_id'];
    $label = "Change Level";
    $cancel = "<a href='departments.php'  class='a'>Cancel</a>";
}

$sql = "SELECT * FROM departments ORDER BY departments_name";
$resource = mysql_query($sql) or die(mysql_error());

if (!empty($_SESSION['departments'])) {
    extract($_SESSION['departments']);
    unset($_SESSION['departments']);
}
?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Levels</h1>

                <form method="post" class='form' action="?<?php echo $action ?>">
                    <?php echo $label?>
                    <input type="text" name="department_name" value="<?php echo $department_name ?>" required="required">
                    <button type="submit" name="save_department">Save</button>
                    <?php echo $cancel ?>
                </form>

                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";

                if (mysql_num_rows($resource) > 0) {
                    ?>
                    <table border=0 align=left cellpadding=5 cellspacing=0 class="list">
                        <tr>
                            <th>#</th>
                            <th>Level Name</th>
                            <th>Enabled</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $index = 0;
                        while ($department = mysql_fetch_assoc($resource)) {
                            $is_enabled = $department['is_enabled'] ? "Yes" : "No";
                            $is_enabled_class = $department['is_enabled'] ? "label-success" : "label-danger";
                            $enable = $department['is_enabled'] ? "Disable" : "Enable";
                            ?>
                            <tr>
                                <td><?php echo ++ $index ?></td>
                                <td><?php echo $department["departments_name"] ?></td>
                                <td><span class="label <?php echo $is_enabled_class ?>"><?php echo $is_enabled ?></span></td>
                                <td>
                                    <a href="?action=edit_department&departmentID=<?php echo $department['departments_id'] ?>" class="a">Edit</a>
                                    ::
                                    <a href="?action=enable_department&departmentID=<?php echo $department['departments_id'] ?>" class="a"><?php echo $enable ?></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php }
                else
                    echo "<p>No level found</p>";
                ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>
