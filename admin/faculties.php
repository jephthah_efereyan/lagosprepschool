<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 10:45 AM
 */
require("header_leftnav.inc.php");

$faculty_name = "";
$label = "Add Class";
$cancel = "";
$action = "";

if (isset($_POST['save_faculty'])) {
    $_POST['faculty_name'] = trim($_POST['faculty_name']);
    $_SESSION['faculties'] = $_POST;

    $sql = "SELECT * FROM faculties WHERE faculties_name = '{$_POST['faculty_name']}'";
    if (!empty($_GET['facultyID']))
        $sql .= " AND faculties_id != " . (int)$_GET['facultyID'];
    $resource = mysql_query($sql);
    if (mysql_num_rows($resource) > 0)
        $msg = "<b>{$_POST['faculty_name']}</b> already exists";
    else {
        if (isset($_GET['action']) && $_GET['action'] == "edit_faculty" && !empty($_GET['facultyID'])) {
            $resource = mysql_query("SELECT faculties_name FROM faculties WHERE faculties_id = " . (int)$_GET['facultyID']);
            $old_faculty_name = mysql_result($resource, 0);

            $sql = "UPDATE faculties SET faculties_name = '{$_POST['faculty_name']}' WHERE faculties_id = " . (int)$_GET['facultyID'];
        }
        else
            $sql = "INSERT INTO faculties (faculties_name, added_on, is_enabled) VALUE ('{$_POST['faculty_name']}', NOW(), TRUE)";
        mysql_query($sql) or die(mysql_error());
        if (mysql_affected_rows() > 0) {
            unset($_SESSION['faculties']);
            if (isset($_GET['action']) && $_GET['action'] == "edit_faculty")
                $msg = "<b>{$old_faculty_name}</b> changed to <b>{$_POST['faculty_name']}</b> successfully";
            else
                $msg = "<b>{$_POST['faculty_name']}</b> added successfully";
        }
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'enable_faculty' && !empty($_GET['facultyID'])) {
    $sql = "UPDATE faculties SET is_enabled = IF (is_enabled = TRUE, FALSE, TRUE) WHERE faculties_id = " . (int)$_GET['facultyID'];
    mysql_query($sql);
    if (mysql_affected_rows() > 0)
        $msg = "Class status changed successfully";
}

if (isset($_GET['action']) && $_GET['action'] == "edit_faculty" && !empty($_GET['facultyID'])) {
    $resource = mysql_query("SELECT * FROM faculties WHERE faculties_id = {$_GET['facultyID']}");
    $this_faculty = mysql_fetch_assoc($resource);
    $faculty_name = $this_faculty['faculties_name'];
    $action = "action=edit_faculty&facultyID=" . $this_faculty['faculties_id'];
    $label = "Change Class";
    $cancel = "<a href='faculties.php'  class='a'>Cancel</a>";
}

$sql = "SELECT * FROM faculties ORDER BY faculties_name";
$resource = mysql_query($sql) or die(mysql_error());

if (!empty($_SESSION['faculties'])) {
    extract($_SESSION['faculties']);
    unset($_SESSION['faculties']);
}
?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Classes</h1>

                <form method="post" class='form' action="?<?php echo $action ?>">
                    <?php echo $label?>
                    <input type="text" name="faculty_name" value="<?php echo $faculty_name ?>" required="required">
                    <button type="submit" name="save_faculty">Save</button>
                    <?php echo $cancel ?>
                </form>

                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";

                if (mysql_num_rows($resource) > 0) {
                    ?>
                    <table border=0 align=left cellpadding=5 cellspacing=0 class="list">
                        <tr>
                            <th>#</th>
                            <th>Class Name</th>
                            <th>Enabled</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $index = 0;
                        while ($faculty = mysql_fetch_assoc($resource)) {
                            $is_enabled = $faculty['is_enabled'] ? "Yes" : "No";
                            $is_enabled_class = $faculty['is_enabled'] ? "label-success" : "label-danger";
                            $enable = $faculty['is_enabled'] ? "Disable" : "Enable";
                            ?>
                            <tr>
                                <td><?php echo ++ $index ?></td>
                                <td><?php echo $faculty["faculties_name"] ?></td>
                                <td><span class="label <?php echo $is_enabled_class ?>"><?php echo $is_enabled ?></span></td>
                                <td>
                                    <a href="?action=edit_faculty&facultyID=<?php echo $faculty['faculties_id'] ?>" class="a">Edit</a>
                                    ::
                                    <a href="?action=enable_faculty&facultyID=<?php echo $faculty['faculties_id'] ?>" class="a"><?php echo $enable ?></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php }
                else
                    echo "<p>No class found</p>";
                ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>
