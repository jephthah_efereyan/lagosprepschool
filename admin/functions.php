<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 1:50 PM
 */

function GetSessions() {
    $ret_val = array();

    $sql = 'SELECT * FROM school_sessions';
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($session = mysql_fetch_assoc($result))
            $ret_val[] = $session;
    }

    return $ret_val;
}

function GetSession($session_ref) {
    $ret_val = array();

    $sql = "SELECT * FROM school_sessions WHERE session_id = " . (int)$session_ref;
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        $ret_val = mysql_fetch_assoc($result);
    }

    return $ret_val;
}

function GetTerms() {
    $ret_val = array();

    $sql = 'SELECT * FROM school_terms';
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($term = mysql_fetch_assoc($result))
            $ret_val[] = $term;
    }

    return $ret_val;
}

function GetTerm($term_ref) {
    $ret_val = array();

    $sql = "SELECT * FROM school_terms WHERE term_id = " . (int)$term_ref;
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        $ret_val = mysql_fetch_assoc($result);
    }

    return $ret_val;
}

function GetCurrentSessionTerm() {
    $ret_val = NULL;

    $sql = "SELECT st.*, s.session_name, s.session_fullname, t.term_name, t.term_fullname FROM session_terms st
            JOIN school_sessions s ON st.session_id = s.session_id
            JOIN school_terms t ON st.term_id = t.term_id
            WHERE st.status = 'Current'";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        $ret_val = mysql_fetch_assoc($result);

    return $ret_val;
}

function GetSessionTerm($session_id, $term_id) {
    $ret_val = NULL;

    $sql = "SELECT * FROM session_terms WHERE session_id = {$session_id} AND term_id = {$term_id}";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        $ret_val = mysql_fetch_assoc($result);

    return $ret_val;
}

function GetSessionTermByID($session_term_id) {
    $ret_val = NULL;

    $sql = "SELECT * FROM session_terms WHERE session_term_id = {$session_term_id}";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        $ret_val = mysql_fetch_assoc($result);

    return $ret_val;
}

function GetFaculties() {
    $ret_val = array();

    $sql = 'SELECT * FROM faculties';
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($faculty = mysql_fetch_assoc($result))
            $ret_val[] = $faculty;
    }

    return $ret_val;
}

function GetDepartments() {
    $ret_val = array();

    $sql = 'SELECT * FROM departments';
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($department = mysql_fetch_assoc($result))
            $ret_val[] = $department;
    }

    return $ret_val;
}

function GetBills_($session_term_id, $faculty_id, $department_id) {
    $ret_val = array();

    $sql = "SELECT b.*, bi.bi_name, f.faculties_name, d.departments_name FROM bills b
            JOIN billing_items bi ON b.item_id = bi.bi_id
            JOIN faculties f ON b.faculty_id = f.faculties_id
            JOIN departments d ON b.department_id = d.departments_id";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($billing_item = mysql_fetch_assoc($result))
            $ret_val[] = $billing_item;
    }

    return $ret_val;
}

function GetBills($session_term_id, $faculty_id, $department_id) {
    $ret_val = array();

    $sql = "SELECT bi.*, b.*, f.faculties_name, d.departments_name FROM billing_items bi
            LEFT JOIN bills b ON bi.bi_id = b.item_id AND b.session_term_id = {$session_term_id} AND b.faculty_id = {$faculty_id} AND b.department_id = {$department_id}
            LEFT JOIN faculties f ON b.faculty_id = f.faculties_id
            LEFT JOIN departments d ON b.department_id = d.departments_id";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($billing_item = mysql_fetch_assoc($result))
            $ret_val[] = $billing_item;
    }

    return $ret_val;
}

/* function GetExchangeRates() {
    $ret_val = array();

    $sql = "SELECT st.session_term_id, st.session_id, st.term_id, s.session_fullname, t.term_fullname, er.rate FROM session_terms st
            JOIN school_sessions s ON st.session_id = s.session_id
            JOIN school_terms t ON st.term_id = t.term_id
            LEFT JOIN exchange_rates er ON st.session_term_id = er.session_term_id";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($exchange_rate = mysql_fetch_assoc($result))
            $ret_val[] = $exchange_rate;
    }

    return $ret_val;
} */

function GetExchangeRate() {
    $ret_val = array();

    $sql = "SELECT * FROM exchange_rates";
    $result = mysql_query($sql);

    if (mysql_num_rows($result) > 0){
    	$ret_val = mysql_fetch_assoc($result);
    }

    return $ret_val;
}

function GetTransaction($trans_no) {
    $ret_val = NULL;

    $sql = "SELECT *  FROM `transactions` WHERE trans_no = '{$trans_no}'";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        $ret_val = mysql_fetch_assoc($result);

    return $ret_val;

}

function GetTransactionItems($id) {
    $ret_val = array();

    $sql = "SELECT * FROM `transactions` t
            JOIN bills b ON t.bill_id = b.bill_id
            JOIN billing_items bi ON b.item_id = bi.bi_id
            WHERE t.id = {$id}";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0)
        while($items = mysql_fetch_assoc($result))
            $ret_val[] = $items;
	else print mysql_error();
    return $ret_val;

}

function GetStudentTransactions($student_id) {
    $transactions = array();
    $sql = "SELECT DISTINCT(trans_no), trans_status, trans_date FROM transactions WHERE student_id = '$student_id' ORDER BY id DESC";
    $result = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($result) > 0) {
        while ($transaction = mysql_fetch_assoc($result))
            $transactions[] = $transaction;
    }

    return $transactions;
}

function GetStudentClass($admission_no, $session, $term) {
    $student_class = array();
    $sql = "SELECT 
    			studentprofile_extra.faculty_id, 
    			studentprofile_extra.department_id, 
    			studentprofile_extra.programme_id, f.faculties_id, d.departments_id, p.programme_id
            FROM studentprofile_extra
            LEFT JOIN faculties f ON studentprofile_extra.class_level = f.faculties_name
            LEFT JOIN departments d ON studentprofile_extra.class_no = d.departments_name
            LEFT JOIN programme p ON studentprofile_extra.class_letter = p.programme_name
            WHERE studentprofile_extra.student_id = '{$admission_no}' AND yearsession = '{$session}' AND term = '{$term}'";
    $result = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($result) > 0)
        $student_class = mysql_fetch_assoc($result);

    return $student_class;
}