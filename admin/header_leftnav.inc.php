<?php

include_once("auth.inc.php");

include_once("config.php");

include_once("lang.php");

include_once("functions.php");

?>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

<head>

    <title><?php print $bk_website_name; ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="adminstyle.css" type="text/css" rel="stylesheet">

    <script src="../js/jquery-1.9.1.js"></script>

</head>



<body>

<center>

    <table width="1000" border="1" bordercolor="#000000" cellpadding="0" cellspacing="0">

        <tr>

            <td valign="top">

                <table width="1000" height="100" border="0" cellpadding="0" cellspacing="0">

                    <tr>

                        <td valign="top"><a href="http://www.upperlink.ng" target="_blank"><img

                                    src="admin_images/header.jpg" width="1000" height="125" border="0"

                                    alt="<?php print $alt_admin_banner; ?>"></a></td>

                    </tr>

                </table>



                <table width="1000" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                        <td valign="top" width="220" bgcolor="<?php print $leftnavcolor ?>">

                            <table width="95%" align="center" cellpadding="5" cellspacing="3" class="nav">

                                <tr>

                                    <td bgcolor=#546376 style="color:#ffffff">

                                        Welcome <?php print !empty($_SESSION['user']['surname']) ? $_SESSION['user']['surname'] . " " . $_SESSION['user']['firstname'] : ""; ?>

                                        <br>

                                        Role: <b><?php echo $_SESSION['user']['role'] ?></b>

                                        <br>

                                        Current Session:

                                        <b><?php echo $_SESSION['current_session_term']['session_fullname'] ?></b>

                                        <br>

                                        Current Term:

                                        <b><?php echo $_SESSION['current_session_term']['term_fullname'] ?></b>

                                    </td>

                                </tr>



                                <?php

                                if ($_SESSION['user']['role_id'] == 1) {

                                    $menu = array(

                                        'administrators' => 'Administrators',

                                        'school-sessions' => 'School Sessions',

                                        /*'classteacher' => 'Class Teachers',

                                        'subjects' => 'Subjects',

                                        'faculties' => 'Classes',

                                        'departments' => 'Levels',

                                        'programmes' => 'Arms',*/

                                        'exchange-rates' => 'Exchange Rates',

                                        'billing-items' => 'Billing Items',

                                        'bills' => 'Bills',

                                        //'bills-addbulk' => 'Upload Bills',

                                        'pupils-addbulk' => 'Add Pupils (Bulk Upload)',

                                        'pupil-profile-create' => 'Create Student Profile',

                                        'pupils-all' => 'Students List',

                                        'payment-history' => 'Summarized Payment History',
										'payment-history-full' => 'Full Payment History'

                                    );

                                } elseif ($_SESSION['user']['role_id'] == 2) {

                                    $menu = array(

                                        'school-sessions' => 'School Sessions',

                                        'billing-items' => 'Billing Items',

                                        'bills' => 'Bills',

                                        'pupils-addbulk' => 'Add Pupils (Bulk Upload)',

                                        'pupil-profile-create' => 'Create Student Profile',

                                        'pupils-all' => 'Students List',

                                        'payment-history' => 'Summarized Payment History',
										'payment-history-full' => 'Full Payment History'

                                    );

                                }



                                foreach ($menu as $file => $text):

                                    if (file_exists($file . ".php"))

                                        $file .= ".php";

                                    ?>

                                    <tr>

                                        <td bgcolor='<?php print $bgcolorleftlinks; ?>'>

                                            <a href='<?php print $file ?>' target='_self'><?php print $text ?></a>

                                        </td>

                                    </tr>

                                <?php

                                endforeach;

                                ?>

                                <tr>

                                    <td bgcolor='<?php print $bgcolorleftlinks; ?>'><a

                                            href='change-password.php'>Change Password</a></td>

                                </tr>

                                <tr>

                                    <td bgcolor='<?php print $bgcolorleftlinks; ?>'><a

                                            href='logout.php'>Log Out</a></td>

                                </tr>

                            </table>

                        </td>