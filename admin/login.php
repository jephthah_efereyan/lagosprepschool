<?php
session_start();
$_SESSION['logged'] = 0;
include_once("lang.php");
include_once("functions.php");
include_once '../autoload.php';

$msg = "You must be logged in to continue";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "";

if (isset($_POST['submit'])) {
    include_once("config.php");
    $redirect = $_POST['redirect'];

    $email = mysql_real_escape_string($_POST['email']);
    $password = mysql_real_escape_string($_POST['password']);

    $administrator = Administrator::authenticate($connect, $email, $password);
    if ($administrator) {
        if ($administrator->active) {
            $_SESSION['logged'] = 1;
            $_SESSION['user']['id'] = $administrator->id;
            $_SESSION['user']['surname'] = $administrator->surname;
            $_SESSION['user']['firstname'] = $administrator->firstname;
            $_SESSION['user']['email'] = $administrator->email;
            $_SESSION['user']['role'] = $administrator->role;
            $_SESSION['user']['role_id'] = $administrator->role_id;

            $_SESSION['current_session_term'] = GetCurrentSessionTerm();

            header("Refresh: 2; URL=" . $redirect);
            print "You are being redirected to your original page request<br/>";
            print "(If your browser doesn't support this, <a href='" . $redirect . "'>click here</a>)";
            die();
        }
        else
            $msg = "You are not activated";
    } else
        $msg = "Invalid Username and/or Password";
}
?>
<html>
<head>
    <title><?php print "$bk_website_name: Login"; ?></title>
    <link href="adminstyle.css" type="text/css" rel="stylesheet">
</head>
<body>
<center>
    <table width="750" border="1" cellspacing="0" cellpadding="0" bordercolor="#000000">
        <tr>
            <td bgcolor="#ffffff">
                <a href="http://www.upperlinkltd.com" target="_blank"><img src="admin_images/header.jpg" width="750"
                                                                           height="125" border="0" alt="Site Admin"></a>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFCC99" align=center>
                <br/>
                <?php print $msg ?>
                <br/><br/>

                <form action="login.php" method="post" autocomplete=off>
                    <input type="hidden" name="redirect" value="<?php print $redirect; ?>">
                    <table>
                        <tr>
                            <td>Email:</td>
                            <td><input type="text" name="email"></td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td><input type="password" name="password"></td>
                        </tr>
                    </table>
                    <br>
                    <input type="submit" name="submit" value="Login">
                </form>
            </td>
        </tr>
    </table>
</center>
</body>
</html>