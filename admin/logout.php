<?php
session_start();
session_destroy();
$_SESSION = array();
header("Refresh: 2; URL=index.php");
echo "(If your browser doesn't support this, <a href='index.php'>click here</a>)";
die();
