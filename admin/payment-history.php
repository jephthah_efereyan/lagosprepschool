<?php require("header_leftnav.inc.php"); ?>



<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">

<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">

<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>

<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>

<style>

    #myTable a {

        color: blue;

    }

    table#items {

        margin: auto;

        border-collapse: collapse;

    }

    table#items th, table#items td{

        padding: 6px;

        border: 1px solid

    }

    table#items .num {

        text-align: right

    }

</style>

<td valign="top" class="page-content">

    <?php

    include_once '../autoload.php';

    $history = Transaction::history($connect);

    ?>

    <table id="myTable" style="background-color:transparent">

        <thead>

        <tr>

            <th align="left">#</th>

            <th align="left">Student</th>

            <th align="left">Date</th>

            <th align="left">&#x20a6; School Fees</th>

            <th align="left">&#x20a6; Charges</th>

            <th align="left">&#x20a6; Total</th>



        </tr>

        </thead>

        <tbody>

        <?php foreach ($history as $i => $payment): ?>

            <tr>

                <td align="left"><?php print $i + 1 ?></td>

                <td align="left"><?php print $payment['name'] . '<br>' . $payment['admission_no'] ?></td>

                <td align="left"><?php print date("Y-m-d", strtotime($payment['trans_date'])) ?></td>

                <td align="left">

                    <?php print number_format(($payment['trans_amount'] - $payment['portal_charge'] - $payment['trans_charge']), 2); ?>

                    <?php if (!empty($payment['items'])): ?>

                        <a href="#" class="dialog-ctrl" id="tr<?php print $payment['id'] ?>">Details</a>

                        <div style="display: none"><?php print json_encode($payment['items']); ?></div>

                    <?php endif; ?>

                </td>

                <td align="left">
	<?php print number_format($payment['trans_charge'] + $payment['portal_charge'], 2) ?>
    			</td>

                <td align="left"><?php print number_format($payment['trans_amount'], 2) ?></td>

            </tr>

        <?php endforeach; ?>

        </tbody>

    </table>

</td>

</tr>

</table>



<div id="dialog" title="Payment Details"></div>



<?php require("footer.php"); ?>



<script src="../js/jquery-ui-1.11.4/jquery-ui.min.js"></script>

<link rel="stylesheet" type="text/css" href="../js/jquery-ui-1.11.4/jquery-ui.min.css">



<script>

    $(document).ready(function () {

        $('#myTable').dataTable({

            //dom: 'Bfrtip', buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print' ]

        });



        $("#dialog").dialog({

            autoOpen: false,

            width: 400,

            modal: true,

            buttons: [

                {

                    text: "Close",

                    click: function () {

                        $(this).dialog("close");

                    }

                }

            ]

        });



        // Link to open the dialog

        $(".dialog-ctrl").click(function (event) {

            //get the items list and parse into a JSON object

            var items = JSON.parse($(this).next().html());



            //prepare the display string

            var str = '<table id="items"><tr><th>Item</th><th>Amount (NGN)</th></tr>';

            var total = 0;



            //iterate over the JSON object

            $.each(items, function(key, item) {

                str += '<tr><td>' + item.item + '</td><td class="num">' + parseFloat(item.amount).format(2) + '</td></tr>';

                total += parseFloat(item.amount);

            });



            //complete the display string

            str += '<tr><th>TOTAL</th><th class="num">' + total.format(2) + '</th></tr>';

            str += '</table>';



            //open the dialog

            $("#dialog").html(str);

            $("#dialog").dialog("open");



            event.preventDefault();

        });

    });



    Number.prototype.format = function(n, x) {

        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';

        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');

    };

</script>

</body>

</html>

