<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/5/2015
 * Time: 5:05 PM
 */
session_start();
include_once("config.php");
if (isset($_GET['module'])) {
    switch ($_GET['module']) {
        case "classteacher":
            switch ($_GET['action']) {
                case "add":
                    $response = addTeacher($_POST);
                    $st = $response ? "00" : "1";
                    $redirect = $_SERVER['HTTP_REFERER'];
                    break;
                case "edit":
                    $response = modifyTeacher($_POST, $_GET['id']);
                    $st = $response ? "00" : "1";
                    $redirect = $_SERVER['HTTP_REFERER'];
                    break;
            }
            break;
        case "wassce_result":
            $_SESSION['form'] = $_POST;
            switch ($_GET['action']) {
                case "add":
                    $response = addResult($_POST, $_FILES);
                    if ($response['status']) {
                        $st = "Result added successfully";
                        unset($_SESSION['form']);
                    }
                    else
                        $st = $response['msg'];
                    $redirect = $_SERVER['HTTP_REFERER'];
                    break;
                case "edit":
                    $response = modifyResult($_POST, $_FILES, $_GET['result']);
                    $st = $response['status'] ? "Result modified successfully" : $response['msg'];
                    $redirect = $_SERVER['HTTP_REFERER'];
                    break;
                case "publish":
                    $response = publishResult($_GET['result']);
                    //$st = $response ? "Result modified successfully" : $response['msg'];
                    $redirect = $_SERVER['HTTP_REFERER'];
                    break;
            }
            break;
    }
    $_SESSION['st'] = !empty($st) ? $st : "";
    header("Location: {$redirect}");
}

function addTeacher($teacher_details)
{
    $sql = "INSERT INTO classteacher
            SET teacherlastname = '{$teacher_details['teacherlastname']}',
                teacherfirstname = '{$teacher_details['teacherfirstname']}',
                teacherothernames = '{$teacher_details['teacherothernames']}',
                teacherusername = '{$teacher_details['teacherusername']}',
                teacher_email = '{$teacher_details['teacher_email']}',
                teacherpassword = '{$teacher_details['teacherpassword']}',
                teacher_phone = '{$teacher_details['teacher_phone']}',
                teacher_address = '{$teacher_details['teacher_address']}',
                teacher_city = '{$teacher_details['teacher_city']}',
                teacher_state_id = 25, /*Lagos*/
                teacherclass_level = '{$teacher_details['teacherclass_level']}',
                teacherclass_no = '{$teacher_details['teacherclass_no']}',
                teacherclass_letter = '{$teacher_details['teacherclass_letter']}'";
    mysql_query($sql) or die(mysql_error());

    return mysql_affected_rows() > 0 ? TRUE : FALSE;
}

function modifyTeacher($teacher_details, $teacher_id)
{
    $sql = "UPDATE classteacher
            SET teachertitle = '{$teacher_details['teachertitle']}',
                teacherlastname = '{$teacher_details['teacherlastname']}',
                teacherfirstname = '{$teacher_details['teacherfirstname']}',
                teacherothernames = '{$teacher_details['teacherothernames']}',
                teacherusername = '{$teacher_details['teacherusername']}',
                teacher_email = '{$teacher_details['teacher_email']}',
                teacherpassword = '{$teacher_details['teacherpassword']}',
                teacher_phone = '{$teacher_details['teacher_phone']}',
                teacher_address = '{$teacher_details['teacher_address']}',
                teacher_city = '{$teacher_details['teacher_city']}',
                teacher_state_id = 25, /*Lagos*/
                teacherclass_level = '{$teacher_details['teacherclass_level']}',
                teacherclass_no = '{$teacher_details['teacherclass_no']}',
                teacherclass_letter = '{$teacher_details['teacherclass_letter']}'
            WHERE classteacher_id = {$teacher_id}";
    mysql_query($sql) or die(mysql_error());

    return mysql_affected_rows() > 0 ? TRUE : FALSE;
}

function addResult($result_detail, $file)
{
    $return = array("status" => FALSE, "msg" => "");
    $resource = mysql_query("SELECT * FROM wassce_results WHERE wassce_result_year = '{$result_detail['wassce_result_year']}'");
    if (mysql_num_rows($resource) > 0)
        $return["msg"] = "WASSCE Result exists";
    else {
        if ($file['result_file']['error'] == 0) {
            if ($file['result_file']['type'] == "application/pdf") {
                if ($file['result_file']['size'] < 1048000) {
                    $filename = $result_detail['wassce_result_year'] . ".pdf";
                    if (move_uploaded_file($file['result_file']['tmp_name'], "../wassce-results/{$filename}")) {
                        $sql = "INSERT INTO wassce_results
                                SET wassce_result_year = '{$result_detail['wassce_result_year']}',
                                    wassce_result_file = '{$filename}',
                                    published = '{$result_detail['published']}',
                                    added_on = NOW(),
                                    modified_by = 1";
                        mysql_query($sql);
                        $return['status'] = TRUE;
                    }
                }
                else
                    $return['msg'] = "File is too large";
            }
            else
                $return['msg'] = "Invalid file format";
        }
        else
            $return['msg'] = "No file selected";
    }

    return $return;
}

function modifyResult($result_detail, $file, $result_id)
{
    $return = array("status" => FALSE, "msg" => "");
    $resource = mysql_query("SELECT * FROM wassce_results WHERE wassce_result_year = '{$result_detail['wassce_result_year']}' AND wassce_result_id != {$result_id}");
    if (mysql_num_rows($resource) > 0)
        $return["msg"] = "WASSCE Result exists";
    else {
        if ($file['result_file']['error'] != 4) {
            if ($file['result_file']['type'] == "application/pdf") {
                if ($file['result_file']['size'] < 1048000) {
                    $filename = $result_detail['wassce_result_year'] . ".pdf";
                    move_uploaded_file($file['result_file']['tmp_name'], "../wassce-results/{$filename}");
                }
                else
                    $return['msg'] = "File is too large";
            }
            else
                $return['msg'] = "Invalid file format";
        }

        if (empty($return['msg'])) {
            $sql = "UPDATE wassce_results
                    SET wassce_result_year = '{$result_detail['wassce_result_year']}',
                        published = '{$result_detail['published']}',
                        modified_on = NOW(),
                        modified_by = 1";
            $sql .= !empty($filename) ? ", wassce_result_file = '{$filename}'" : "";
            $sql .= " WHERE wassce_result_id = {$result_id}";
            mysql_query($sql);
            $return['status'] = TRUE;
        }
    }

    return $return;
}

function publishResult($result_id) {
    $sql = "UPDATE wassce_results SET published = IF(published = 'yes', 'no', 'yes'), modified_on = NOW() WHERE wassce_result_id = {$result_id}";
    mysql_query($sql);
    return TRUE;
}