<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 11:05 AM
 */

require("header_leftnav.inc.php");

$programme_name = "";
$label = "Add Arm";
$cancel = "";
$action = "";

if (isset($_POST['save_programme'])) {
    $_POST['programme_name'] = trim($_POST['programme_name']);
    $_SESSION['programme'] = $_POST;

    $sql = "SELECT * FROM programme WHERE programme_name = '{$_POST['programme_name']}'";
    if (!empty($_GET['programmeID']))
        $sql .= " AND programme_id != " . (int)$_GET['programmeID'];
    $resource = mysql_query($sql);
    if (mysql_num_rows($resource) > 0)
        $msg = "<b>{$_POST['programme_name']}</b> already exists";
    else {
        if (isset($_GET['action']) && $_GET['action'] == "edit_programme" && !empty($_GET['programmeID'])) {
            $resource = mysql_query("SELECT programme_name FROM programme WHERE programme_id = " . (int)$_GET['programmeID']);
            $old_programme_name = mysql_result($resource, 0);

            $sql = "UPDATE programme SET programme_name = '{$_POST['programme_name']}' WHERE programme_id = " . (int)$_GET['programmeID'];
        }
        else
            $sql = "INSERT INTO programme (programme_name, added_on, is_enabled) VALUE ('{$_POST['programme_name']}', NOW(), TRUE)";
        mysql_query($sql) or die(mysql_error());
        if (mysql_affected_rows() > 0) {
            unset($_SESSION['programme']);
            if (isset($_GET['action']) && $_GET['action'] == "edit_programme")
                $msg = "<b>{$old_programme_name}</b> changed to <b>{$_POST['programme_name']}</b> successfully";
            else
                $msg = "<b>{$_POST['programme_name']}</b> added successfully";
        }
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'enable_programme' && !empty($_GET['programmeID'])) {
    $sql = "UPDATE programme SET is_enabled = IF (is_enabled = TRUE, FALSE, TRUE) WHERE programme_id = " . (int)$_GET['programmeID'];
    mysql_query($sql);
    if (mysql_affected_rows() > 0)
        $msg = "Arm status changed successfully";
}

if (isset($_GET['action']) && $_GET['action'] == "edit_programme" && !empty($_GET['programmeID'])) {
    $resource = mysql_query("SELECT * FROM programme WHERE programme_id = {$_GET['programmeID']}");
    $this_programme = mysql_fetch_assoc($resource);
    $programme_name = $this_programme['programme_name'];
    $action = "action=edit_programme&programmeID=" . $this_programme['programme_id'];
    $label = "Change Arm";
    $cancel = "<a href='programme.php'  class='a'>Cancel</a>";
}

$sql = "SELECT * FROM programme ORDER BY programme_name";
$resource = mysql_query($sql) or die(mysql_error());

if (!empty($_SESSION['programme'])) {
    extract($_SESSION['programme']);
    unset($_SESSION['programme']);
}
?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Arms</h1>

                <form method="post" class='form' action="?<?php echo $action ?>">
                    <?php echo $label?>
                    <input type="text" name="programme_name" value="<?php echo $programme_name ?>" required="required">
                    <button type="submit" name="save_programme">Save</button>
                    <?php echo $cancel ?>
                </form>

                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";

                if (mysql_num_rows($resource) > 0) {
                    ?>
                    <table border=0 align=left cellpadding=5 cellspacing=0 class="list">
                        <tr>
                            <th>#</th>
                            <th>Arm Name</th>
                            <th>Enabled</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $index = 0;
                        while ($programme = mysql_fetch_assoc($resource)) {
                            $is_enabled = $programme['is_enabled'] ? "Yes" : "No";
                            $is_enabled_class = $programme['is_enabled'] ? "label-success" : "label-danger";
                            $enable = $programme['is_enabled'] ? "Disable" : "Enable";
                            ?>
                            <tr>
                                <td><?php echo ++ $index ?></td>
                                <td><?php echo $programme["programme_name"] ?></td>
                                <td><span class="label <?php echo $is_enabled_class ?>"><?php echo $is_enabled ?></span></td>
                                <td>
                                    <a href="?action=edit_programme&programmeID=<?php echo $programme['programme_id'] ?>" class="a">Edit</a>
                                    ::
                                    <a href="?action=enable_programme&programmeID=<?php echo $programme['programme_id'] ?>" class="a"><?php echo $enable ?></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php }
                else
                    echo "<p>No arm found</p>";
                ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>
