<?php require("header_leftnav.inc.php"); ?>
<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">
	<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
	<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>
	<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="../js/jquery-2.2.3.min.js"></script> -->
<script type="text/javascript" src="../js/jquery.validation/jquery.validate.min.js"></script>
<style>
    #myTable a {
        color: blue;
    }
    dl {
    height: 200px;
    border: 3px double #ccc;
    padding: 0.5em;
    font-size: 13px;
   // width: 400px
  }
  dt {
    float: left;
    clear: left;
    width: 120px;
    text-align: right;
    font-weight: bold;
    color: #444;
    margin-bottom: 5px;
    
  }
  dt:after {
    content: ":";
  }
  dd {
    margin: 0 0 0 130px;
   // padding: 0 0 0.5em 0;
    font-size: 13px;
     margin-bottom: 5px;
  }
  form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	color: red;
	font-style: italic
}
div.error { display: none; }
/*input {	border: 1px solid black; }*/
input:focus { border: 1px dotted black; }
input.error { border: 1px dotted red; }
form.cmxform .gray * { color: gray; }
input:disabled {	border: 1px dotted orange; color: yellow; }
.text-error{
	padding: 5px;
	font-size: 14px;
	color: white;
	background-color: red;
	margin: 10px;
}
.text-success{
	padding: 5px;
	font-size: 14px;
	color: white;
	background-color: green;
	margin: 10px;
}
</style>
<td valign="top" class="page-content">
    <!-- <p>Welcome, please use the navigation links to your left to perform necessary action</p> -->

    <?php
    include_once '../autoload.php';
	?>
   <h1>New Student Profile</h1>
   <button onclick="location= 'pupil-all.php'">View All</button>
    
<?php 
$success = false;
if($_POST){
	 if(Student::create($connect, $_POST)){ 
		$success = true;
		$link = 'pupil-profile.php?admissionNo='.$_POST['admission_no'];
		
		print '<div class="text-success">Student\'s profile was created</div>';
		print '<div class="text-success">click <a href="'.$link.'">here to view Profile</a></div>';
	}else{
		print '<div class="text-error">Profile was not created</div>';
	} 
}

if(!$success):
?>
 <form method="post" name="profile">
   
    <dl>
    	<dt>Addmission Number</dt>
    	<dd><input name="admission_no" value="<?php print @$admission_no ?>" ></dd>
    	<dt>Surname</dt>
    	<dd><br><input name="lastname" value="<?php print @$lastname ?>" ></dd>
    	
    	<dt>Firstname</dt>
    	<dd><input name="firstname" value="<?php print @$firstname ?>" ></dd>
    	
    	<dt>Othername</dt>
    	<dd><input name="othernames" value="<?php print @$othernames ?>" ></dd>
    	
    	<dt>Gender</dt>
    	<dd><select name="gender" title="Gender is required. Choose Male/Female">
    		<option><?php print @$gender?></option>
    		<option>Male</option>
    		<option>Female</option>
    		</select>
    	</dd>
        
        <dt>Date of Birth</dt>
        <dd><input type="date" name="dateofbirth" value="<?php print @$dateofbirth?>"></dd>
        
    <!--<dt>Place of Birth</dt>
        <dd><input name="placeofbirth" value="<?php print @$placeofbirth?>"></dd>
        
        <dt>Nationality</dt>
        <dd><input name="nationality" value="<?php print @$nationality?>" placeholder="e.g Nigeria"></dd>
        
        <dt>State of Origin	</dt>
        <dd><input name="stateoforigin" value="<?php print @$stateoforigin?>"></dd>
        
        <dt>L. G. A.</dt>
        <dd><input name="localgov" value="<?php print @$localgov?>"></dd>
        
        <dt>Contact Address</dt>
        <dd><textarea name="address"><?php print @$address?></textarea></dd>
        
        <dt>Student's Email	</dt>
        <dd><input type="email" name="email" value="<?php print @$email?>" title="email address should be in this format (****@****.***)"></dd>
         
        <dt>Disability</dt>
        <dd><input name="disability" value="<?php print @$disability?>"></dd>
        
        <dt>Name of Parents/Guardian</dt>
        <dd><input name="parent_name" value="<?php print @$parent_name?>"></dd>
        
        <dt>Address of Parents/Guardian</dt>
        <dd><br><textarea name="parent_address"><?php print @$parent_address?></textarea></dd>
        
        <dt>Parents/Guardian Telephone No</dt>
        <dd><input type="telephone" name="parent_telephone" value="<?php print @$parent_telephone?>"></dd>
    	
    -->

		
</dl>	
	<button type="submit">Submit</button>
	</form>
	
<!--Student's Additional Profile-->	

	
<?php endif;?>	
	


</td>
	 <?php require("footer.php"); ?>
<script>

$(function(){
	
	$(profile).validate(
			{
		rules: {
			"email": {
				required: false,
				email: true
			},
			"parent_telephone": {
				required: false,
				number: true
			},
			"gender": {
				required: true
			},
			"lastname": {
				required: true
			},
			"firstname": {
				required: true
			},
			"admission_no": {
				required: true
			}
		}
			
		
	
	})
})
</script>

</body>
</html>
