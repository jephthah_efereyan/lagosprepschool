<?php require("header_leftnav.inc.php"); ?>
<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">
	<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
	<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>
	<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="../js/jquery-2.2.3.min.js"></script> -->
<script type="text/javascript" src="../js/jquery.validation/jquery.validate.min.js"></script>
<style>
    #myTable a {
        color: blue;
    }
    dl {
    height: 450px;
    border: 3px double #ccc;
    padding: 0.5em;
    font-size: 13px;
   // width: 400px
  }
  dt {
    float: left;
    clear: left;
    width: 120px;
    text-align: right;
    font-weight: bold;
    color: #444;
    margin-bottom: 5px;
    
  }
  dt:after {
    content: ":";
  }
  dd {
    margin: 0 0 0 130px;
   // padding: 0 0 0.5em 0;
    font-size: 13px;
     margin-bottom: 5px;
  }
  form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	color: red;
	font-style: italic
}
div.error { display: none; }
/*input {	border: 1px solid black; }*/
input:focus { border: 1px dotted black; }
input.error { border: 1px dotted red; }
form.cmxform .gray * { color: gray; }
input:disabled {	border: 1px dotted orange; color: yellow; }
.text-error{
	padding: 5px;
	font-size: 14px;
	color: white;
	background-color: red;
	margin: 10px;
}
.text-success{
	padding: 5px;
	font-size: 14px;
	color: white;
	background-color: green;
	margin: 10px;
}
</style>
<td valign="top" class="page-content">
    <!-- <p>Welcome, please use the navigation links to your left to perform necessary action</p> -->

    <?php
    include_once '../autoload.php';
	if(!empty($_GET['admissionNo'])){
		$student =  new Student($connect, $_GET['admissionNo']);
		if($student->student_id){
	?>
   <h1>Edit Profile: <?php print $student->admission_no?></h1>
   <button onclick="location= 'pupil-profile.php?admissionNo=<?php print $student->admission_no ?>'">Back</button>
   <button onclick="location= 'pupil-profile-create.php'">Create New Record</button>
    
<?php 
if($_POST){
	if($student->update($connect, $_POST)){
		print '<div class="text-success">Profile was updated</div>';
		$student->init();
	}else{
		print '<div class="text-error">Profile was not updated: Nothing Changed</div>';
	}
}


?>
 <form method="post" name="profile">
   
    <dl>
    	<dt>Surname</dt>
    	<dd><input name="lastname" value="<?php print $student->lastname ?>" ></dd>
    	
    	<dt>Firstname</dt>
    	<dd><input name="firstname" value="<?php print $student->firstname ?>" ></dd>
    	
    	<dt>Othername</dt>
    	<dd><input name="othernames" value="<?php print $student->othernames ?>" ></dd>
    	
    	<dt>Gender</dt>
    	<dd><select name="gender" title="Gender is required. Choose Male/Female">
    		<option><?php print $student->gender?></option>
    		<option>Male</option>
    		<option>Female</option>
    		</select>
    	</dd>
        
        <dt>Date of Birth</dt>
        <dd><input type="date" name="dateofbirth" value="<?php print $student->dateofbirth?>"></dd>
        
        <dt>Place of Birth</dt>
        <dd><input name="placeofbirth" value="<?php print $student->placeofbirth?>"></dd>
        
        <dt>Nationality</dt>
        <dd><input name="nationality" value="<?php print $student->nationality?>" placeholder="e.g Nigeria"></dd>
        
        <dt>State of Origin	</dt>
        <dd><input name="stateoforigin" value="<?php print $student->stateoforigin?>"></dd>
        
        <dt>L. G. A.</dt>
        <dd><input name="localgov" value="<?php print $student->localgov?>"></dd>
        
        <dt>Contact Address</dt>
        <dd><textarea name="address"><?php print $student->address?></textarea></dd>
        
        <dt>Student's Email	</dt>
        <dd><input type="email" name="email" value="<?php print $student->email?>" title="email address should be in this format (****@****.***)"></dd>
         
        <dt>Disability</dt>
        <dd><input name="disability" value="<?php print $student->disability?>"></dd>
        
        <dt>Name of Parents/Guardian</dt>
        <dd><input name="parent_name" value="<?php print $student->parent_name?>"></dd>
        
        <dt>Address of Parents/Guardian</dt>
        <dd><br><textarea name="parent_address"><?php print $student->parent_address?></textarea></dd>
        
        <dt>Parents/Guardian Telephone No</dt>
        <dd><br><input type="telephone" name="parent_telephone" value="<?php print $student->parent_telephone?>"></dd>
    	
   

		
</dl>	
	<button type="submit">Submit</button>
	</form>
	
<!--Student's Additional Profile-->	

	
	
	

<?php 
		}else{
			print '<h3 style="margin-bottom: 300px">Invalid selection was made. Click <a href="pupils-all.php" style="color:blue">here</a> to view the List</h3>';
		}
			
	}else{
	print '<h3 style="margin-bottom: 300px">Student data was found. Click <a href="pupils-all.php" style="color:blue">here</a> to view the List</h3>';
}?>
</td>
	 <?php require("footer.php"); ?>
<script>

$(function(){
	
	$(profile).validate(
			{
		rules: {
			"email": {
				required: false,
				email: true
			},
			"parent_telephone": {
				required: false,
				number: true
			},
			"gender": {
				required: true
			},
			"lastname": {
				required: true
			},
			"firstname": {
				required: true
			}
		}
			
		
	
	})
})
</script>

</body>
</html>
