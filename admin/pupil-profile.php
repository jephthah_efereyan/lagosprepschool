<?php require("header_leftnav.inc.php"); ?>
<style>
    #myTable a {
        color: blue;
    }
    dl {
    height: 300px;
    border: 3px double #ccc;
    padding: 0.5em;
    font-size: 13px;
  }
  dt {
    float: left;
    clear: left;
    width: 110px;
    text-align: right;
    font-weight: bold;
    color: #444;
    
  }
  dt:after {
    content: ":";
  }
  dd {
    margin: 0 0 0 120px;
   // padding: 0 0 0.5em 0;
    font-size: 13px;
  }
</style>
<td valign="top" class="page-content">
    <!-- <p>Welcome, please use the navigation links to your left to perform necessary action</p> -->

    <?php
    include_once '../autoload.php';
	if(!empty($_GET['admissionNo'])){
		$student =  new Student($connect, $_GET['admissionNo']);
		if($student->student_id){
	?>
    <h1>Profile </h1>
    <hgroup>
    	<h1>Name: <?php print "{$student->lastname} {$student->firstname} {$student->othernames}"?></h1>
       <!-- <h2>Class: <?php print "{$student->faculties_name} {$student->departments_name} {$student->programme_name}"?></h2> --> 
    </hgroup>
    
    
    <dl>
    	<dt>Gender</dt>
    	<dd><?php print $student->gender?></dd>
        
        <dt>Date of Birth</dt>
        <dd><?php print $student->dateofbirth?></dd>
        
        <dt>Place of Birth</dt>
        <dd><?php print $student->placeofbirth?></dd>
        
        <dt>Nationality</dt>
        <dd><?php print $student->nationality?></dd>
        
        <dt>State of Origin	</dt>
        <dd><?php print $student->stateoforigin?></dd>
        
        <dt>L. G. A.</dt>
        <dd><?php print $student->localgov?></dd>
        
        <dt>Contact Address</dt>
        <dd><?php print $student->address?></dd>
        
        <dt>Student's Email	</dt>
        <dd><?php print $student->email?></dd>
         
        <dt>Disability</dt>
        <dd><?php print $student->disability?></dd>
        
        <dt>Name of Parents/Guardian</dt>
        <dd><?php print $student->parent_name?></dd>
        
        <dt>Address of Parents/Guardian</dt>
        <dd><?php print $student->parent_address?></dd>
        
        <dt>Parents/Guardian Telephone No</dt>
        <dd><?php print $student->parent_telephone?></dd>
    	
   
</dl>
<button onclick="location= 'pupil-profile-edit.php?admissionNo=<?php print $student->admission_no ?>'">Edit</button>
		
	

	
	
<!--Student's Additional Profile-->	

	
	
	

<?php 
		}else{
			print '<h3 style="margin-bottom: 300px">Invalid selection was made. Click <a href="pupils-all.php" style="color:blue">here</a> to view the List</h3>';
		}
			
	}else{
	print '<h3 style="margin-bottom: 300px">Student data was found. Click <a href="pupils-all.php" style="color:blue">here</a> to view the List</h3>';
}?>
</td>
	 <?php require("footer.php"); ?>

</body>
</html>
