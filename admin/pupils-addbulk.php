<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/11/2016
 * Time: 5:02 PM
 */

require("header_leftnav.inc.php");
include_once '../autoload.php';

if (isset($_POST['upload'])) {
    //die('<pre>'.print_r($_FILES,1));
    if ($_FILES['upload_file']['error'] == 0) {
        $new_filename = time() . '_list';
        list($name, $ext) = explode('.', basename($_FILES['upload_file']['name']));
        move_uploaded_file($_FILES['upload_file']['tmp_name'], "uploads/{$new_filename}.{$ext}");
        //die('Uploaded');
    } else {
        $msg = "Something wrong with the upload";
    }

    //  Include PHPExcel_IOFactory
    include '../PHPExcel/Classes/PHPExcel/IOFactory.php';

    $inputFileName = "uploads/{$new_filename}.{$ext}"; //'./sampleData/example1.xls';

//  Read your Excel workbook
    try {
        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);
    } catch(Exception $e) {
        die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    }

    //get the data in the file
    $dataArray = $objPHPExcel->getActiveSheet()->toArray();

    //get the column headers
    $col_headers = array_shift($dataArray);

    //validate each cell in each row
    $validation_errors = array();

    //this will generate variables with the names of the column headers and set their values to the column indices
    foreach ($col_headers as $col_index => $col_name) {
        $col_name_ = str_replace(' ', '__', strtolower(trim($col_name)));
        ${$col_name_} = $col_index;
    }

    $profile_data = array();
    $profile_extra_data = array();

    //Loop through the profiles
    $i = 1;
    foreach ($dataArray as $studentProfile) {
        $profile_data[] = "('{$studentProfile[$admission__number]}', '{$studentProfile[$surname]}', '{$studentProfile[$first__name]}', '{$studentProfile[$other__names]}', '" . strtolower($studentProfile[$surname]) . "')";

        $section = Sections::getSectionByName($connect, $studentProfile[$class]);
        $_class = Classes::getClassByName($connect, $studentProfile[$level]);
        $_arm = Arms::getArmByName($connect, $studentProfile[$arm]);
        
        $profile_extra_data[$studentProfile[$admission__number]] = "('{$_SESSION['current_session_term']['session_term_id']}', 'null', '" . ($section ? $section->faculties_id : '') . "', '" . ($_class ? $_class->departments_id : '') . "', '" . ($_arm ? $_arm->programme_id : '') . "')";
    }

    $sql = "INSERT IGNORE INTO studentprofile (admission_no, lastname, firstname, othernames, password) VALUES " . implode(', ', $profile_data);
    mysql_query($sql, $connect) or die(mysql_error());
    $profile_records_created = mysql_affected_rows();

    //fetch the student Ids just generated
    $sql = "SELECT id, admission_no FROM studentprofile";
    $result = mysql_query($sql, $connect) or die(mysql_error());

    //insert the student Id into the profile_extra record for insertion
    if ($result && mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_object($result)) {
            if (array_key_exists($row->admission_no, $profile_extra_data)) {
                $profile_extra_data[$row->admission_no] = str_replace('null', $row->id, $profile_extra_data[$row->admission_no]);
            }
        }
    }

    $sql = "INSERT IGNORE INTO studentprofile_extra (session_term_id, student_id, faculty_id, department_id, programme_id) VALUES " . implode(', ', $profile_extra_data);
    mysql_query($sql) or die(mysql_error());
    $profile_extra_records_created = mysql_affected_rows();

    if ($profile_records_created && $profile_extra_records_created)
        $msg = $profile_records_created . " pupils data uploaded successfully.";
}

?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Pupils Bulk Upload</h1>
                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";
                ?>

                <form method="post" class='form' action="" enctype="multipart/form-data">
                    <input type="file" name="upload_file">
                    <button type="submit" name="upload">Upload</button>
                </form>
            </td>
        </tr>
    </table>
</td>
</tr>

<?php require("footer.php"); ?>

</body>
</html>

