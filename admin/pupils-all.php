<?php require("header_leftnav.inc.php"); ?>

<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">
<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>
<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<style>
    #myTable a {
        color: blue;
    }
</style>
<td valign="top" class="page-content">
    <!-- <p>Welcome, please use the navigation links to your left to perform necessary action</p> -->

    <?php
    include_once '../autoload.php';

    $sessions = Sessions::getAll($connect); //var_dump($sessions);
    ; //var_dump($terms);

    $currentSession = Sessions::current($connect);
    $currentTerm = Terms::currentSessionTerm($connect);//var_dump($sessionID);

    $sessionID = @$_GET['session'] ?: $currentSession;


    if (!@$_GET['session'] or (@$_GET['session'] == $currentSession and !@$_GET['term'])) {
        $termID = $currentTerm;
    } else {
        $termID = @$_GET['term'] ?: -1;
    }


    $terms = Terms::getSessionTerms($connect, $sessionID);


    @$sectionID = $_GET['section'];
    @$classID = $_GET['class'];
    @$armID = $_GET['arm'];
    //var_dump(Arms::getAll($connect));
    $sections = Sections::getAll($connect);
    $classes = Classes::getAll($connect);
    $arms = Arms::getAll($connect);

    //var_dump($termID, $sectionID, $classID, $armID);
    $data = Student::getAll($connect, $termID, $sectionID, $classID, $armID);
    ?>
    <form name="pupilSelectForm" style="display:inline">
        <label>
            <b>Session: </b>
            <select name="session" onChange="pupilSelectForm.submit()">
                <option value="<?php print $currentSession ?>">Current Session</option>
                <?php foreach ($sessions as $row):
                    $select = ($sessionID == $row['session_id']) ? 'selected' : '';
                    ?>
                    <option
                        value="<?php print $row['session_id']?>" <?php print $select?>><?php print $row['session_name']?></option>
                <?php endforeach; ?>
            </select>
        </label>
    </form>
    <form style="display:inline">
        <input type="hidden" name="session" value="<?php print $sessionID ?>">
        <label><b>Term:</b>
            <select name="term">
                <?php if ($currentSession == $sessionID): ?>
                    <option value="<?php print $currentTerm ?>">Current Term</option>
                <?php else: print '<option></option>'; endif; ?>
                <?php foreach ($terms as $row):
                    $select = ($termID == $row['term_id']) ? 'selected' : '';
                    ?>
                    <option
                        value="<?php print $row['term_id']?>" <?php print $select?>><?php print $row['term_fullname']?></option>
                <?php endforeach; ?>
            </select>
        </label>
        <hr>
        <label>
            <b>Section: </b>
            <select name="section">
                <option value="">All</option>
                <?php foreach ($sections as $row):
                    $select = ($sectionID == $row['faculties_id']) ? 'selected' : '';
                    ?>
                    <option
                        value="<?php print $row['faculties_id']?>" <?php print $select?>><?php print $row['faculties_name']?></option>
                <?php endforeach; ?>

            </select>
        </label>
        <label>
            <b>Class: </b>
            <select name="class">
                <option value="">All</option>
                <?php foreach ($classes as $row):
                    $select = ($classID == $row['departments_id']) ? 'selected' : '';
                    ?>
                    <option
                        value="<?php print $row['departments_id']?>" <?php print $select?>><?php print $row['departments_name']?></option>
                <?php endforeach; ?>
            </select>
        </label>
        <label>
            <b>Arm: </b>
            <select name="arm">
                <option value="">All</option>
                <?php foreach ($arms as $row):
                    $select = ($armID == $row['programme_id']) ? 'selected' : '';
                    ?>
                    <option
                        value="<?php print $row['programme_id']?>" <?php print $select?>><?php print $row['programme_name']?></option>
                <?php endforeach; ?>
            </select>
        </label>
        <button type="submit">Show List</button>
    </form>
    <hr>
    <?php if ($data) { ?>
        <!--<form action="bill-generate-list.php" method="post" target="_blank">
            <input type="hidden" name="sessionID" value="<?php print $sessionID ?>">
            <input type="hidden" name="termID" value="<?php print $termID ?>">
            <input type="hidden" name="sectionID" value="<?php print $sectionID ?>">
            <input type="hidden" name="classID" value="<?php print $classID ?>">
            <input type="hidden" name="armID" value="<?php print $armID ?>">
            <button type="submit">Generate Excel Sheet</button>
        </form>
        <hr>-->
    <?php } ?>

    <!-- admission Number, fullname class(section, class, arm), Bill items -->
    <table id="myTable">
        <thead>
        <tr>
            <th>ADMISSION NO</th>
            <th>NAME</th>
            <!--<th>CLASS</th>-->
            <th>ACTION</th>
        </tr>
        </thead>
        <?php foreach ($data as $row):
            $fullClassName = "{$row['faculties_name']} {$row['departments_name']} {$row['programme_name']}";
            ?>
            <tr>
                <td><?php print $row['admission_no']?></td>
                <td><?php print "{$row['lastname']} {$row['firstname']} {$row['othernames']}"?></td>
                <!--<td><?php print $fullClassName?></td>-->
                <td><a href="billing.php?admissionNo=<?php print $row['admission_no']?>">View Billing</a><br>
                    <a href="pupil-profile.php?admissionNo=<?php print $row['admission_no']?>">View Profile</a>
                </td>
            </tr>
        <?php endforeach; ?>
        <tbody>
        </tbody>


    </table>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
<script>
    $(document).ready(function () {
        $('#myTable').dataTable({
            //dom: 'Bfrtip', buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print' ]
        });
    });
</script>
</body>
</html>
