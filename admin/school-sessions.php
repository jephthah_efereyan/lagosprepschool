<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/8/2016
 * Time: 12:05 PM
 */
require("header_leftnav.inc.php");

$msg = $start_date = $end_date = $cancel_edit = "";
$form_heading = 'Add Session';

# when save-session form is submitted, either add or edit
if (isset($_POST['save_session'])) {
    //die('<pre>'.print_r($_POST,1));
    if (!empty($_POST['session_name'])) { #add a session
        # create the session
        $session_name = $_POST['session_name'];
        $session_fullname = $_POST['session_name'] . "/" . ((int)$_POST['session_name'] + 1);

        $sql = "INSERT INTO school_sessions SET session_name = '{$session_name}', session_fullname = '{$session_fullname}', added_on = NOW(), added_by = {$_SESSION['user']['id']}, modified_by = {$_SESSION['user']['id']}";
        mysql_query($sql);
        if (mysql_affected_rows() > 0) {
            # create the session-terms
            $session_id = mysql_insert_id();
            $session_terms_data = array(
                "({$session_id}, 1, NOW(), {$_SESSION['user']['id']}, {$_SESSION['user']['id']})",
                "({$session_id}, 2, NOW(), {$_SESSION['user']['id']}, {$_SESSION['user']['id']})",
                "({$session_id}, 3, NOW(), {$_SESSION['user']['id']}, {$_SESSION['user']['id']})"
            );
            $sql = "INSERT INTO session_terms (session_id, term_id, added_on, added_by, modified_by) VALUES " . implode(', ', $session_terms_data);
            mysql_query($sql);
            if (mysql_affected_rows() > 0)
                $msg = 'Session <b>' . $session_fullname . '</b> added successfully';
        }
    }
    elseif (!empty($_POST['session_id'])) { #edit a session
        $sql = "UPDATE school_sessions SET start_date = '$_POST[start_date]', end_date = '$_POST[end_date]', modified_by = {$_SESSION['user']['id']} WHERE session_id = {$_POST['session_id']}";
        mysql_query($sql);
        if (mysql_affected_rows() > 0)
            $msg = 'Session modified successfully';
    }
}
elseif (isset($_POST['set_current_session'])) {
    # when set-current-session form is submitted
    $session_id = $_POST['session_id'];
    $term_id = $_POST['term_id'];
    $sql = "UPDATE session_terms SET status = CASE
            WHEN session_id = {$session_id} AND term_id = {$term_id} THEN 'Current'
            WHEN session_id != {$session_id} AND term_id != {$term_id} THEN NULL
            END";
    if (mysql_query($sql))
        $msg = 'Current Session set successfully';

    $_SESSION['current_session_term'] = GetCurrentSessionTerm();
}

# Get details of a session to edit
if (!empty($_GET['id'])) {
    $cancel_edit = '<a href="session-setup.php">Cancel</a>';
    $form_heading = 'Modify Session';
    $sql = "SELECT * FROM school_sessions WHERE session_id = " . (int)$_GET['id'];
    $result = mysql_query($sql);
    if ($result) {
        $session = mysql_fetch_assoc($result);
        $session_name = $session['session_name'];
        $session_fullname = $session['session_fullname'];
        $start_date = $session['start_date'];
        $end_date = $session['end_date'];
    }
}

# get list of sessions
$sql = "SELECT * FROM school_sessions";
$result = mysql_query($sql);
?>

<td valign="top" class="page-content">
    <h3 class='page-title'>SESSIONS</h3>
    <?php
    if (!empty($msg))
        echo "<div class='msg'><p>" . $msg . "</p></div>";
    ?>
    <fieldset>
        <legend>School Sessions</legend>
        <form method="post">
            <h4><?php echo $form_heading ?></h4>
            Session:
            <?php
            if (!empty($_GET['id'])) {
                echo '<b>' . $session_fullname . '</b> &nbsp;';
                echo '<input type="hidden" name="session_id" value="' . (int)$_GET['id'] . '">';
            }
            else {
                ?>
                <select name="session_name">
                    <option value="">--Select Session--</option>
                    <?php
                    $_result = mysql_query("SELECT MAX(session_name) AS session_name FROM school_sessions");
                    $max_session_name = mysql_result($_result, 0);
                    //for ($year = $max_session_name + 1; $year <= $max_session_name + 2; $year ++)
                    for ($year = date('Y') - 1; $year <= date('Y') + 1; $year ++)
                        echo "<option value='$year'>" . $year . '/' . ((int)$year + 1) . "</option>";
                    ?>
                </select>
            <?php } ?>
            <!--Start Date:
            <input type="date" name="start_date" value="<?php /*echo $start_date */ ?>">
            End Date:
            <input type="date" name="end_date" value="<?php /*echo $end_date */ ?>">-->
            <button type="submit" name="save_session">Save</button>
            <?php echo $cancel_edit ?>
        </form>

        <hr>
        <?php
        if ($result && mysql_num_rows($result) > 0) { ?>
        <table border="0" cellspacing="0" cellpadding="5" class="list">
            <thead>
            <tr bgcolor=#CCCCCC>
                <th>#</th>
                <th>Session Name</th>
                <th>Session Full Name</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Action</th>
            </tr>
            </thead>

            <tbody>
            <?php
                $i = 0;
                while ($session = mysql_fetch_assoc($result)) {
                    $i ++;
                    $row_style = $i % 2 == 0 ? 'even' : "odd";
                    $edit = "<a href='session-setup.php?id=$session[session_id]' class='blue'>Edit</a>";
                    echo '<tr class="' . $row_style . '">
                            <td>' . $i . '</td>
                            <td>' . $session['session_name'] . '</td>
                            <td>' . $session['session_fullname'] . '</td>
                            <td>' . $session['start_date'] . '</td>
                            <td>' . $session['end_date'] . '</td>
                            <td>' . $edit . '</td>
                        </tr>';
                }
            ?>
            </tbody>
        </table>
        <?php } else {
            echo '<div>No session found.</div>';
        }
        ?>
    </fieldset>
    <br><br>
    <fieldset>
        <legend>School Terms</legend>
        <table border="0" cellspacing="0" cellpadding="5" class="list">
            <thead>
            <tr bgcolor=#CCCCCC>
                <th>#</th>
                <th>Term Name</th>
                <th>Term Full Name</th>
            </tr>
            </thead>

            <tbody>
            <?php
            $result = mysql_query("SELECT * FROM school_terms");
            if ($result) {
                $i = 0;
                while ($term = mysql_fetch_assoc($result)) {
                    $i ++;
                    $row_style = $i % 2 == 0 ? 'even' : "odd";
                    echo '<tr class="' . $row_style . '">
                            <td>' . $i . '</td>
                            <td>' . $term['term_name'] . '</td>
                            <td>' . $term['term_fullname'] . '</td>
                        </tr>';
                }
            }
            ?>
            </tbody>
        </table>
    </fieldset>
    <br><br>
    <fieldset>
        <legend>Current Session-Term</legend>
        <table border="0" cellspacing="0" cellpadding="5" class="list">
            <thead>
            <tr bgcolor=#CCCCCC>
                <th>Current Session</th>
                <th>Current Term</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>
            </thead>

            <tbody>
            <?php
            $sql = "SELECT st.*, s.session_fullname, t.term_fullname FROM session_terms st JOIN school_sessions s ON st.session_id = s.session_id JOIN school_terms t ON st.term_id = t.term_id WHERE st.status = 'Current'";
            $result = mysql_query($sql) or die(mysql_error());
            if ($result) {
                $session_term = mysql_fetch_assoc($result);
                echo '<tr>
                        <td>' . $session_term['session_fullname'] . '</td>
                        <td>' . $session_term['term_fullname'] . '</td>
                        <td>' . $session_term['start_date'] . '</td>
                        <td>' . $session_term['end_date'] . '</td>
                    </tr>';
            }
            ?>
            </tbody>
        </table>

        <form method="post">
            <h4>Set Current Session-Term</h4>
            Session:
            <select name="session_id">
                <option value="">--Select Session--</option>
                <?php
                $sql = "SELECT DISTINCT st.session_id, s.session_fullname FROM session_terms st JOIN school_sessions s ON st.session_id = s.session_id WHERE st.status IS NULL";
                $result = mysql_query($sql);
                while ($row = mysql_fetch_assoc($result))
                    echo "<option value='$row[session_id]'>" . $row[session_fullname] . "</option>";
                ?>
            </select>

            Term:
            <select name="term_id">
                <option value="">--Select Term--</option>
                <?php
                $sql = "SELECT * FROM school_terms";
                $result = mysql_query($sql);
                while ($row = mysql_fetch_assoc($result))
                    echo "<option value='$row[term_id]'>" . $row[term_fullname] . "</option>";
                ?>
            </select>
            <!--&nbsp;&nbsp;
            Start Date:
            <input type="date" name="start_date" value="<?php /*echo $start_date */?>">
            &nbsp;&nbsp;
            End Date:
            <input type="date" name="end_date" value="<?php /*echo $end_date */?>">-->
            &nbsp;&nbsp;
            <button type="submit" name="set_current_session">Save</button>
            <?php echo $cancel_edit ?>
        </form>
    </fieldset>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>

<script>
    function filter_teachers(a)
    location.href = "<?php echo $_SERVER['PHP_SELF'] ?>" + "?_status=" + a;
</script>