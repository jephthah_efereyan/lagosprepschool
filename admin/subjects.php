<?php
require("header_leftnav.inc.php");

$subject_name = "";
$label = "Add Subject";
$cancel = "";
$action = "";

if (isset($_POST['save_subject'])) {
    $_POST['subject_name'] = trim($_POST['subject_name']);
    $_SESSION['subjects'] = $_POST;

    $sql = "SELECT * FROM subjects WHERE name = '{$_POST['subject_name']}'";
    if (!empty($_GET['subjectID']))
        $sql .= " AND id != " . (int)$_GET['subjectID'];
    $resource = mysql_query($sql);

    if (mysql_num_rows($resource) > 0)
        $msg = "<b>{$_POST['subject_name']}</b> already exists";
    else {
        if (isset($_GET['action']) && $_GET['action'] == "edit_subject" && !empty($_GET['subjectID'])) {
            $resource = mysql_query("SELECT name FROM subjects WHERE id = " . (int)$_GET['subjectID']);
            $old_subject_name = mysql_result($resource, 0);
            
            $sql = "UPDATE subjects SET name = '{$_POST['subject_name']}' WHERE id = " . (int)$_GET['subjectID'];
        }
        else
            $sql = "INSERT INTO subjects (name, added_on, enabled) VALUE ('{$_POST['subject_name']}', NOW(), TRUE)";
        mysql_query($sql) or die(mysql_error());
        if (mysql_affected_rows() > 0) {
            unset($_SESSION['subjects']);
            if (isset($_GET['action']) && $_GET['action'] == "edit_subject")
                $msg = "<b>{$old_subject_name}</b> changed to <b>{$_POST['subject_name']}</b> successfully";
            else
                $msg = "<b>{$_POST['subject_name']}</b> added successfully";
        }
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'enable_subject' && !empty($_GET['subjectID'])) {
    $sql = "UPDATE subjects SET enabled = IF (enabled = TRUE, FALSE, TRUE) WHERE id = " . (int)$_GET['subjectID'];
    mysql_query($sql);
    if (mysql_affected_rows() > 0) {
        $sql = "SELECT IF(enabled = TRUE, 'enabled', 'disabled') AS status FROM subjects WHERE id = " . (int)$_GET['subjectID'];
        $result = mysql_query($sql);
        $msg = "Subject " . mysql_result($result, 0) . " successfully";
    }
}

if (isset($_GET['action']) && $_GET['action'] == "edit_subject" && !empty($_GET['subjectID'])) {
    $resource = mysql_query("SELECT * FROM subjects WHERE id = {$_GET['subjectID']}");
    $this_subject = mysql_fetch_assoc($resource);
    $subject_name = $this_subject['name'];
    $action = "action=edit_subject&subjectID=" . $this_subject['id'];
    $label = "Change Subject";
    $cancel = "<a href='subjects.php'  class='a'>Cancel</a>";
}

$sql = "SELECT * FROM subjects ORDER BY name";
$resource = mysql_query($sql) or die(mysql_error());

if (!empty($_SESSION['subjects'])) {
    extract($_SESSION['subjects']);
    unset($_SESSION['subjects']);
}
?>

<td valign="top">
    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <h1 class="title">Subjects</h1>

                <form method="post" class='form' action="?<?php echo $action ?>">
                    <?php echo $label?>
                    <input type="text" name="subject_name" value="<?php echo $subject_name ?>" required="required">
                    <button type="submit" name="save_subject">Save</button>
                    <?php echo $cancel ?>
                </form>

                <?php
                if (!empty($msg))
                    echo "<div class='msg'><p>{$msg}<p></div>";

                if (mysql_num_rows($resource) > 0) {
                    ?>
                    <table border=0 align=left cellpadding=5 cellspacing=0 class="list">
                        <tr>
                            <th>#</th>
                            <th>Subject Name</th>
                            <th>Enabled</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $index = 0;
                        while ($subject = mysql_fetch_assoc($resource)) {
                            $enabled = $subject['enabled'] ? "Yes" : "No";
                            $enabled_class = $subject['enabled'] ? "label-success" : "label-danger";
                            $enable = $subject['enabled'] ? "Disable" : "Enable";
                            ?>
                            <tr>
                                <td><?php echo ++ $index ?></td>
                                <td><?php echo $subject["name"] ?></td>
                                <td><span class="label <?php echo $enabled_class ?>"><?php echo $enabled ?></span></td>
                                <td>
                                    <a href="?action=edit_subject&subjectID=<?php echo $subject['id'] ?>" class="a">Edit</a>
                                    ::
                                    <a href="?action=enable_subject&subjectID=<?php echo $subject['id'] ?>" class="a"><?php echo $enable ?></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                <?php }
                else
                    echo "<p>No subject found</p>";
                ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>
