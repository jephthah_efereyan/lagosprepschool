<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/6/2015
 * Time: 2:06 PM
 */

require("header_leftnav.inc.php");

$action = "add";
$heading = "ADD";
$button = "Add";
$link = "";

if (isset($_GET['result'])) {
    $sql = "SELECT * FROM wassce_results WHERE wassce_result_id = {$_GET['result']}";
    $resource = mysql_query($sql);
    $result = mysql_fetch_assoc($resource);
    $wassce_result_year = $result['wassce_result_year'];
    $published = $result['published'];

    $action = "edit&result=" . $result['wassce_result_id'];
    $heading = "MODIFY";
    $button = "Modify";
    $link = "<a href='wassce-result.php' class='blue'>Cancel</a>";
}

$query = "SELECT * FROM wassce_results ORDER BY wassce_result_year DESC";
$wassce_results = mysql_query($query);

$msg = isset($_SESSION['st']) ? $_SESSION['st'] : "";
unset($_SESSION['st']);

if(isset($_SESSION['form'])) {
    extract(($_SESSION['form']));
    unset($_SESSION['form']);
}
?>

<td valign="top" class="page-content">
    <h3 class='page-title'>WASSCE RESULTS</h3>
    <?php
    if (!empty($msg))
        echo "<div class='msg'><p>" . $msg . "</p></div>";
    ?>

    <form action="process.php?module=wassce_result&action=<?php print $action ?>" method="post" enctype="multipart/form-data" class="inlineform">
        <span><b><?php print $heading ?> RESULT</b></span>
        &nbsp;&nbsp;
        Year
        <select name="wassce_result_year">
            <?php
            for($year = date("Y"); $year >= (date("Y") - 10); $year--) {
                echo "<option";
                echo isset($wassce_result_year) && $wassce_result_year == $year ? " selected": "";
                echo ">{$year}</option>";
            }
            ?>
        </select>
        &nbsp;&nbsp;
        Published
        <select name="published">
            <?php
            $published_status = array("yes", "no");
            foreach ($published_status as $status) {
                echo "<option value='{$status}'";
                echo isset($published) && $published == $status ? " selected" : "";
                echo ">" . ucfirst($status) . "</option>";
            }
            ?>
        </select>
        &nbsp;&nbsp;
        File
        <input type="file" name="result_file" />
        &nbsp;&nbsp;
        <button type="submit"><?php print $button ?></button>
        <?php print $link ?>
    </form>

    <table border="0" cellspacing="0" cellpadding="5" class="list">
        <thead>
        <tr bgcolor=#CCCCCC>
            <th>#</th>
            <th>Year</th>
            <th>Published</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if (mysql_num_rows($wassce_results) > 0) {
            $i = 0;
            while ($wassce_result = mysql_fetch_assoc($wassce_results)) {
                $i ++;
                $row_style = $i % 2 == 0 ? 'even' : "odd";
                $published = $wassce_result["published"] == 'yes' ? "<span class='green'>Yes</span>" : "<span class='red'>No</span>";
                $text = $wassce_result["published"] == 'yes' ? "Unpublish" : "Publish";
                $action = "<a href='process.php?module=wassce_result&action=publish&result={$wassce_result['wassce_result_id']}' class='blue' onClick=\"return confirmLink(this, '" . strtoupper($text) . " this?')\">{$text}</a> |
                                            <a href='?result={$wassce_result['wassce_result_id']}' class='blue'>Edit</a>";
                ?>
                <tr class="<?php print $row_style ?>">
                    <td><?php print $i ?></td>
                    <td><?php print $wassce_result['wassce_result_year'] ?></td>
                    <td><?php print $published ?></td>
                    <td><?php print $action ?></td>
                </tr>
                <?php
            }
        } else
            print "<tr><td colspan='4'>No results have been uploaded yet</td></tr>";
        ?>
        </tbody>
    </table>
</td>
</tr>
</table>

<?php require("footer.php"); ?>
</body>
</html>

<script>
    function filter_teachers(a) {
        location.href = "<?php echo $_SERVER['PHP_SELF'] ?>" + "?_status=" + a;
    }
</script>