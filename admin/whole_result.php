<?php
include_once("auth.inc.php");
include_once("config.php");
include_once("lang.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><? echo "$bk_website_name: "; ?> Result Management</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="adminstyle.css" type="text/css" rel="stylesheet">
<!- content starts here -->
<script language="JavaScript" type="text/JavaScript">
<!--
/**
 * Displays an confirmation box before to submit a "DROP DATABASE" query.
 * This function is called while clicking links
 *
 * @param   object   the link
 * @param   object   the sql query to submit
 *
 * @return  boolean  whether to run the query or not
 */
function confirmLinkDropDB(theLink, theSqlQuery)
{
    // Confirmation is not required in the configuration file
    // or browser is Opera (crappy js implementation)
    if (confirmMsg == '' || typeof(window.opera) != 'undefined') {
        return true;
    }

    var is_confirmed = confirm(confirmMsgDropDB + '\n' + confirmMsg + ' :\n' + theSqlQuery);
    if (is_confirmed) {
        theLink.href += '&is_js_confirmed=1';
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

/**
 * Displays an confirmation box beforme to submit a "DROP/DELETE/ALTER" query.
 * This function is called while clicking links
 *
 * @param   object   the link
 * @param   object   the sql query to submit
 *
 * @return  boolean  whether to run the query or not
 */
function confirmLink(theLink, theSqlQuery)
{
    // Confirmation is not required in the configuration file
    // or browser is Opera (crappy js implementation)
    if (confirmMsg == '' || typeof(window.opera) != 'undefined') {
        return true;
    }

    var is_confirmed = confirm(confirmMsg + ' \n' + theSqlQuery);
    if (is_confirmed) {
        theLink.href += '&is_js_confirmed=1';
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

    // js form validation stuff
    var confirmMsg  = 'Do you really want to ';

//-->
</script>
<script type="text/JavaScript">
<!--
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
</head>

<body>
<b style="color: darkblue; font-size: 16px;"><u>Result Management</u></b>
<br>
<form id="form1" name="form1" method="get" action="">

  <table width="60%" border="0" cellspacing="0" cellpadding="5" style="border: 1px solid #333333;">
    <tr>
      <td><strong>Search Student</strong></td>
    </tr>
    <tr>
      <td>Admission No: 
        <input name="adms" type="text" id="adms" value="<? echo $adms ?>"> 
Surname: 
        <input name="ymsname" type="text" id="ymsname" value="<? echo $ymsname ?>"> 
        Term: 
        <select name="ymterm" id="ymterm">
          <option value="">Select</option>
          <option value="firstterm" <? if ($ymterm == firstterm) {echo "selected"; } ?>>First Term</option>
          <option value="secondterm" <? if ($ymterm == secondterm) {echo "selected"; } ?>>Second Term</option>
          <option value="thirdterm" <? if ($ymterm == thirdterm) {echo "selected"; } ?>>Third Term</option>
        </select>
         Session: <select name="ymsession" id="ymsession">
<option value="">Select</option>
<?php
$tillyear = 2006;
for ($year = date(Y); $year >= $tillyear; $year--) {
?>
    <option value="<? echo $year ?>"  <? if ($year == $ymsession) {echo "selected"; } ?>><? echo $year ?>/<? echo $year+1 ?></option>
<?
}
?>
          </select></td>
    </tr>
    <tr>
      <td><input type="submit" name="Submit" value="Search" /></td>
    </tr>
  </table>
</form>
<form name="form" method="post" action="">
<?php
if ($update_r == "Update Result") {
require("update_result.php");
}
?>
<table width="100%"  border="0" cellspacing="0" cellpadding="1" class="text">
   <tr bgcolor=#cccccc>
    <td colspan="12"><strong><? echo "$w_update" ?></strong></td>
  </tr>
   <tr bgcolor=#cccccc>
    <td colspan="12">&nbsp;</td>
  </tr>
<?
//set the number of columns
$columns = 1;
if (!$rowstart) { $rowstart=0;}

$ending = 100;

if ($adms) {
$qr .= "  AND
	studentsubjects.ss_admission = '$adms'";
}

if ($ymsession) {
$qr .= "  AND
	studentsubjects.s_session = '$ymsession'";
}

if ($ymterm) {
$qr .= "  AND
	studentsubjects.s_term = '$ymterm'";
}

if ($ymsname) {
$qr .= "  AND
	studentprofile.lastname = '$ymsname'";
}

$query ="SELECT studentprofile.lastname,studentprofile.firstname,studentprofile.othernames,studentprofile_extra.class_level,studentprofile_extra.class_no,studentprofile_extra.class_letter,studentsubjects.s_id,studentsubjects.ss_admission,studentsubjects.subject,studentsubjects.testmark1,studentsubjects.testmark2,studentsubjects.finalexam,studentsubjects.s_term,studentsubjects.s_session,studentsubjects.class
	FROM studentsubjects,studentprofile,studentprofile_extra
	WHERE studentsubjects.ss_admission = studentprofile.studentprofile_id AND
	studentsubjects.ss_admission = studentprofile_extra.studentprofile_id AND
	studentsubjects.s_session = studentprofile_extra.yearsession AND
	studentsubjects.s_term = studentprofile_extra.term
	$qr
	ORDER BY studentprofile.lastname,studentsubjects.ss_admission
	LIMIT $rowstart,$ending";

$query2 = "SELECT studentprofile.lastname,studentprofile.firstname,studentprofile.othernames,studentprofile_extra.class_level,studentprofile_extra.class_no,studentprofile_extra.class_letter,studentsubjects.s_id,studentsubjects.ss_admission,studentsubjects.subject,studentsubjects.testmark1,studentsubjects.testmark2,studentsubjects.finalexam,studentsubjects.s_term,studentsubjects.s_session,studentsubjects.class
	FROM studentsubjects,studentprofile,studentprofile_extra
	WHERE studentsubjects.ss_admission = studentprofile.studentprofile_id AND
	studentsubjects.ss_admission = studentprofile_extra.studentprofile_id AND
	studentsubjects.s_session = studentprofile_extra.yearsession AND
	studentsubjects.s_term = studentprofile_extra.term
	$qr
	ORDER BY studentprofile.lastname,studentsubjects.ss_admission";

$result = mysql_query($query)
or die(mysql_error());
$result2 = mysql_query($query2)
or die(mysql_error());
$num2 = mysql_num_rows($result2);
$num_rows = mysql_num_rows($result2);
$num = mysql_num_rows($result);

$thisrows=mysql_num_rows($result);
$allrows=mysql_num_rows($result2);
$rowbegin=$rowstart+1;
$rowend=$rowstart+$thisrows;

//put all the code here for running the script itself using $result in the while loop

//we add this line because we need to know the number of rows


if ($num > 1) {
$themembers = "Records";
}
else {
$themembers = "Record";
}
?>
<?
if ($num != 0) {
?>
       <tr>
        <td colspan="6"><strong><? echo "Showing $rowbegin - $rowend of $allrows $themembers"; ?></strong></td>
      <td colspan="6" align=right><input name="update_r" type="submit" id="update_r" style="background-color:#006699; color:#FFFFFF; font-family:Tahoma, Verdana, Arial; font-size:11px; border:none; height:20px" onClick="return confirmLink(this, 'UPDATE this/these?')" value="Update Result"></td>
      </tr>
 <tr bgcolor=#999999>
    <td><strong>S/No</strong></td>
    <td><strong>Admission No</strong></td>
    <td><strong>Surname</strong></td>
    <td><strong>First Name</strong></td>
    <td><strong>Othernames</strong></td>
    <td><strong>Term</strong></td>
    <td><strong>Session</strong></td>
    <td><strong>Present Class</strong></td>
	<td><strong>Subject</strong></td>
    <td><strong>&nbsp;&nbsp;CA 1</strong></td>
    <td><strong>&nbsp;&nbsp;CA 2</strong></td>
    <td><strong>&nbsp;&nbsp;Exam</strong></td>
  </tr>
<?
}
?>
<?
for ($i=1+$rowstart; $i<=$num+$rowstart; $i++) {
if ($i%2 ==0) {$bgcolor = '#f0f0f0';} else {$bgcolor = '#cccccc';} 
$row = mysql_fetch_array($result);
$row2 = mysql_fetch_array($result2);
extract($row);


$surname = stripslashes($lastname);
$firstname = stripslashes($firstname);
$othernames = stripslashes($othernames);

$encsurname = urlencode($surname);
$encfirstname = urlencode($firstname);
$encothernames = urlencode($othernames);

$encfullname = strtoupper(trim("$surname $firstname $othernames"));

if ($testmark1 > 20) {
$star = "<span style='color: red'>*</span>";
$bgcolor = "#FFCC00";
}
else {
$star = "&nbsp;&nbsp;";
$bgcolor = "$bgcolor";
}
if ($testmark2 > 20) {
$star2 = "<span style='color: red'>*</span>";
$bgcolor = "#FFCC00";
}
else {
$star2 = "&nbsp;&nbsp;";
$bgcolor = "$bgcolor";
}
if ($finalexam > 60) {
$star3 = "<span style='color: red'>*</span>";
$bgcolor = "#FFCC00";
}
else {
$star3 = "&nbsp;&nbsp;";
$bgcolor = "$bgcolor";
}
   if($i % $columns == 0) {
        //if there is no remainder, we want to start a new row
        echo "<TR>";
    }
    echo "<TD valign='top'>" ;
if ($s_id) {
?>
  <tr bgcolor=<? echo $bgcolor ?> >
    <td><? echo $i ?> &nbsp;&nbsp;<input type="checkbox" name="check[]" value="<? echo $s_id ?>"></td>
    <td><? echo $ss_admission ?><input type="hidden" name="matric[<? echo $s_id ?>]" value="<? echo $ss_admission ?>"></td>
    <td><b><? echo strtoupper($surname) ?></b></td>
    <td><? echo strtoupper($firstname) ?></td>
    <td><? echo strtoupper($othernames) ?></td>
    <td><? echo strtoupper($s_term) ?><input type="hidden" name="ds_term[<? echo $s_id ?>]" value="<? echo $s_term ?>"></td>
    <td><? echo $s_session ?>/<? echo $s_session+1 ?><input type="hidden" name="ds_session[<? echo $s_id ?>]" value="<? echo $s_session ?>"></td>
    <td><? echo $class_level ?>

<?
$queryb = "SELECT *
	FROM departments
	ORDER BY departments_name";
$resultb = mysql_query($queryb)
or die(mysql_error());
print " <select name=\"class_no[$s_id]\" id=\"class_no\">";
while ($row4 = mysql_fetch_assoc($resultb)) { 
$departments_name = $row4["departments_name"];
?>
<option value="<? echo $departments_name ?>" <? if($departments_name == $class) { echo "selected"; } ?>><? echo $departments_name ?></option>
<?
}
print "</select>";
?>

<?
$queryc = "SELECT *
	FROM programme
	ORDER BY programme_name";
$resultc = mysql_query($queryc)
or die(mysql_error());
print " <select name=\"class_letter[$s_id]\" id=\"class_letter\">";
while ($row5 = mysql_fetch_assoc($resultc)) { 
$programme_name = $row5["programme_name"];
?>
	<option value="<? echo $programme_name ?>" <? if($programme_name == $class_letter) { echo "selected"; } ?>><? echo $programme_name ?></option>
<?
}
print "</select>";
?>

</td>
    <td><? echo $subject ?></td>
    <td><? echo $star ?><input name="testmark1[<? echo $s_id ?>]" id="testmark1<? echo $s_id ?>" type="text" size="5" maxlength="5" value="<? echo $testmark1 ?>" onBlur="MM_validateForm('testmark1<? echo $s_id ?>','','NinRange0:20');return document.MM_returnValue"></td>
    <td><? echo $star2 ?><input name="testmark2[<? echo $s_id ?>]" id="testmark2<? echo $s_id ?>" type="text" size="5" maxlength="5" value="<? echo $testmark2 ?>" onBlur="MM_validateForm('testmark2<? echo $s_id ?>','','NinRange0:20');return document.MM_returnValue"></td>
    <td><? echo $star3 ?><input name="finalexam[<? echo $s_id ?>]" id="finalexam<? echo $s_id ?>" type="text" size="5" maxlength="5" value="<? echo $finalexam ?>" onBlur="MM_validateForm('finalexam<? echo $s_id ?>','','NinRange0:60');return document.MM_returnValue"></td>
  </tr>
<?
}
echo "</TD>";
    if(($i % $columns) == ($columns - 1) || ($i + 1) == $num_rows) {
        //if there is a remainder of 1, end the row
        //or if there is nothing left in our result set, end the row
        echo "</TR>";
    }
}
?>
</table>
</form>
<br>
<?
if ($num == 0) {
?>
<table width="98%"  border="0" cellpadding="5" cellspacing="0" align="center" class="text" id="border">
  <tr>
    <td align=left>
No Record Found
</td>
  </tr>
</table>
<br>
<?
}
?>
<table width="98%"  border="0" cellpadding="5" cellspacing="0" align="center" class="text" id="border">
  <tr>
    <td align=right>
<?


$numrows=mysql_num_rows($result);
if (($rowbegin>$numrows) AND ($numrows !=0))
{?>
        <A HREF="<? $php_self ?>?rowstart=<? echo $rowstart-$ending; ?>&od=<? echo $od ?>&adms=<? echo $adms ?>&ymsession=<? echo $ymsession ?>&ymterm=<? echo $ymterm ?>" style="color: darkblue"> < Previous</A> 
        <?}?>
<?
$num_rows=mysql_num_rows($result2);
if (($rowbegin>$numrows) AND ($rowstart+$ending<$num_rows))
{?>
        |
        <?}?>

        <?
$num_rows=mysql_num_rows($result2);
if($rowstart+$ending<$num_rows) 
{?>
        <A HREF="<? $php_self?>?rowstart=<?echo $rowstart+$ending; ?>&od=<? echo $od ?>&adms=<? echo $adms ?>&ymsession=<? echo $ymsession ?>&ymterm=<? echo $ymterm ?>" style="color: darkblue"> Next ></A> 
        <? } ?>
</td>
  </tr>
</table>
<br>


<!- content ends here -->
</body>
</html>