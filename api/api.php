<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 4/29/2016
 * Time: 8:26 AM
 */

require_once '../students_data/config.php';
include_once '../autoload.php';

$currentSessionTermID = @$_POST['sessionTerm'] ?: Terms::currentSessionTerm($connect);

header('Content-type: application/json; charset=UTF-8');

extract($_GET);

$retVal = array('status' => false, 'message' => '');

if ($f == 'getBillingItems') {
    $billing_items = Bills::listItems($connect);
    if ($billing_items) {
        $retVal['data'] = array();
        $i = 0;
        foreach ($billing_items as $item) {
            $item_ = array('sn' => ++$i, "id" => $item['bi_id'], "name" => $item['bi_name']);
            array_push($retVal['data'], $item_);
        }
    }

    $retVal['status'] = true;
}

echo json_encode($retVal);
exit;