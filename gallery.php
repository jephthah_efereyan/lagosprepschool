<?php
	include("header.php");
?>

<nav>
              <ul class="nav nav-justified">
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Courses</a></li>
                <li class="active"><a href="gallery.php">Gallery</a></li>
                <li><a href="#">Academic Staff</a></li>
                <li><a href="#">Students</a></li>
              </ul>
        </nav>

<link rel="stylesheet" type="text/css" href="ResponsiveImageGallery/css/demo.css" />
	<link rel="stylesheet" type="text/css" href="ResponsiveImageGallery/css/style.css" />
	<link rel="stylesheet" type="text/css" href="ResponsiveImageGallery/css/elastislide.css" />

<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
		</script>


<div id="rg-gallery" class="rg-gallery">
					<div class="rg-thumbs">
						<!-- Elastislide Carousel Thumbnail Viewer -->
						<div class="es-carousel-wrapper">
							<div class="es-nav">
								<span class="es-nav-prev">Previous</span>
								<span class="es-nav-next">Next</span>
							</div>
							<div class="es-carousel">
								<ul>
									<li><a href="#"><img src="images/thumbs/1.jpg" data-large="images/1.jpg" alt="image01" data-description="From off a hill whose concave womb reworded" /></a></li>
									<li><a href="#"><img src="images/thumbs/2.jpg" data-large="images/2.jpg" alt="image02" data-description="A plaintful story from a sistering vale" /></a></li>
									<li><a href="#"><img src="images/thumbs/3.jpg" data-large="images/3.jpg" alt="image03" data-description="A plaintful story from a sistering vale" /></a></li>
									<li><a href="#"><img src="images/thumbs/4.jpg" data-large="images/4.jpg" alt="image04" data-description="My spirits to attend this double voice accorded" /></a></li>
									<li><a href="#"><img src="images/thumbs/5.jpg" data-large="images/5.jpg" alt="image05" data-description="And down I laid to list the sad-tuned tale" /></a></li>
									<li><a href="#"><img src="images/thumbs/6.jpg" data-large="images/6.jpg" alt="image06" data-description="Ere long espied a fickle maid full pale" /></a></li>
									<li><a href="#"><img src="images/thumbs/7.jpg" data-large="images/7.jpg" alt="image07" data-description="Tearing of papers, breaking rings a-twain" /></a></li>
									<li><a href="#"><img src="images/thumbs/8.jpg" data-large="images/8.jpg" alt="image08" data-description="Storming her world with sorrow's wind and rain" /></a></li>
									<li><a href="#"><img src="images/thumbs/9.jpg" data-large="images/9.jpg" alt="image09" data-description="Upon her head a platted hive of straw" /></a></li>
									<li><a href="#"><img src="images/thumbs/10.jpg" data-large="images/10.jpg" alt="image10" data-description="Which fortified her visage from the sun" /></a></li>
									<li><a href="#"><img src="images/thumbs/11.jpg" data-large="images/11.jpg" alt="image11" data-description="Whereon the thought might think sometime it saw" /></a></li>
									<li><a href="#"><img src="images/thumbs/12.jpg" data-large="images/12.jpg" alt="image12" data-description="The carcass of beauty spent and done" /></a></li>
									<li><a href="#"><img src="images/thumbs/13.jpg" data-large="images/13.jpg" alt="image13" data-description="Time had not scythed all that youth begun" /></a></li>
									<li><a href="#"><img src="images/thumbs/14.jpg" data-large="images/14.jpg" alt="image14" data-description="Nor youth all quit; but, spite of heaven's fell rage" /></a></li>
									<li><a href="#"><img src="images/thumbs/15.jpg" data-large="images/15.jpg" alt="image15" data-description="Some beauty peep'd through lattice of sear'd age" /></a></li>
									<li><a href="#"><img src="images/thumbs/16.jpg" data-large="images/16.jpg" alt="image16" data-description="Oft did she heave her napkin to her eyne" /></a></li>
									<li><a href="#"><img src="images/thumbs/17.jpg" data-large="images/17.jpg" alt="image17" data-description="Which on it had conceited characters" /></a></li>
									<li><a href="#"><img src="images/thumbs/18.jpg" data-large="images/18.jpg" alt="image18" data-description="Laundering the silken figures in the brine" /></a></li>
									<li><a href="#"><img src="images/thumbs/19.jpg" data-large="images/19.jpg" alt="image19" data-description="That season'd woe had pelleted in tears" /></a></li>
									<li><a href="#"><img src="images/thumbs/20.jpg" data-large="images/20.jpg" alt="image20" data-description="And often reading what contents it bears" /></a></li>
									<li><a href="#"><img src="images/thumbs/21.jpg" data-large="images/21.jpg" alt="image21" data-description="As often shrieking undistinguish'd woe" /></a></li>
									<li><a href="#"><img src="images/thumbs/22.jpg" data-large="images/22.jpg" alt="image22" data-description="In clamours of all size, both high and low" /></a></li>
									<li><a href="#"><img src="images/thumbs/23.jpg" data-large="images/23.jpg" alt="image23" data-description="Sometimes her levell'd eyes their carriage ride" /></a></li>
									<li><a href="#"><img src="images/thumbs/24.jpg" data-large="images/24.jpg" alt="image24" data-description="As they did battery to the spheres intend" /></a></li>
								</ul>
							</div>
						</div>
						<!-- End Elastislide Carousel Thumbnail Viewer -->
					</div><!-- rg-thumbs -->
				</div><!-- rg-gallery -->
                
                <script type="text/javascript" src="ResponsiveImageGallery/js/jquery.tmpl.min.js"></script>
		<script type="text/javascript" src="ResponsiveImageGallery/js/jquery.easing.1.3.js"></script>
		<script src="ResponsiveImageGallery/js/jquery.elastislide.js"></script>
		<script type="text/javascript" src="ResponsiveImageGallery/js/gallery.js"></script>
        
        <div class="row" id="footer-bar">
                 <div class="col-sm-3 col-md-3">
                    <div class="external-links">
                        <h3>External Links</h3>
                        <ul>
                            <li><a href="#">Abuja Portal</a></li>
                            <li><a href="#">Nigerian law School, Abuja</a></li>
                            <li><a href="#">Law School Alumni</a></li>
                        </ul>
                    </div>
                 </div>
                 <div class="col-sm-3 col-md-3">
                    <div class="academic">
                        <h3>Academic</h3>
                        <ul>
                            <li><a href="#">Academic Calendar</a></li>
                            <li><a href="#">Course Catalogue</a></li>
                            <li><a href="#">Curriculum</a></li>
                        </ul>
                    </div>
                 </div>
                 <div class="col-sm-3 col-md-3">
                    <div class="student">
                        <h3>Student</h3>
                        <ul>
                            <li><a href="#">Student Associations</a></li>
                            <li><a href="#">Departments</a></li>
                            <li><a href="#">Student Journals</a></li>
                        </ul>
                    </div>
                 </div>
                 <div class="col-sm-3 col-md-3">
                    <div class="apply">
                        <h3>Apply</h3>
                        <ul>
                            <li><a href="#">Register Online</a></li>
                            <li><a href="#">Bar I Application</a></li>
                            <li><a href="#">Bar II Application</a></li>
                        </ul>
                    </div>
                 </div>
        </div>
	
<footer>
		<div class="row foot">
        	<div class="col-sm-8 col-md-8"><p>&copy;Nigerian Law School - Lagos Campus. All rights reserved</p></div>
            <div class="col-sm-4 col-md-4"><p>Designed by #trend</p></div>
        </div>
</footer>