/**
 * Created by jephthah.efereyan on 4/27/2016.
 */
(function () {
    var app = angular.module('App', []);

    function getSum(items) {
        var totalAmount = 0;

        angular.forEach(items, function (item, key) {
            totalAmount += item.amount ? parseFloat(item.amount) : 0;
        });

        return totalAmount;
    }

    app.controller('PaymentCtrl', function ($scope, $http) {

        var apiUrl = 'http://localhost:8181/lagosprepsch/api/';

        $scope.billingItems = [];

        $http.get(apiUrl + 'api.php?f=getBillingItems')
            .then(function (response) {
                if (response.data.status) {
                    $scope.billingItems = response.data.data;
                    $scope.totalAmount = getSum($scope.billingItems);
                }
            });

        $scope.enterAmount = function (amount) {
            $scope.totalAmount = getSum($scope.billingItems);
        };
    })
}());