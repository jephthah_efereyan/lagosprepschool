-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2016 at 04:37 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lagpreps_portal`
--
CREATE DATABASE IF NOT EXISTS `lagpreps_portal` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lagpreps_portal`;

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `user_id` int(2) NOT NULL,
  `user_name` varchar(15) NOT NULL DEFAULT '',
  `user_pswd` varchar(15) NOT NULL DEFAULT '',
  `user_email` varchar(150) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`user_id`, `user_name`, `user_pswd`, `user_email`) VALUES
(1, 'admin', 'l@pr3psc#', '');

-- --------------------------------------------------------

--
-- Table structure for table `admin_type`
--

DROP TABLE IF EXISTS `admin_type`;
CREATE TABLE `admin_type` (
  `a_id` int(11) NOT NULL,
  `admin_name` varchar(255) NOT NULL DEFAULT '',
  `admin_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_type`
--

INSERT INTO `admin_type` (`a_id`, `admin_name`, `admin_type`) VALUES
(1, 'Class Teacher', 0),
(2, 'Subject Teacher', 1),
(3, 'Dorm Tutor', 2),
(4, 'Super Admin', 3),
(5, 'ICT', 4),
(6, 'HOD', 5);

-- --------------------------------------------------------

--
-- Table structure for table `application_profile`
--

DROP TABLE IF EXISTS `application_profile`;
CREATE TABLE `application_profile` (
  `std_id` int(11) NOT NULL,
  `std_logid` int(11) NOT NULL DEFAULT '0',
  `matric_no` varchar(100) NOT NULL DEFAULT '',
  `surname` varchar(100) NOT NULL DEFAULT '',
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `othernames` varchar(100) NOT NULL DEFAULT '',
  `grade` varchar(255) NOT NULL DEFAULT '',
  `sex` varchar(6) NOT NULL DEFAULT '',
  `age` varchar(10) NOT NULL DEFAULT '',
  `birthdate` date NOT NULL DEFAULT '0000-00-00',
  `place_of_birth` varchar(100) NOT NULL DEFAULT '',
  `home_town` varchar(100) NOT NULL DEFAULT '',
  `local_gov` varchar(100) NOT NULL DEFAULT '',
  `state_of_origin` varchar(100) NOT NULL DEFAULT '',
  `nationality` varchar(100) NOT NULL DEFAULT '',
  `contact_address` varchar(255) NOT NULL DEFAULT '',
  `student_email` varchar(100) NOT NULL DEFAULT '',
  `father_name` varchar(255) NOT NULL DEFAULT '',
  `father_addy` varchar(255) NOT NULL DEFAULT '',
  `father_tel` varchar(100) NOT NULL DEFAULT '',
  `father_mail` varchar(100) NOT NULL DEFAULT '',
  `father_emp` varchar(255) NOT NULL DEFAULT '',
  `mother_name` varchar(255) NOT NULL DEFAULT '',
  `mother_addy` varchar(255) NOT NULL DEFAULT '',
  `mother_tel` varchar(100) NOT NULL DEFAULT '',
  `mother_mail` varchar(100) NOT NULL DEFAULT '',
  `mother_emp` varchar(255) NOT NULL DEFAULT '',
  `student_homeaddress` varchar(255) NOT NULL DEFAULT '',
  `student_mobiletel` varchar(100) NOT NULL DEFAULT '',
  `student_landtel` varchar(100) NOT NULL DEFAULT '',
  `next_of_kin` varchar(255) NOT NULL DEFAULT '',
  `nok_address` varchar(255) NOT NULL DEFAULT '',
  `nok_tel` varchar(100) NOT NULL DEFAULT '',
  `stdprogramme_id` int(11) NOT NULL DEFAULT '0',
  `stdfaculty_id` int(11) NOT NULL DEFAULT '0',
  `stddepartment_id` int(11) NOT NULL DEFAULT '0',
  `stdprogramme_type_id` int(11) NOT NULL DEFAULT '0',
  `std_custome1` mediumtext NOT NULL,
  `std_custome2` mediumtext NOT NULL,
  `std_custome3` mediumtext NOT NULL,
  `std_custome4` mediumtext NOT NULL,
  `std_custome5` mediumtext NOT NULL,
  `std_custome6` mediumtext NOT NULL,
  `std_custome7` mediumtext NOT NULL,
  `std_custome8` mediumtext NOT NULL,
  `std_custome9` text NOT NULL,
  `std_custome10` text NOT NULL,
  `std_custome11` varchar(100) NOT NULL DEFAULT 'PENDING',
  `std_custome12` text NOT NULL,
  `std_custome13` text NOT NULL,
  `std_custome14` text NOT NULL,
  `std_custome15` text NOT NULL,
  `std_custome16` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Table structure for table `billing_items`
--

DROP TABLE IF EXISTS `billing_items`;
CREATE TABLE `billing_items` (
  `bi_id` int(6) NOT NULL,
  `bi_name` varchar(150) NOT NULL DEFAULT '',
  `bi_is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `bi_added_on` datetime NOT NULL,
  `bi_added_by` int(11) NOT NULL,
  `bi_modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bi_modified_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `billing_items`
--

INSERT INTO `billing_items` (`bi_id`, `bi_name`, `bi_is_enabled`, `bi_added_on`, `bi_added_by`, `bi_modified_on`, `bi_modified_by`) VALUES
(1, 'Building Development Levy', 1, '2016-03-11 11:20:20', 0, '2016-03-11 10:20:20', 0),
(2, 'Common Entrance Lesson', 1, '2016-03-11 11:20:37', 0, '2016-03-11 10:20:37', 0),
(3, 'School Bus', 1, '2016-03-11 11:21:09', 0, '2016-03-11 10:22:51', 0),
(4, 'Lasgems (one-off payment to Lagos State)', 1, '2016-03-11 11:23:16', 0, '2016-03-11 10:23:16', 0),
(5, 'Tuition', 1, '2016-03-11 11:23:29', 0, '2016-03-11 10:23:29', 0),
(6, 'Year Book Levy', 1, '2016-03-11 11:25:04', 0, '2016-03-11 10:25:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills` (
  `bill_id` int(6) NOT NULL,
  `session_term_id` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `bill_added_on` datetime NOT NULL,
  `bill_added_by` int(11) NOT NULL,
  `bill_modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `bill_modified_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`bill_id`, `session_term_id`, `faculty_id`, `department_id`, `item_id`, `amount`, `bill_added_on`, `bill_added_by`, `bill_modified_on`, `bill_modified_by`) VALUES
(1, 1, 1, 1, 1, '2500.00', '2016-03-11 16:05:47', 1, '2016-03-11 15:05:47', 1),
(2, 1, 1, 1, 2, '3500.00', '2016-03-11 16:05:47', 1, '2016-03-11 15:05:47', 1),
(3, 1, 1, 1, 3, '1000.00', '2016-03-11 16:05:47', 1, '2016-03-11 15:05:47', 1),
(4, 1, 1, 2, 1, '1000.00', '2016-03-11 16:14:07', 1, '2016-03-11 15:14:07', 1),
(5, 1, 1, 2, 2, '1500.00', '2016-03-11 16:14:07', 1, '2016-03-11 15:35:02', 1),
(6, 1, 1, 2, 3, '500.00', '2016-03-11 16:14:07', 1, '2016-03-11 15:34:57', 1),
(7, 1, 1, 2, 5, '22000.00', '2016-03-11 16:14:07', 1, '2016-03-11 15:34:51', 1),
(8, 2, 1, 1, 1, '2500.00', '2016-03-11 16:21:35', 1, '2016-03-11 15:21:35', 1),
(9, 2, 1, 1, 2, '3000.00', '2016-03-11 16:21:35', 1, '2016-03-11 15:21:35', 1),
(10, 2, 1, 1, 3, '1500.00', '2016-03-11 16:21:35', 1, '2016-03-11 15:21:35', 1),
(11, 2, 1, 1, 5, '20000.00', '2016-03-11 16:21:35', 1, '2016-03-11 15:23:01', 1),
(12, 1, 1, 1, 5, '8500.00', '2016-03-11 16:28:38', 1, '2016-03-11 15:32:08', 1),
(13, 1, 1, 2, 4, '0.00', '2016-03-11 16:34:20', 1, '2016-03-11 15:34:20', 1),
(14, 1, 2, 1, 1, '5000.00', '2016-03-11 18:40:03', 1, '2016-03-11 17:40:03', 1),
(15, 1, 2, 1, 2, '2300.00', '2016-03-11 18:40:03', 1, '2016-03-11 17:40:03', 1),
(16, 1, 2, 1, 3, '1200.00', '2016-03-11 18:40:03', 1, '2016-03-11 17:40:03', 1),
(17, 1, 2, 1, 5, '52000.00', '2016-03-11 18:40:03', 1, '2016-03-11 17:40:03', 1),
(18, 1, 2, 1, 6, '3500.00', '2016-03-11 18:40:03', 1, '2016-03-11 17:40:03', 1),
(19, 2, 2, 1, 1, '5000.00', '2016-03-11 18:40:32', 1, '2016-03-11 17:40:32', 1),
(20, 2, 2, 1, 3, '2000.00', '2016-03-11 18:40:32', 1, '2016-03-11 17:40:32', 1),
(21, 2, 2, 1, 5, '55000.00', '2016-03-11 18:40:32', 1, '2016-03-11 17:40:32', 1),
(22, 2, 2, 1, 6, '2500.00', '2016-03-11 18:40:32', 1, '2016-03-11 17:40:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bills_students`
--

DROP TABLE IF EXISTS `bills_students`;
CREATE TABLE `bills_students` (
  `id` int(6) NOT NULL,
  `student_id` int(11) NOT NULL,
  `session_term_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `added_on` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `classteacher`
--

DROP TABLE IF EXISTS `classteacher`;
CREATE TABLE `classteacher` (
  `classteacher_id` int(100) NOT NULL,
  `teachertitle` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherlastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherfirstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherothernames` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherusername` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherpassword` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacher_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacher_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `teacher_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `teacher_city` varchar(150) COLLATE utf8_unicode_ci DEFAULT '',
  `teacher_state_id` int(11) NOT NULL,
  `teacherclass_level` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherclass_no` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `teacherclass_letter` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classteacher`
--

INSERT INTO `classteacher` (`classteacher_id`, `teachertitle`, `teacherlastname`, `teacherfirstname`, `teacherothernames`, `teacherusername`, `teacherpassword`, `teacher_email`, `teacher_phone`, `teacher_address`, `teacher_city`, `teacher_state_id`, `teacherclass_level`, `teacherclass_no`, `teacherclass_letter`, `status`, `type`, `last_updated`) VALUES
(1, '', 'Admin', 'Admin', '', 'admin', 'l@pr3psc#', 'admin@lps.com', '4567890', 'Ikeja', 'Lagos', 25, '', '', '', 1, 3, '2016-03-11 17:32:36'),
(2, 'Mr', 'AbdulRahman', 'Ridwan', '', 'ridwan', 'ridwan', 'ridwan.abdulrahman@upperlink.ng', '567890', 'Berger', 'Lagos', 25, 'Primary', '1', 'A', 1, 0, '2016-03-15 15:27:55'),
(3, '', 'Ridwan', 'Abdulrahman', '', 'rahman', 'rahman', 'ridwan.abdulrahman@upperlink.ng', '987654', 'Ojodu', 'Lagos', 25, '', '', '', 0, 3, '2016-03-11 17:34:14');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `departments_id` int(100) NOT NULL,
  `departments_name` varchar(255) NOT NULL DEFAULT '',
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `added_on` datetime NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`departments_id`, `departments_name`, `is_enabled`, `added_on`, `modified_on`) VALUES
(1, '1', 1, '2016-03-11 11:02:37', '2016-03-11 10:02:37'),
(2, '2', 1, '2016-03-11 11:02:42', '2016-03-11 10:02:42'),
(3, '3', 1, '2016-03-11 11:02:46', '2016-03-11 10:03:13');

-- --------------------------------------------------------

--
-- Table structure for table `exchange_rates`
--

DROP TABLE IF EXISTS `exchange_rates`;
CREATE TABLE `exchange_rates` (
  `er_id` int(11) NOT NULL,
  `session_term_id` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `er_is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `er_added_on` datetime NOT NULL,
  `er_added_by` int(11) NOT NULL,
  `er_modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `er_modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exchange_rates`
--

INSERT INTO `exchange_rates` (`er_id`, `session_term_id`, `rate`, `er_is_enabled`, `er_added_on`, `er_added_by`, `er_modified_on`, `er_modified_by`) VALUES
(1, 2, '250.00', 1, '2016-03-15 12:07:01', 1, '2016-03-15 11:07:01', 1),
(2, 1, '199.20', 1, '2016-03-15 16:27:36', 1, '2016-03-15 15:27:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

DROP TABLE IF EXISTS `faculties`;
CREATE TABLE `faculties` (
  `faculties_id` int(11) NOT NULL,
  `faculties_name` varchar(150) NOT NULL DEFAULT '',
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `added_on` datetime NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`faculties_id`, `faculties_name`, `is_enabled`, `added_on`, `modified_on`) VALUES
(1, 'Nursery', 1, '2016-03-11 11:03:43', '2016-03-11 10:04:03'),
(2, 'Primary', 1, '2016-03-11 11:03:47', '2016-03-11 10:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

DROP TABLE IF EXISTS `grades`;
CREATE TABLE `grades` (
  `grade_id` int(10) UNSIGNED NOT NULL,
  `grade_name` varchar(5) NOT NULL,
  `grade_remark` varchar(50) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`grade_id`, `grade_name`, `grade_remark`, `modified_on`) VALUES
(1, 'A', 'Excellent', '2015-03-03 14:30:14'),
(2, 'B', 'V. Good', '2015-03-03 14:30:14'),
(3, 'C', 'Good', '2015-03-03 14:30:14'),
(4, 'D', 'Fair', '2015-03-03 14:30:14'),
(5, 'E', 'Just Fair', '2015-03-03 14:30:14'),
(6, 'F', 'Poor', '2015-03-03 14:30:14');

-- --------------------------------------------------------

--
-- Table structure for table `grade_system`
--

DROP TABLE IF EXISTS `grade_system`;
CREATE TABLE `grade_system` (
  `grade_system_id` int(11) NOT NULL,
  `max_score` decimal(10,0) NOT NULL,
  `lower_limit` decimal(10,2) NOT NULL,
  `upper_limit` decimal(10,2) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_system`
--

INSERT INTO `grade_system` (`grade_system_id`, `max_score`, `lower_limit`, `upper_limit`, `grade_id`, `modified_on`) VALUES
(1, '10', '9.00', '10.00', 1, '2015-03-03 14:40:15'),
(2, '10', '8.00', '8.90', 2, '2015-03-03 14:40:15'),
(3, '10', '7.00', '7.90', 3, '2015-03-03 14:40:15'),
(4, '10', '6.00', '6.90', 4, '2015-03-03 14:40:15'),
(5, '10', '5.00', '5.90', 5, '2015-03-03 14:40:15'),
(6, '10', '0.00', '4.90', 6, '2015-03-03 14:40:15'),
(7, '15', '13.00', '15.00', 1, '2015-03-03 14:40:15'),
(8, '15', '11.00', '12.90', 2, '2015-03-03 14:40:15'),
(9, '15', '9.00', '10.90', 3, '2015-03-03 14:40:15'),
(10, '15', '7.00', '8.90', 4, '2015-03-03 14:40:15'),
(11, '15', '5.00', '6.90', 5, '2015-03-03 14:40:15'),
(12, '15', '0.00', '4.90', 6, '2015-03-03 14:40:15'),
(13, '100', '90.00', '100.00', 1, '2015-03-03 14:40:15'),
(14, '100', '80.00', '89.00', 2, '2015-03-03 14:40:15'),
(15, '100', '70.00', '79.00', 3, '2015-03-03 14:40:15'),
(16, '100', '60.00', '69.00', 4, '2015-03-03 14:40:15'),
(17, '100', '50.00', '59.00', 5, '2015-03-03 14:40:15'),
(18, '100', '0.00', '49.00', 6, '2015-03-03 14:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `log_id` int(11) NOT NULL,
  `log_surname` varchar(150) NOT NULL,
  `std_id` int(11) NOT NULL COMMENT 'links wit student_profile table',
  `log_othernames` varchar(255) NOT NULL,
  `log_username` varchar(80) NOT NULL,
  `log_email` varchar(80) NOT NULL,
  `log_password` varchar(255) NOT NULL,
  `log_stdtypeid` tinyint(4) NOT NULL,
  `log_programmeid` tinyint(4) NOT NULL,
  `log_status` tinyint(4) NOT NULL,
  `log_status2` tinyint(4) NOT NULL,
  `log_status3` tinyint(4) NOT NULL COMMENT 'Faculty ID',
  `log_status4` tinyint(4) NOT NULL COMMENT 'programme type id',
  `log_status5` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `log_faculty` int(10) NOT NULL,
  `log_department` int(10) NOT NULL,
  `log_study` int(10) NOT NULL,
  `log_degree` int(10) NOT NULL,
  `termsofservice` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `pinid` varchar(10) NOT NULL,
  `center` varchar(15) NOT NULL COMMENT 'Applicant or null',
  `matricnum` varchar(50) NOT NULL COMMENT 'Assigned to newly admitted studs',
  `email_verification_code` varchar(16) DEFAULT NULL,
  `email_verified` tinyint(1) UNSIGNED NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`log_id`, `log_surname`, `std_id`, `log_othernames`, `log_username`, `log_email`, `log_password`, `log_stdtypeid`, `log_programmeid`, `log_status`, `log_status2`, `log_status3`, `log_status4`, `log_status5`, `state`, `log_faculty`, `log_department`, `log_study`, `log_degree`, `termsofservice`, `password`, `pinid`, `center`, `matricnum`, `email_verification_code`, `email_verified`, `modified_on`) VALUES
(1, 'Adeyi', 0, 'ridwan', '', 'readone23@gmail.com', 'adeyi1981', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, '', '', '', '', '', NULL, 0, '2015-10-15 09:19:52'),
(2, 'Adeyi', 0, 'ridwan', '', 'ridwan.abdulrahman@upperlink.ng', 'Adeyi1981', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, '', '', '', '', '', NULL, 0, '2015-10-15 09:23:55'),
(3, 'abdulrahman', 0, 'ridwan', '', 'readone23@outlook.com', 'adeyi1981', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, '', '', '', '', '', NULL, 0, '2015-10-15 09:41:08'),
(4, 'Efereyan', 0, 'Jephthah', '', 'jefereyan@gmail.com', 'dubbledutch', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, '', '', '', '', '', NULL, 0, '2015-10-26 15:44:09'),
(5, 'Efereyan', 0, 'Gail', '', 'jefe@gmail.com', 'gailmi', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, '', '', '', '', '', NULL, 0, '2015-10-26 17:21:15'),
(6, 'Efereyan', 0, 'Jephthah', '', 'jefereyan@yahoo.com', 'gailmi', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, '', '', '', '', '', NULL, 0, '2016-02-29 09:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

DROP TABLE IF EXISTS `nationality`;
CREATE TABLE `nationality` (
  `nationality_id` int(10) UNSIGNED NOT NULL,
  `nationality_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`nationality_id`, `nationality_name`) VALUES
(1, 'Afghanistan'),
(2, 'Aringland Islands'),
(3, 'Albania'),
(4, 'Algeria'),
(5, 'American Samoa'),
(6, 'Andorra'),
(7, 'Angola'),
(8, 'Anguilla'),
(9, 'Antarctica'),
(10, 'Antigua and Barbuda'),
(11, 'Argentina'),
(12, 'Armenia'),
(13, 'Aruba'),
(14, 'Australia'),
(15, 'Austria'),
(16, 'Azerbaijan'),
(17, 'Bahamas'),
(18, 'Bahrain'),
(19, 'Bangladesh'),
(20, 'Barbados'),
(21, 'Belarus'),
(22, 'Belgium'),
(23, 'Belize'),
(24, 'Benin'),
(25, 'Bermuda'),
(26, 'Bhutan'),
(27, 'Bolivia'),
(28, 'Bosnia and Herzegovina'),
(29, 'Botswana'),
(30, 'Bouvet Island'),
(31, 'Brazil'),
(32, 'British Indian Ocean territory'),
(33, 'Brunei Darussalam'),
(34, 'Bulgaria'),
(35, 'Burkina Faso'),
(36, 'Burundi'),
(37, 'Cambodia'),
(38, 'Cameroon'),
(39, 'Canada'),
(40, 'Cape Verde'),
(41, 'Cayman Islands'),
(42, 'Central African Republic'),
(43, 'Chad'),
(44, 'Chile'),
(45, 'China'),
(46, 'Christmas Island'),
(47, 'Cocos (Keeling) Islands'),
(48, 'Colombia'),
(49, 'Comoros'),
(50, 'Congo'),
(51, 'Congo'),
(52, ' Democratic Republic'),
(53, 'Cook Islands'),
(54, 'Costa Rica'),
(55, 'Ivory Coast (Ivory Coast)'),
(56, 'Croatia (Hrvatska)'),
(57, 'Cuba'),
(58, 'Cyprus'),
(59, 'Czech Republic'),
(60, 'Denmark'),
(61, 'Djibouti'),
(62, 'Dominica'),
(63, 'Dominican Republic'),
(64, 'East Timor'),
(65, 'Ecuador'),
(66, 'Egypt'),
(67, 'El Salvador'),
(68, 'Equatorial Guinea'),
(69, 'Eritrea'),
(70, 'Estonia'),
(71, 'Ethiopia'),
(72, 'Falkland Islands'),
(73, 'Faroe Islands'),
(74, 'Fiji'),
(75, 'Finland'),
(76, 'France'),
(77, 'French Guiana'),
(78, 'French Polynesia'),
(79, 'French Southern Territories'),
(80, 'Gabon'),
(81, 'Gambia'),
(82, 'Georgia'),
(83, 'Germany'),
(84, 'Ghana'),
(85, 'Gibraltar'),
(86, 'Greece'),
(87, 'Greenland'),
(88, 'Grenada'),
(89, 'Guadeloupe'),
(90, 'Guam'),
(91, 'Guatemala'),
(92, 'Guinea'),
(93, 'Guinea-Bissau'),
(94, 'Guyana'),
(95, 'Haiti'),
(96, 'Heard and McDonald Islands'),
(97, 'Honduras'),
(98, 'Hong Kong'),
(99, 'Hungary'),
(100, 'Iceland'),
(101, 'India'),
(102, 'Indonesia'),
(103, 'Iran'),
(104, 'Iraq'),
(105, 'Ireland'),
(106, 'Israel'),
(107, 'Italy'),
(108, 'Jamaica'),
(109, 'Japan'),
(110, 'Jordan'),
(111, 'Kazakhstan'),
(112, 'Kenya'),
(113, 'Kiribati'),
(114, 'Korea (north)'),
(115, 'Korea (south)'),
(116, 'Kuwait'),
(117, 'Kyrgyzstan'),
(118, 'Lao People''s Democratic Republic'),
(119, 'Latvia'),
(120, 'Lebanon'),
(121, 'Lesotho'),
(122, 'Liberia'),
(123, 'Libyan Arab Jamahiriya'),
(124, 'Liechtenstein'),
(125, 'Lithuania'),
(126, 'Luxembourg'),
(127, 'Macao'),
(128, 'Macedonia'),
(129, 'Madagascar'),
(130, 'Malawi'),
(131, 'Malaysia'),
(132, 'Maldives'),
(133, 'Mali'),
(134, 'Malta'),
(135, 'Marshall Islands'),
(136, 'Martinique'),
(137, 'Mauritania'),
(138, 'Mauritius'),
(139, 'Mayotte'),
(140, 'Mexico'),
(141, 'Micronesia'),
(142, 'Moldova'),
(143, 'Monaco'),
(144, 'Mongolia'),
(145, 'Montserrat'),
(146, 'Morocco'),
(147, 'Mozambique'),
(148, 'Myanmar'),
(149, 'Namibia'),
(150, 'Nauru'),
(151, 'Nepal'),
(152, 'Netherlands'),
(153, 'Netherlands Antilles'),
(154, 'New Caledonia'),
(155, 'New Zealand'),
(156, 'Nicaragua'),
(157, 'Niger'),
(158, 'Nigeria'),
(159, 'Niue'),
(160, 'Norfolk Island'),
(161, 'Northern Mariana Islands'),
(162, 'Norway'),
(163, 'Oman'),
(164, 'Pakistan'),
(165, 'Palau'),
(166, 'Palestinian Territories'),
(167, 'Panama'),
(168, 'Papua New Guinea'),
(169, 'Paraguay'),
(170, 'Peru'),
(171, 'Philippines'),
(172, 'Pitcairn'),
(173, 'Poland'),
(174, 'Portugal'),
(175, 'Puerto Rico'),
(176, 'Qatar'),
(177, 'Runion'),
(178, 'Romania'),
(179, 'Russian Federation'),
(180, 'Rwanda'),
(181, 'Saint Helena'),
(182, 'Saint Kitts and Nevis'),
(183, 'Saint Lucia'),
(184, 'Saint Pierre and Miquelon'),
(185, 'Saint Vincent and the Grenadines'),
(186, 'Samoa'),
(187, 'San Marino'),
(188, 'Sao Tome and Principe'),
(189, 'Saudi Arabia'),
(190, 'Senegal'),
(191, 'Serbia and Montenegro'),
(192, 'Seychelles'),
(193, 'Sierra Leone'),
(194, 'Singapore'),
(195, 'Slovakia'),
(196, 'Slovenia'),
(197, 'Solomon Islands'),
(198, 'Somalia'),
(199, 'South Africa'),
(200, 'South Georgia and the South Sandwich Islands'),
(201, 'Spain'),
(202, 'Sri Lanka'),
(203, 'Sudan'),
(204, 'Suriname'),
(205, 'Svalbard and Jan Mayen Islands'),
(206, 'Swaziland'),
(207, 'Sweden'),
(208, 'Switzerland'),
(209, 'Syria'),
(210, 'Taiwan'),
(211, 'Tajikistan'),
(212, 'Tanzania'),
(213, 'Thailand'),
(214, 'Togo'),
(215, 'Tokelau'),
(216, 'Tonga'),
(217, 'Trinidad and Tobago'),
(218, 'Tunisia'),
(219, 'Turkey'),
(220, 'Turkmenistan'),
(221, 'Turks and Caicos Islands'),
(222, 'Tuvalu'),
(223, 'Uganda'),
(224, 'Ukraine'),
(225, 'United Arab Emirates'),
(226, 'United Kingdom'),
(227, 'United States of America'),
(228, 'Uruguay'),
(229, 'Uzbekistan'),
(230, 'Vanuatu'),
(231, 'Vatican City'),
(232, 'Venezuela'),
(233, 'Vietnam'),
(234, 'Virgin Islands (British)'),
(235, 'Virgin Islands (US)'),
(236, 'Wallis and Futuna Islands'),
(237, 'Western Sahara'),
(238, 'Yemen'),
(239, 'Zaire'),
(240, 'Zambia'),
(241, 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `payment_invoices`
--

DROP TABLE IF EXISTS `payment_invoices`;
CREATE TABLE `payment_invoices` (
  `invoice_id` int(11) NOT NULL,
  `invoice_no` varchar(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `session_term_id` int(11) NOT NULL,
  `invoice_added_on` datetime NOT NULL,
  `invoice_added_by` int(11) NOT NULL,
  `invoice_modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `invoice_modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programme`
--

DROP TABLE IF EXISTS `programme`;
CREATE TABLE `programme` (
  `programme_id` int(100) NOT NULL,
  `programme_name` varchar(255) NOT NULL DEFAULT '',
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `added_on` datetime NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

--
-- Dumping data for table `programme`
--

INSERT INTO `programme` (`programme_id`, `programme_name`, `is_enabled`, `added_on`, `modified_on`) VALUES
(1, 'Samuel', 1, '2016-03-11 11:08:05', '2016-03-11 10:08:17'),
(2, 'Bamboo', 1, '2016-03-11 11:08:10', '2016-03-11 10:08:23'),
(3, 'A', 1, '2016-03-11 11:08:26', '2016-03-11 10:08:26');

-- --------------------------------------------------------

--
-- Table structure for table `school_sessions`
--

DROP TABLE IF EXISTS `school_sessions`;
CREATE TABLE `school_sessions` (
  `session_id` int(11) NOT NULL,
  `session_name` varchar(20) NOT NULL,
  `session_fullname` varchar(50) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `added_on` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Created on March 7, 2016 by Jefe';

--
-- Dumping data for table `school_sessions`
--

INSERT INTO `school_sessions` (`session_id`, `session_name`, `session_fullname`, `start_date`, `end_date`, `added_on`, `added_by`, `modified_on`, `modified_by`) VALUES
(1, '2015', '2015/2016', NULL, NULL, '2016-03-11 13:47:16', 1, '2016-03-11 12:47:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `school_terms`
--

DROP TABLE IF EXISTS `school_terms`;
CREATE TABLE `school_terms` (
  `term_id` int(11) NOT NULL,
  `term_name` varchar(20) NOT NULL,
  `term_fullname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Created on March 7, 2016 by Jefe' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `school_terms`
--

INSERT INTO `school_terms` (`term_id`, `term_name`, `term_fullname`) VALUES
(1, 'firstterm', 'First Term'),
(2, 'secondterm', 'Second Term'),
(3, 'thirdterm', 'Third Term');

-- --------------------------------------------------------

--
-- Table structure for table `session_terms`
--

DROP TABLE IF EXISTS `session_terms`;
CREATE TABLE `session_terms` (
  `session_term_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `term_id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` enum('Past','Current','Next') DEFAULT NULL,
  `added_on` datetime NOT NULL,
  `added_by` int(11) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Created on March 7, 2016 by Jefe' ROW_FORMAT=COMPACT;

--
-- Dumping data for table `session_terms`
--

INSERT INTO `session_terms` (`session_term_id`, `session_id`, `term_id`, `start_date`, `end_date`, `status`, `added_on`, `added_by`, `modified_on`, `modified_by`) VALUES
(1, 1, 1, NULL, NULL, NULL, '2016-03-11 13:47:16', 1, '2016-03-11 12:57:02', 1),
(2, 1, 2, NULL, NULL, 'Current', '2016-03-11 13:47:16', 1, '2016-03-11 12:57:02', 1),
(3, 1, 3, NULL, NULL, NULL, '2016-03-11 13:47:16', 1, '2016-03-11 12:47:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
CREATE TABLE `state` (
  `state_id` int(11) NOT NULL DEFAULT '0',
  `state_name` varchar(20) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`) VALUES
(1, 'Abia'),
(2, 'Adamawa'),
(3, 'Akwa Ibom'),
(4, 'Anambra'),
(5, 'Bauchi'),
(6, 'Bayelsa'),
(7, 'Benue'),
(8, 'Borno'),
(9, 'Cross River'),
(10, 'Delta'),
(11, 'Ebonyi'),
(12, 'Edo'),
(13, 'Ekiti'),
(14, 'Enugu'),
(15, 'Gombe'),
(16, 'Imo'),
(17, 'Jigawa'),
(18, 'Kaduna'),
(19, 'Kano'),
(20, 'Katsina'),
(21, 'Kebbi'),
(22, 'Kogi'),
(23, 'Kwara'),
(24, 'Lagos'),
(25, 'Nasarawa'),
(26, 'Niger'),
(27, 'Ogun'),
(28, 'Ondo'),
(29, 'Osun'),
(30, 'Oyo'),
(31, 'Plateau'),
(32, 'Rivers'),
(33, 'Sokoto'),
(34, 'Taraba'),
(35, 'Yobe'),
(36, 'Zamfara'),
(37, 'FCT'),
(38, 'Foreign');

-- --------------------------------------------------------

--
-- Table structure for table `studentprofile`
--

DROP TABLE IF EXISTS `studentprofile`;
CREATE TABLE `studentprofile` (
  `id` int(100) NOT NULL,
  `studentprofile_id` varchar(50) NOT NULL DEFAULT '',
  `spin` varchar(20) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `othernames` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `dateofbirth` date DEFAULT NULL,
  `placeofbirth` varchar(100) NOT NULL DEFAULT '',
  `nativity` varchar(100) NOT NULL DEFAULT '',
  `localgov` varchar(100) NOT NULL DEFAULT '',
  `stateoforigin` varchar(100) NOT NULL DEFAULT '',
  `nationality` varchar(100) NOT NULL DEFAULT '',
  `address` mediumtext NOT NULL,
  `otherprofile` mediumtext NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `disability` varchar(100) NOT NULL,
  `teachersname` varchar(200) NOT NULL DEFAULT '',
  `height` varchar(5) NOT NULL DEFAULT '',
  `weight` varchar(5) NOT NULL DEFAULT '',
  `enabled` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studentprofile`
--

INSERT INTO `studentprofile` (`id`, `studentprofile_id`, `spin`, `lastname`, `firstname`, `othernames`, `password`, `gender`, `dateofbirth`, `placeofbirth`, `nativity`, `localgov`, `stateoforigin`, `nationality`, `address`, `otherprofile`, `email`, `disability`, `teachersname`, `height`, `weight`, `enabled`) VALUES
(1, '9780771041587', '', 'Efereyan', 'Jephthah', 'O.', 'efereyan', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(2, '9780883682791', '', 'Abdulrahman', 'Ridwan', '', 'abdulrahman', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(3, '9780892765256', '', 'King', 'Edward', '', 'king', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '1'),
(4, '9781603740500', '', 'Ogunlade', 'Omotayo', 'Opeyem', 'ogunlade', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `studentprofile_extra`
--

DROP TABLE IF EXISTS `studentprofile_extra`;
CREATE TABLE `studentprofile_extra` (
  `id` int(100) NOT NULL,
  `session_term_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `programme_id` int(11) NOT NULL,
  `teachersname` varchar(100) NOT NULL DEFAULT '',
  `age` varchar(50) NOT NULL COMMENT 'Student''s age at the time of the move',
  `class_avg_age` varchar(50) NOT NULL,
  `added_on` datetime NOT NULL COMMENT 'when the move occurred',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `id` int(100) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `enabled` tinyint(4) NOT NULL,
  `added_on` datetime DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `enabled`, `added_on`, `modified_on`) VALUES
(1, 'English', 1, '2016-05-18 09:44:17', '2016-05-18 08:49:57');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(20) NOT NULL,
  `trans_no` varchar(100) NOT NULL DEFAULT '',
  `trans_amount` decimal(15,2) NOT NULL,
  `trans_charge` decimal(15,2) NOT NULL,
  `trans_status` varchar(255) NOT NULL DEFAULT '',
  `trans_date` datetime,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `payment_option` varchar(100) NOT NULL,
  `student_id` int(11) NOT NULL,
  `session_term_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `trans_no`, `trans_amount`, `trans_charge`, `trans_status`, `trans_date`, `modified_on`, `payment_option`, `student_id`, `session_term_id`) VALUES
(1, '3061418733098020', '29000.00', '2000.00', 'Pending', '2016-03-15 16:35:07', '2016-05-18 14:33:21', 'MasterCard', 1, 2),
(2, '3061893696224929', '29300.00', '2300.00', 'Pending', '2016-03-15 16:38:08', '2016-05-18 14:33:21', 'BranchCollect', 1, 2),
(3, '3061612155294241', '29300.00', '2300.00', 'Pending', '2016-03-15 16:40:16', '2016-05-18 14:33:21', 'BranchCollect', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `vpc_transactions`
--

DROP TABLE IF EXISTS `vpc_transactions`;
CREATE TABLE `vpc_transactions` (
  `id` int(20) NOT NULL,
  `trans_id` int(20) NOT NULL DEFAULT '0' COMMENT 'id on transactiontotal table',
  `vpc_request` text NOT NULL,
  `vpc_response` text NOT NULL,
  `vpc_command` varchar(50) NOT NULL DEFAULT '',
  `vpc_merchtxnref` varchar(50) NOT NULL DEFAULT '',
  `vpc_merchant` varchar(50) NOT NULL DEFAULT '',
  `vpc_orderinfo` varchar(50) NOT NULL DEFAULT '',
  `vpc_amount` decimal(15,2) NOT NULL,
  `vpc_currency` varchar(5) NOT NULL DEFAULT '',
  `vpc_message` varchar(255) NOT NULL,
  `vpc_txnresponsecode` varchar(5) NOT NULL DEFAULT '',
  `vpc_receiptno` varchar(50) NOT NULL DEFAULT '',
  `vpc_acqresponsecode` varchar(20) NOT NULL DEFAULT '',
  `vpc_transactionno` varchar(50) NOT NULL DEFAULT '',
  `vpc_batchno` varchar(50) NOT NULL DEFAULT '',
  `vpc_authorizeid` varchar(20) NOT NULL DEFAULT '',
  `vpc_card` varchar(20) NOT NULL DEFAULT '',
  `vpc_securehash` varchar(150) NOT NULL DEFAULT '',
  `vpc_securehashtype` varchar(20) NOT NULL DEFAULT '',
  `vpc_cardnum` varchar(20) DEFAULT '',
  `vpc_returnaci` varchar(10) DEFAULT '',
  `vpc_transactionidentifier` varchar(50) DEFAULT '',
  `vpc_commercialcardindicator` varchar(10) DEFAULT '',
  `vpc_commercialcard` varchar(10) DEFAULT '',
  `vpc_cardlevelindicator` varchar(10) DEFAULT '',
  `vpc_financialnetworkcode` varchar(20) DEFAULT '',
  `vpc_marketspecificdata` varchar(10) DEFAULT '',
  `added_on` datetime NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `vpc_transactions`
--

INSERT INTO `vpc_transactions` (`id`, `trans_id`, `vpc_request`, `vpc_response`, `vpc_command`, `vpc_merchtxnref`, `vpc_merchant`, `vpc_orderinfo`, `vpc_amount`, `vpc_currency`, `vpc_message`, `vpc_txnresponsecode`, `vpc_receiptno`, `vpc_acqresponsecode`, `vpc_transactionno`, `vpc_batchno`, `vpc_authorizeid`, `vpc_card`, `vpc_securehash`, `vpc_securehashtype`, `vpc_cardnum`, `vpc_returnaci`, `vpc_transactionidentifier`, `vpc_commercialcardindicator`, `vpc_commercialcard`, `vpc_cardlevelindicator`, `vpc_financialnetworkcode`, `vpc_marketspecificdata`, `added_on`, `modified_on`) VALUES
(1, 1, 'vpc_AccessCode=D77455B5&vpc_Amount=11600&vpc_Command=pay&vpc_Currency=USD&vpc_Locale=English (Nigeria)&vpc_MerchTxnRef=3061418733098020114601&vpc_Merchant=3214LAFCMB1M&vpc_OrderInfo=30614187330980202640&vpc_ReturnURL=http://lagosprepschool.com/vpc/pay-response.php&vpc_Version=1&vpc_SecureHash=5f8d0473a77c82d6b632e459940d2cd8c7d18abbe57b6044bc9c5e2b2939db0a&vpc_SecureHashType=SHA256', '', '', '', '', '30614187330980202640', '0.00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2016-03-15 16:35:49', '2016-03-15 15:35:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `user_pswd` (`user_pswd`);

--
-- Indexes for table `admin_type`
--
ALTER TABLE `admin_type`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `application_profile`
--
ALTER TABLE `application_profile`
  ADD PRIMARY KEY (`std_id`);

--
-- Indexes for table `billing_items`
--
ALTER TABLE `billing_items`
  ADD PRIMARY KEY (`bi_id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`bill_id`),
  ADD UNIQUE KEY `bill` (`session_term_id`,`faculty_id`,`department_id`,`item_id`),
  ADD KEY `session_term_id` (`session_term_id`),
  ADD KEY `faculty_id` (`faculty_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `bills_students`
--
ALTER TABLE `bills_students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bill` (`session_term_id`,`item_id`),
  ADD KEY `session_term_id` (`session_term_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `classteacher`
--
ALTER TABLE `classteacher`
  ADD PRIMARY KEY (`classteacher_id`),
  ADD UNIQUE KEY `teacherusername` (`teacherusername`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`departments_id`),
  ADD UNIQUE KEY `departments_name` (`departments_name`);

--
-- Indexes for table `exchange_rates`
--
ALTER TABLE `exchange_rates`
  ADD PRIMARY KEY (`er_id`),
  ADD UNIQUE KEY `session_term_id` (`session_term_id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`faculties_id`),
  ADD UNIQUE KEY `faculties_name` (`faculties_name`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`grade_id`),
  ADD UNIQUE KEY `grade_name` (`grade_name`);

--
-- Indexes for table `grade_system`
--
ALTER TABLE `grade_system`
  ADD PRIMARY KEY (`grade_system_id`),
  ADD UNIQUE KEY `max_score_lower_limit_upper_limit` (`max_score`,`lower_limit`,`upper_limit`),
  ADD KEY `grade_id` (`grade_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`nationality_id`);

--
-- Indexes for table `payment_invoices`
--
ALTER TABLE `payment_invoices`
  ADD PRIMARY KEY (`invoice_id`),
  ADD UNIQUE KEY `invoice_no` (`invoice_no`),
  ADD UNIQUE KEY `student_id_session_term_id` (`student_id`,`session_term_id`);

--
-- Indexes for table `programme`
--
ALTER TABLE `programme`
  ADD PRIMARY KEY (`programme_id`),
  ADD UNIQUE KEY `programme_name` (`programme_name`);

--
-- Indexes for table `school_sessions`
--
ALTER TABLE `school_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_name_` (`session_name`),
  ADD KEY `session_name` (`session_name`);

--
-- Indexes for table `school_terms`
--
ALTER TABLE `school_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `session_name_` (`term_name`),
  ADD KEY `session_name` (`term_name`);

--
-- Indexes for table `session_terms`
--
ALTER TABLE `session_terms`
  ADD PRIMARY KEY (`session_term_id`),
  ADD UNIQUE KEY `session_id_term_id` (`session_id`,`term_id`),
  ADD KEY `FK_session_terms_school_terms` (`term_id`),
  ADD KEY `session_id` (`session_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `studentprofile`
--
ALTER TABLE `studentprofile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `studentprofile_id` (`studentprofile_id`);

--
-- Indexes for table `studentprofile_extra`
--
ALTER TABLE `studentprofile_extra`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `session_history` (`session_term_id`,`student_id`) USING BTREE;

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subjects` (`name`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trans_no` (`trans_no`);

--
-- Indexes for table `vpc_transactions`
--
ALTER TABLE `vpc_transactions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `user_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_type`
--
ALTER TABLE `admin_type`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `application_profile`
--
ALTER TABLE `application_profile`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_items`
--
ALTER TABLE `billing_items`
  MODIFY `bi_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `bill_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `bills_students`
--
ALTER TABLE `bills_students`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `classteacher`
--
ALTER TABLE `classteacher`
  MODIFY `classteacher_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `departments_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `exchange_rates`
--
ALTER TABLE `exchange_rates`
  MODIFY `er_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `faculties_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `grade_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `grade_system`
--
ALTER TABLE `grade_system`
  MODIFY `grade_system_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `nationality_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
--
-- AUTO_INCREMENT for table `payment_invoices`
--
ALTER TABLE `payment_invoices`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `programme`
--
ALTER TABLE `programme`
  MODIFY `programme_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `school_sessions`
--
ALTER TABLE `school_sessions`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `school_terms`
--
ALTER TABLE `school_terms`
  MODIFY `term_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `session_terms`
--
ALTER TABLE `session_terms`
  MODIFY `session_term_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `studentprofile`
--
ALTER TABLE `studentprofile`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `studentprofile_extra`
--
ALTER TABLE `studentprofile_extra`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vpc_transactions`
--
ALTER TABLE `vpc_transactions`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
