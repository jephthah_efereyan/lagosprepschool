<?php

class Arms
{

    function __construct()
    {

    }


    public static function getAll($connect)
    {
        $data = array();
        $query = "
			SELECT 
				*
			FROM 
				programme
			";


        if ($result = mysql_query($query, $connect)) {

            while ($row = mysql_fetch_array($result)) {
                $data[] = $row;
            }

        } else {
            //print mysql_error();
        }
        return $data;
    }


    /**
     * Get the details of an arm by name
     * @param string $arm_name
     * @return object $arm
     */
    public static function getArmByName($connect, $arm_name)
    {
        $arm = null;

        $query = "
			SELECT
				*
			FROM
				programme
            WHERE
                programme_name = '{$arm_name}'
			";

        $result = mysql_query($query, $connect);

        if ($result && mysql_num_rows($result) > 0) {
            $arm = mysql_fetch_object($result);
        }

        return $arm;
    }
}