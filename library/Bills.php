<?php

class Bills
{

    public function __construct($connect)
    {

    }


    public static function create($connect, $studentID, $session_term_id, $teacherID, $items, $amounts)
    {
        if (!$items and !is_array($items)) return false;
        $query = "INSERT INTO  bills_students
				(`student_id`, `session_term_id`, `item_id`, `amount`, `added_on`, `added_by`,`modified_by`)
				VALUES ";

        //generate values
        foreach ($items as $i => $item_id) {

            $amount = $amounts[$i];

            if ($i > 0) $query .= ", ";

            $query .= "({$studentID}, {$session_term_id}, {$item_id}, '{$amount}', NOW(), {$teacherID},{$teacherID})";
        }

        $query .= " ON DUPLICATE KEY UPDATE amount = VALUES(amount)";

        if (mysql_query($query, $connect)) {
            return mysql_affected_rows();
        }
        //print mysql_error();
        return false;


    }

    public static function studentTotalPayment($connect, $studentID)
    {

        $query = "
		SELECT
			(SUM(trans_amount) - SUM(trans_charge) )  as total
		FROM transactions
		WHERE
			trans_status = 'Paid' AND
			student_id	= {$studentID}";
        if ($result = mysql_query($query, $connect)) {
            while ($row = mysql_fetch_assoc($result)) {
                return $row['total'];
            }

        } else {
            //print mysql_error();
        }
        //return 0.0;
    }


    public static function studentTotalBills($connect, $studentID)
    {

        $query = "
		SELECT
			SUM(bills_students.amount) as total
		FROM bills_students
		LEFT JOIN billing_items ON billing_items.bi_id = bills_students.item_id
		WHERE
			bills_students.student_id		= {$studentID}
		";

        if ($result = mysql_query($query, $connect)) {

            while ($row = mysql_fetch_assoc($result)) {
                return $row['total'];
            }

        } else {
            //print mysql_error();
        }


        //return 0.0;
    }

    public static function studentBills($connect, $studentID, $sessionTermID)
    {
        $sessionTermID = mysql_real_escape_string($sessionTermID);

        $data = array();
        $query = "
			SELECT 
				bills_students.item_id AS bi_id,
				bills_students.amount,
				bills_students.added_on,
				bills_students.modified_on,
				billing_items.bi_name
			FROM bills_students
			LEFT JOIN billing_items ON billing_items.bi_id = bills_students.item_id
			WHERE 
				bills_students.student_id		= {$studentID} AND 
				bills_students.session_term_id	= {$sessionTermID} 
			ORDER BY bills_students.item_id ASC
			";


        if ($result = mysql_query($query, $connect)) {

            while ($row = mysql_fetch_assoc($result)) {
                $data[] = $row;
            }

        } else {
            //print mysql_error();
        }
        return $data;


    }

    public static function listItems($connect)
    {
        $data = array();
        $query = "
		SELECT
			bi_id,
			bi_name,
			bi_is_enabled,
			bi_added_on,
			bi_modified_on
		FROM
			billing_items
		WHERE bi_is_enabled = 1
		";
        
        if ($result = mysql_query($query, $connect)) {

            while ($row = mysql_fetch_assoc($result)) {
                $data[] = $row;
            }

        } else {
            //print mysql_error();
        }
        return $data;

    }


    public static function getAmount($billItemsID, $bills)
    {
        if (!is_array($bills)) return;
        foreach ($bills as $row) {
            if ($billItemsID == $row['bi_id'])
                return $row['amount'];
        }
    }


    /**
     * Get the details of a billing item by name
     * @param string $item_name
     * @return object $billing_item
     */
    public static function getItemByName($connect, $item_name)
    {
        $billing_item = null;

        $query = "SELECT * FROM billing_items WHERE bi_name = '{$item_name}'";

        $result = mysql_query($query, $connect);

        if ($result && mysql_num_rows($result) > 0) {
            $billing_item = mysql_fetch_object($result);
        }

        return $billing_item;
    }

}
