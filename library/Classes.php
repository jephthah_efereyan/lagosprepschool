<?php

class Classes
{

    function __construct()
    {

    }


    public static function getAll($connect)
    {
        $data = array();
        $query = "
			SELECT 
				*
			FROM 
				departments
			";

        if ($result = mysql_query($query, $connect)) {
            while ($row = mysql_fetch_array($result)) {
                $data[] = $row;
            }
        } else {
            //print mysql_error();
        }
        return $data;
    }


    /**
     * Get the details of a class by name
     * @param string $class_name
     * @return object $class
     */
    public static function getClassByName($connect, $class_name)
    {
        $class = null;

        $query = "
			SELECT
				*
			FROM
				departments
            WHERE
                departments_name = '{$class_name}'
			";

        $result = mysql_query($query, $connect);

        if ($result && mysql_num_rows($result) > 0) {
            $class = mysql_fetch_object($result);
        }

        return $class;
    }
}