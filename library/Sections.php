<?php

class Sections
{

    function __construct()
    {

    }


    public static function getAll($connect)
    {
        $data = array();
        $query = "
			SELECT 
				*
			FROM 
				faculties
			";

        if ($result = mysql_query($query, $connect)) {

            while ($row = mysql_fetch_array($result)) {
                $data[] = $row;
            }
        } else {
            //print mysql_error();
        }

        return $data;
    }

    /**
     * Get the details of a section by name
     * @param string $section_name
     * @return object $section
     */
    public static function getSectionByName($connect, $section_name)
    {
        $section = null;

        $query = "
			SELECT
				*
			FROM
				faculties
            WHERE
                faculties_name = '{$section_name}'
			";

        $result = mysql_query($query, $connect);

        if ($result && mysql_num_rows($result) > 0) {
            $section = mysql_fetch_object($result);
        }

        return $section;
    }
}