<?php

class Student
{
    public $student_id,
        $admission_no,
        $lastname,
        $firstname,
        $othernames,
        //$password,
        $gender,
        $dateofbirth,
        $placeofbirth,
        $nativity,
        $localgov,
        $stateoforigin,
        $nationality,
        $address,
        $otherprofile,
        $email,
        $disability,
       // $teachersname,
        $height,
        $weight,

        $faculties_name,
        $departments_name,
        $programme_name,
    	$parent_name, $parent_address, $parent_telephone, $billing_invoice_no;

    private $conn;

    public function __construct($conn, $admission_no)
    {
        $this->admission_no = $admission_no;

        $this->conn = $conn;
        $this->init();

    }

    public function init()
    {
        $admission_no = mysql_real_escape_string($this->admission_no);

        $query = "SELECT
				studentprofile.admission_no,
				studentprofile.lastname,
				studentprofile.firstname,
				studentprofile.othernames,
				studentprofile.password,
				studentprofile.gender,
				studentprofile.dateofbirth,
				studentprofile.placeofbirth,
				studentprofile.nativity,
				studentprofile.localgov,
				studentprofile.stateoforigin,
				studentprofile.nationality,
				studentprofile.address,
				studentprofile.otherprofile,
				studentprofile.email,
				studentprofile.disability,
				
				studentprofile.height,
				studentprofile.weight,
				studentprofile.parent_name, studentprofile.parent_address, studentprofile.parent_telephone,
				
				`studentprofile_extra`.`student_id`,
				
				faculties.faculties_name,
				departments.departments_name,
				programme.programme_name,
				`studentprofile_extra`.`billing_invoice_no`
			FROM 
				studentprofile
			LEFT JOIN `studentprofile_extra` ON `studentprofile_extra`.`student_id` = studentprofile.id
			LEFT JOIN faculties ON faculties.faculties_id = studentprofile_extra.faculty_id
			LEFT JOIN departments ON departments.departments_id =  studentprofile_extra.department_id
			LEFT JOIN programme ON `programme`.`programme_id` = `studentprofile_extra`.`programme_id`
			RIGHT JOIN `session_terms` ON `session_terms`.`session_term_id` = `studentprofile_extra`.`session_term_id`
			
			WHERE 
			
				`session_terms`.`status` = 'Current' AND 
				studentprofile.admission_no = '{$admission_no}'
      
       		ORDER BY `session_terms`.`session_term_id` DESC";

        if ($result = mysql_query($query, $this->conn)) {
            if (mysql_num_rows($result) > 0) {
                $data = mysql_fetch_assoc($result);
                foreach ($data as $key => $value) {
                    $this->$key = $value;
                }
            }
        } else {
            //print mysql_error();
        }

    }


    public static function getAll($connect, $term_id, $sections = '', $classes = '', $arms = '')
    {
        $data = array();

		$term_id = mysql_real_escape_string( (int)$term_id);
        $query = "
			SELECT 
				studentprofile.admission_no,
				studentprofile.lastname,
				studentprofile.firstname,
				studentprofile.othernames,
				studentprofile.password,
				studentprofile.gender,
				studentprofile.dateofbirth,
				studentprofile.placeofbirth,
				studentprofile.nativity,
				studentprofile.localgov,
				studentprofile.stateoforigin,
				studentprofile.nationality,
				studentprofile.address,
				studentprofile.otherprofile,
				studentprofile.email,
				studentprofile.disability,
				studentprofile.height,
				studentprofile.weight,
				
				faculties.faculties_name,
				departments.departments_name,
				programme.programme_name,
				`studentprofile_extra`.`billing_invoice_no`
			FROM 
				studentprofile
			LEFT JOIN `studentprofile_extra` ON `studentprofile_extra`.`student_id` = studentprofile.id
			LEFT JOIN faculties ON faculties.faculties_id = studentprofile_extra.faculty_id
			LEFT JOIN departments ON departments.departments_id =  studentprofile_extra.department_id
			LEFT JOIN programme ON `programme`.`programme_id` = `studentprofile_extra`.`programme_id`
			LEFT JOIN `session_terms` ON `session_terms`.`session_term_id` = `studentprofile_extra`.`session_term_id`
			
			WHERE 
				`session_terms`.`status` = 'Current' AND
				`session_terms`.`session_term_id` = {$term_id}";

        if ($sections)
            $query .= " AND faculties.faculties_id = {$sections}";
        if ($classes)
            $query .= " AND departments.departments_id = {$classes}";
        if ($arms)
            $query .= " AND programme.programme_id = {$arms}";


        $query .= " ORDER BY studentprofile.lastname ASC";

        if ($result = mysql_query($query, $connect)) {
            while ($row = mysql_fetch_assoc($result)) {
                $data[] = $row;
            }
        } else {
            //print mysql_error();
        }

        return $data;
    }

    /**
     * list all the session in which a student was registered
     */
    public function listSession()
    {

        $data = array();
        $query = "
			SELECT
				DISTINCT
				school_sessions.session_id,
				school_sessions.session_name AS session_name,
				school_sessions.session_fullname
			FROM
				studentprofile_extra
			LEFT JOIN session_terms ON session_terms.session_term_id = studentprofile_extra.session_term_id
			LEFT JOIN school_sessions ON school_sessions.session_id = session_terms.session_id
			
			WHERE studentprofile_extra.student_id = {$this->student_id};
			";


        if ($result = mysql_query($query, $this->conn)) {

            while ($row = mysql_fetch_assoc($result)) {
                $data[] = $row;
            }
        } else {
            print mysql_error();
        }
        return $data;


    }

    /**
     * list the terms in a student was registered in a session
     * @param int $sessionID
     */
    public function listTerm($sessionID)
    {
        $sessionID = mysql_real_escape_string($sessionID);
        $data = array();
        $query = "
		SELECT
		DISTINCT
		session_terms.session_term_id,
		school_terms.term_id,
		school_terms.term_name,
		school_terms.term_fullname
		FROM
		studentprofile_extra
		LEFT JOIN session_terms ON session_terms.session_term_id = studentprofile_extra.session_term_id
		LEFT JOIN school_terms ON school_terms.term_id = session_terms.term_id
			
		WHERE studentprofile_extra.student_id = {$this->student_id} AND 
		session_terms.session_id	= {$sessionID}
		";


        if ($result = mysql_query($query, $this->conn)) {

            while ($row = mysql_fetch_assoc($result)) {
                $data[] = $row;
            }

        } else {
            print mysql_error();
        }

        return $data;
    }
    
    public function update($connect, $data){
    	if(!$data) return false;
    	$fields = array(
    			'admission_no', 'lastname', 'firstname, studentprofile', 'password', 'gender', 'dateofbirth', 'placeofbirth',
    			'nativity','localgov','stateoforigin','nationality','address','otherprofile',
				'email','disability','height','weight','parent_name', 'parent_address', 'parent_telephone');
    	
    	$query = "UPDATE `studentprofile` SET  ";
    	
    	$i = 0;
    	foreach ($data as $key=>$value){
    		if(in_array($key, $fields)){
    			
    			if($i>0) $query .= ", ";
    			
    			$query .= " {$key} = '$value' ";
    			$i++;
    		}
    	}
    	
    	$query .= " WHERE id = {$this->student_id}";
    	
    	if(mysql_query($query, $connect)){
    		$this->init();
    		return mysql_affected_rows();
    	}
    	return false;
    }
}