<?php
class Terms
{
	
	public function __construct($sessionID)
	{
		
	}
	
	public static function getSessionTerms($connect, $sessionID)
	{
		$sessionID = mysql_real_escape_string((int)$sessionID);
		$data = array();
		$query = "
			SELECT 
	`school_terms`.`term_name`,
    `school_terms`.`term_fullname`,
    `session_terms`.`session_term_id` as `term_id`
    
FROM `school_terms` 

LEFT JOIN `session_terms` ON `session_terms`.`term_id` = `school_terms`.`term_id`

WHERE `session_terms`.`session_id` = {$sessionID}
			";
		
		
		if($result = mysql_query($query,$connect))
		{
			
			while($row = mysql_fetch_array($result))
			{
				$data[] = $row;
			}
				
		}
		else
		{
			print mysql_error();
		}
		return $data;
		
		
	}
	public static function getAll($connect)
	{
		$data = array();
		$query = "
			SELECT 
				*
			FROM 
				school_terms
			";
		
		
		if($result = mysql_query($query,$connect))
		{
			
			while($row = mysql_fetch_array($result))
			{
				$data[] = $row;
			}
				
		}
		else
		{
			//print mysql_error();
		}
		return $data;
		
		
	}
	public static function currentSessionTerm($connect)
	{
		$query = "
			SELECT
				session_terms.session_term_id
			FROM
				session_terms
			RIGHT JOIN school_terms on school_terms.term_id = session_terms.term_id
			WHERE status = 'current'
				
			ORDER BY session_term_id DESC
			
			LIMIT 0, 1";
	
	
		if($result = mysql_query($query,$connect))
		{
	
			if($data = mysql_fetch_assoc($result))
			{
				return $data['session_term_id'];
			}
	
		}
		else
		{
			print mysql_error();
		}
		return false;
	}
}