<?php



class Transaction

{

    public static function studentPaymentHistory($connect, $student_id)

    {

        $data = array();



        $query = "SELECT

					*

				FROM transactions 

				WHERE student_id = '{$student_id}' AND trans_status = 'Paid'

				ORDER BY trans_date DESC";



        $result = mysql_query($query, $connect);



        if ($result && mysql_num_rows($result) > 0) {

            while ($row = mysql_fetch_assoc($result)) {

                $data[] = $row;

            }

        }

        // print mysql_error();

        return $data;



    }



    public static function history($connect)

    {

        $data = array();



        $query = "

        SELECT

			t.*, CONCAT_WS(' ', sp.lastname, sp.firstname, sp.othernames) as name, sp.admission_no

		FROM transactions t

		JOIN studentprofile sp ON sp.id =  t.student_id

		LEFT JOIN `studentprofile_extra` spe ON spe.`student_id` = t.student_id AND spe.`session_term_id` = t.session_term_id

		WHERE t.trans_status = 'Paid'

		ORDER BY t.trans_date DESC";



        $result = mysql_query($query, $connect);



        if ($result && mysql_num_rows($result) > 0) {

            while ($row = mysql_fetch_assoc($result)) {

                $details = self::details($connect, $row['id']);

                if (!empty($details))

                    $row['items'] = $details;



                $data[] = $row;

            }

        }print mysql_error();



        return $data;

    }



    /**

     * @param object $connect

     * @param int $trans_id

     *

     * @return array $details

     */

    public static function details($connect, $trans_id)

    {

        $data = array();



        $query = "SELECT bi.bi_name AS item, td.amount

                    FROM transaction_details td

                    JOIN billing_items bi ON td.item_id = bi.bi_id

                    WHERE td.transaction_id = {$trans_id}";

        $result = mysql_query($query, $connect);



        if ($result && mysql_num_rows($result) > 0) {

            while ($row = mysql_fetch_assoc($result)) {

                $data[] = $row;

            }

        }



        return $data;

    }
	static function amount_from_details($item, $details){
		
		if($details and is_array($details)){
				foreach($details as $value){
					if($value['item'] == $item)
						return $value['amount'];
				}
		}
		return '';
	}

}

