<?php

/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 6/7/2016
 * Time: 2:19 PM
 */
class Administrator
{
    public $id,
        $email,
        $surname,
        $firstname,
        $role_id,
        $active,
        $created_at,
        $updated_at,
        $role;

    private $conn;


    /**
     * Authenticate an administrator
     *
     * @param string $username
     * @param string $password
     *
     * @return object $administrator
     */
    public static function authenticate($conn, $email, $password)
    {
        $retVal = null;

        if (!empty($email) && !empty($password)) {
            $sql = "SELECT a.*, r.description AS role
                      FROM administrator a
                      JOIN roles r ON r.id = a.role_id
                      WHERE a.email = '{$email}' AND a.password = md5('{$password}')";
            $result = mysql_query($sql, $conn);

            if ($result && mysql_num_rows($result) > 0) {
                $retVal = self::instantiate(mysql_fetch_assoc($result));
            }
        }

        return $retVal;
    }


    /**
     * Fetch an administrator
     *
     * @param int $id
     *
     * @return object $administrator
     */
    public static function get($conn, $id)
    {
        $retVal = null;

        $sql = "SELECT a.*, r.description AS role
                  FROM administrator a
                  JOIN roles r ON r.id = a.role_id
                  WHERE a.id = {$id}";
        $result = mysql_query($sql, $conn);

        if ($result && mysql_num_rows($result) > 0) {
            $retVal = self::instantiate(mysql_fetch_assoc($result));
        }

        return $retVal;
    }


    /**
     * Fetch all administrator
     *
     * @return array $administrators
     */
    public static function getAll($conn)
    {
        $retVal = array();

        $sql = "SELECT a.*, r.description AS role
                  FROM administrator a
                  JOIN roles r ON r.id = a.role_id";
        $result = mysql_query($sql, $conn);

        if ($result && mysql_num_rows($result) > 0) {
            while ($administrator = mysql_fetch_assoc($result))
                $retVal[] = self::instantiate($administrator);
        }

        return $retVal;
    }


    public static function create($connect, $data)
    {
        @$surname = mysql_real_escape_string($data['surname']);
        @$firstname = mysql_real_escape_string($data['firstname']);
        @$email = mysql_real_escape_string($data['email']);
        @$password = md5($email);

        $sql = "INSERT INTO administrator (surname, firstname, email, `password`, role_id, created_at)
                VALUES ('{$surname}', '{$firstname}', '{$email}', '{$password}', 2, NOW())";

        if (mysql_query($sql, $connect)) {
            $id = mysql_insert_id();

            $retVal = self::get($connect, $id);
        } else {
            if (self::emailExists($connect, $email)) {
                $retVal = false;
            }
        }

        return $retVal;
    }


    /**
     * Change an Administrator's password
     *
     * @param string $new_password
     * @param string $old_password
     * @param int $id
     *
     * @return boolean
     */
    public static function changePassword($conn, $new_password, $old_password, $id)
    {
        $retVal = null;

        if (!empty($new_password) && !empty($old_password) && !empty($id)) {
            $sql = "UPDATE administrator SET `password` = '{$new_password}' WHERE id = {$id} AND `password` = '{$old_password}'";

            if (mysql_query($sql, $conn)) {
                if (mysql_affected_rows() > 0)
                    $retVal = true;
            }
        }

        return $retVal;
    }


    /**
     * Save an Administrator's details
     *
     * @param string $password
     * @param int $id
     *
     * @return boolean
     */
    public static function validPassword($conn, $password, $id)
    {
        $retVal = false;

        if (!empty($password) && !empty($id)) {
            $sql = "SELECT password FROM administrator WHERE id = {$id}";
            $result = mysql_query($sql, $conn);
            if ($result && mysql_num_rows($result) > 0) {
                if (mysql_result($result, 0) == $password)
                    $retVal = true;
            }
        }

        return $retVal;
    }


    /**
     * Save an Administrator's details
     *
     * @param string $email
     *
     * @return object $administrator
     */
    public static function addAdmin($conn, $data)
    {
        $retVal = null;

        if (!empty($data)) {
            $sql = "INSERT INTO administrator (surname, firstname, email, `password`, role_id, created_at)
                    VALUES ('{$data['surname']}', '{$data['firstname']}', '{$data['email']}', '{$data['password']}', 2, NOW())";

            if (mysql_query($sql, $conn)) {
                $id = mysql_insert_id();
                $sql = "SELECT * FROM administrator WHERE id = {$id}";
                $result = mysql_query($sql, $conn);
                $retVal = self::instantiate(mysql_fetch_assoc($result));
            }
        }

        return $retVal;
    }


    /**
     * Update an Administrator's profile record
     *
     * @param array $data
     *
     * @return object $administrator
     */
    public static function updateAdmin($conn, $data)
    {
        $retVal = null;

        if (!empty($data) && !empty($data['id'])) {
            $sql = "UPDATE administrator
                    SET surname = '{$data['surname']}',
                        firstname = '{$data['firstname']}',
                        email = '{$data['email']}'";
            $sql .= !empty($data['new_password']) ? ", password = '{$data['new_password']}'" : "";
            $sql .= " WHERE id = {$data['id']}";

            if (mysql_query($sql, $conn)) {
                $retVal = self::get($conn, $data['id']);
            }
        }

        return $retVal;
    }


    /**
     * Activate/Deactivate an Administrator
     *
     * @param int $id
     *
     * @return boolean
     */
    public static function activateAdmin($conn, $id)
    {
        $retVal = false;

        if (!empty($id)) {
            $sql = "UPDATE administrator SET active = IF(active, false, true) WHERE id = {$id}";

            if (mysql_query($sql, $conn)) {
                $retVal = true;
            }
        }

        return $retVal;
    }


    /**
     *
     * @return boolean
     */
    public static function emailExists($connect, $email)
    {
        $retVal = false;

        $query = "SELECT 1 FROM administrator WHERE email = '{$email}'";
        if ($result = mysql_query($query, $connect) and mysql_num_rows($result) > 0) {
            $retVal = true;
        }

        return $retVal;
    }


    /**
     * @param $values
     * @return $object
     */
    private static function instantiate($values)
    {
        $obj = new Administrator();
        foreach (get_class_vars(get_class($obj)) as $attr => $value) {
            if (array_key_exists($attr, $values))
                $obj->$attr = $values[$attr];
        }

        return $obj;
    }
}