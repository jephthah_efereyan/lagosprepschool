<?php

session_start();
if (!isset($_SESSION['logged']) || $_SESSION['logged'] != 4) {
    $redirect = $_SERVER['PHP_SELF'];
    $tc_level = isset($_REQUEST['tc_level']) ? $_REQUEST['tc_level'] : "";
    header("Refresh: 2; URL=login.php?redirect=$redirect&tc_level=$tc_level");
    echo "You are being redirected to the login page!<br>";
    echo "(If your browser doesn't support this, <a href='login.php?redirect=$redirect&tc_level=$tc_level'>click here</a>)";
    die();
}
