<?php
include_once("lang.php");
require("header_leftnav.inc.php");
include("../admin/functions.php");
?>

<td valign="top" style="padding: 5px">
    <script language="javascript" type="text/javascript">
        function checkform(form) {
            if (document.form.yss.value == "") {
                alert("Please select Session");
                document.form.yss.focus();
                return false;
            }
        }
    </script>
    <?php //die("<pre>".print_r($_SESSION,1)); ?>
    <h3 class="page-title">Fees</h3>
    <form name="form" method="post" action="fees.php">
        <input name="admission_no" type="hidden" id="admission_no" value="<?php print $admission_no ?>">
        <table width="100%" class="t">
            <tr>
                <td>Session</td>
                <td>
                    <?php $sessions = GetSessions(); ?>
                    <select name="session_id">
                        <?php
                        foreach ($sessions as $session) {
                            echo "<option value='{$session['session_id']}'";
                            echo $session['session_name'] == $_SESSION['current_session_term']['session_name'] ? " selected" : "";
                            echo ">" . $session['session_fullname'] . "</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Term</td>
                <td>
                    <?php $terms = GetTerms(); ?>
                    <select name="term_id">
                        <?php
                        foreach ($terms as $term) {
                            echo "<option value='{$term['term_id']}'";
                            echo $term['term_name'] == $_SESSION['current_session_term']['term_name'] ? " selected" : "";
                            echo ">{$term['term_fullname']}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <!--<tr>
                <td>Class</td>
                <td>
                    <?php /*$faculties = GetFaculties(); */?>
                    <select name="pay_faculty">
                        <?php
/*                        foreach ($faculties as $faculty) {
                            echo "<option value='{$faculty['faculties_name']}'";
                            echo !empty($_SESSION['current_class']['class_level']) && $_SESSION['current_class']['class_level'] == $faculty['faculties_name'] ? " selected" : "";
                            echo ">{$faculty['faculties_name']}</option>";
                        }
                        */?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Level</td>
                <td>
                    <?php /*$departments = GetDepartments(); */?>
                    <select name="pay_department">
                        <?php
/*                        foreach ($departments as $department) {
                            echo "<option value='{$department['departments_name']}'";
                            echo !empty($_SESSION['current_class']['class_no']) && $_SESSION['current_class']['class_no'] == $department['departments_name'] ? " selected" : "";
                            echo ">{$department['departments_name']}</option>";
                        }
                        */?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Nationality</td>
                <td>
                    <select name="nationality" id="nationality">
                        <option value="Nigerian" selected="selected">Nigerian</option>
                        <option value="Non Nigerian"> Non Nigerian</option>
                    </select>
                </td>
            </tr>-->
            <tr>
                <td align="right"><input type="reset" name="Reset" value="Reset"></td>
                <td><input name="Submit" type="submit" value="Continue" onClick="return checkform(this)"></td>
            </tr>
        </table>
    </form>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
</body>
</html>