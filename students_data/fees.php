<?php
require("header_leftnav.inc.php");
include("../admin/functions.php");
?>
<style>
    .item-amount {
        padding: 5px
    }
    #po {
        margin-bottom: 10px;
        font-size: 14px;
    }
    #feesForm select {
        padding: 5px;
    }
    #feesTable th, #feesTable td {
        border: 1px solid;
        padding: 5px
    }
    #currency-value {
        font-size: 14px;
        color: red;
        background: #ffffff;
        padding: 5px
    }
</style>
<td valign="top" style="padding: 5px" ng-controller="PaymentCtrl">
    <h3 class="page-title">Fees</h3>

    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <form name="form" method="post" action="transact_confirm.php" id="feesForm">
                    <div id="po">
                        Payment Option:
                        <select name="payment_option" onchange="toggleCurrency(this.value)">
                            <option value="">Select</option>
                            <option value="BranchCollect">Local Payment</option>
                            <option value="MasterCard">International Payment</option>
                        </select>
                    </div>

                    <div style="display: none" id="feesDetails">
                        <p id="currency-value"></p>

                        <table id="feesTable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ITEM NAME</th>
                                <th>AMOUNT (<span class="currency">&#x20a6;</span>)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="item in billingItems">
                                <td>{{ item.sn }}</td>
                                <td>{{ item.name }}</td>
                                <td><input type="number" name="amount[{{ item.id }}]" class="item-amount"
                                           ng-model="item.amount" ng-keyup="enterAmount(item.amount)"></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="2" align="right">Total</th>
                                <th align="left"><span class="currency">&#x20a6;</span> {{ totalAmount }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <div style="margin: 10px 0">
                            <p><input type="submit" name="Submit" value="Proceed"
                                      style="padding: 10px 20px; font-size: 14px"></p>
                        </div>
                    </div>
                </form>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
</body>
</html>

<script>
    function toggleCurrency(po) {
        var info, currency;

        if (po != '') {
            if (po == 'BranchCollect') {
                currency = '&#x20a6;';
                info = 'Please enter the amounts in Naira';
            } else {
                currency = '&#36;';
                info = 'Please enter the amounts in Dollars';
            }

            $('span.currency').html(currency);
            $('p#currency-value').html(info);

            $("#feesDetails").css({display:'block'});
        } else {
            $('span.currency').html('');
            $('p#currency-value').html('');
            $("#feesDetails").css({display:'none'});
        }
    }
</script>