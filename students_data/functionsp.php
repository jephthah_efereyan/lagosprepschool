<?php

function GetSessions() {
    $sessions = array();
    $sql = "SELECT * FROM school_sessions ORDER BY session_name DESC";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($session = mysql_fetch_assoc($result))
            $sessions[] = $session;
    }
    return $sessions;
}

function GetTerms() {
    $terms = array();
    $sql = "SELECT * FROM school_terms";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($term = mysql_fetch_assoc($result))
            $terms[] = $term;
    }
    return $terms;
}

function GetFaculties() {
    $faculties = array();
    $sql = "SELECT * FROM faculties";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($faculty = mysql_fetch_assoc($result))
            $faculties[] = $faculty;
    }
    return $faculties;
}

function GetDepartments() {
    $departments = array();
    $sql = "SELECT * FROM departments";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($department = mysql_fetch_assoc($result))
            $departments[] = $department;
    }
    return $departments;
}

function GetCurrentSessionFullName() {
    $sql = "SELECT `year` FROM current_term WHERE current = 'Yes'";
    $result = mysql_query($sql);
    $current_session = mysql_num_rows($result) > 0 ? mysql_result($result, 0) : "";
    if (!empty($current_session))
        $current_session = $current_session . "/" . ((int)$current_session + 1);
    return $current_session;
}

function GetCurrentSessionY()
{
    $sql = "SELECT `year` FROM current_term WHERE `current` = 'Yes'";
    $result = mysql_query($sql);
    $current_session = mysql_num_rows($result) > 0 ? mysql_result($result, 0) : "";
    return $current_session;
}

function GetCurrentTerm()
{
    $sql = "SELECT `term` FROM current_term WHERE current = 'Yes'";
    $result = mysql_query($sql);
    $term = mysql_num_rows($result) > 0 ? mysql_result($result, 0) : "";
    return $term;
}

function GetCurrentTermFullName()
{
    $sql = "SELECT `term` FROM current_term WHERE current = 'Yes'";
    $result = mysql_query($sql);
    $term = mysql_num_rows($result) > 0 ? mysql_result($result, 0) : 0;
    if ($term > 0) {
        if ($term == 1)
            $term = "First Term";
        elseif ($term == 2)
            $term == "Second Term";
        elseif ($term == 3)
            $term = "Third Term";
    }
    return $term;
}

function GetFees($session, $term, $faculty, $class, $nationality) {
    $fees = array();
    $sql = "SELECT f.*, fi.*, s.session_name, t.term_name FROM fees f
            JOIN fees_items fi ON f.fee_item_id = fi.item_id
            JOIN school_sessions s ON f.session_id = s.session_id
            JOIN school_terms t ON f.term_id = t.term_id
            WHERE f.session_id = '$session' AND f.term_id = '$term' AND f.faculty_id = '$faculty' AND f.department_id = '$class' AND f.fee_nationality = '$nationality'";
    $result = mysql_query($sql);
    if (mysql_num_rows($result) > 0) {
        while ($fee = mysql_fetch_assoc($result))
            $fees[] = $fee;
    }
    return $fees;
}

function GetStudentTransactions($student_id) {
    $transactions = array();
    $sql = "SELECT DISTINCT(trans_no), trans_status, trans_date FROM transactiontotal WHERE student_id = '$student_id' ORDER BY transtotal_id DESC";
    $result = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($result) > 0) {
        while ($transaction = mysql_fetch_assoc($result))
            $transactions[] = $transaction;
    }

    return $transactions;
}

function GetStudentClass($admission_no, $session, $term) {
    $student_class = array();
    $sql = "SELECT spe.class_level, spe.class_no, spe.class_letter, f.faculties_id, d.departments_id, p.programme_id
            FROM studentprofile_extra spe
            LEFT JOIN faculties f ON spe.class_level = f.faculties_name
            LEFT JOIN departments d ON spe.class_no = d.departments_name
            LEFT JOIN programme p ON spe.class_letter = p.programme_name
            WHERE studentprofile_id = '{$admission_no}' AND yearsession = '{$session}' AND term = '{$term}'";
    $result = mysql_query($sql) or die(mysql_error());
    if (mysql_num_rows($result) > 0)
        $student_class = mysql_fetch_assoc($result);

    return $student_class;
}

function GetAmount($scholar)
{
    $year = "SELECT * FROM scholarship WHERE item_id = '$scholar'";
    $ryear = mysql_query($year);
    while ($tab = mysql_fetch_array($ryear)) {
        //$num = mysql_num_rows($ryear);
        $res1a = $tab['item_name'];
        $res1b = $tab['item_faculty'];
        $res1c = $tab['item_custom3'];
        $res1d = $tab['item_custom4'];
    }
    $year = "SELECT * FROM fees_scholars WHERE item_name = '$res1a' and item_faculty = '$res1b' and item_custom3 = '$res1c' and item_custom4 = '$res1d'";
    $ryear = mysql_query($year);
    $num = mysql_num_rows($ryear);
    while ($tab = mysql_fetch_array($ryear)) {
        //$num = mysql_num_rows($ryear);
        $res1 = $tab['item_amount'];
    }

    //echo "$res++++$rows*****$num";
    return $res1;
}

?>
