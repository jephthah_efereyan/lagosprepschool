<?php
set_time_limit(1000);
function computeStudentsAge($birthyear, $birthmonth, $yearsession, $term)
{
    $sf = "";
    $student_age = "";
    if ($term == "firstterm") {
        $ref_year = $yearsession;
        $ref_month = $yearsession == date("Y") ? date("n") : 9; //if session name is same as current year, use current month; else, use September
    }
    elseif ($term == "secondterm") {
        $ref_year = $yearsession + 1;
        $ref_month = $yearsession + 1 == date("Y") ? date("n") : 1; //if session's lastname is same as current year, use current month; else use January
    }
    elseif ($term == "thirdterm") {
        $ref_year = $yearsession + 1;
        $ref_month = $yearsession + 1 == date("Y") ? date("n") : 5; //if session's lastname is same as current year, use current month; else use May;
    }
    if ($birthyear != '' || $birthyear != 0) {
        $age = $ref_year - $birthyear;
        if ($age < 0)
            return "0";
        if ($age > 0 && $birthmonth > $ref_month)
            $age -= 1;
        if ($age > 1)
            $sf = "yrs";
        elseif ($age == 1)
            $sf = "yr";
        elseif ($age == 0) {
            if ($birthmonth < $ref_month)
                $age = date("n") - $birthmonth;
            else $age = 12 - ($birthmonth - $ref_month);
            if ($age == 1)
                $sf = "mth";
            if ($age == 12) {
                $age = 1;
                $sf = "yr";
            }
            else {
                $sf = "mths";
            }
        }
        $student_age = $age . $sf;
    }

    return $student_age;
}

function computeClassAvgAge($class_level, $class_no, $class_letter, $yearsession, $term)
{
    $query = "SELECT age FROM studentprofile_extra spe
                WHERE spe.yearsession = '$yearsession'
                    AND spe.term = '$term'
                    AND spe.class_level = '$class_level'
                    AND spe.class_no = '$class_no'
                    AND spe.class_letter = '$class_letter'
                    AND spe.age != '0'";
    $result = mysql_query($query) or die(mysql_error());
    if ($result) {
        $sum = 0;
        $num = mysql_num_rows($result);
        while ($row = mysql_fetch_array($result)) {
            $age = $row['age'];
            if (strpos($age, 'm') !== FALSE) {
                $age = 1; //if age is less than 1 year, use 1 year
            }
            $sum += (int)$age;
        }
        $avg_age = number_format($sum / $num, 0);
        $avg_age .= $avg_age == 1 ? "yr" : "yrs";

        return $avg_age;
    }
}

function updateClassAvgAge($class_level, $class_no, $class_letter, $yearsession, $term)
{
    $class_avg_age = computeClassAvgAge($class_level, $class_no, $class_letter, $yearsession, $term);
    $update = "UPDATE studentprofile_extra spe
                SET class_avg_age = '$class_avg_age'
                WHERE spe.yearsession = '$yearsession'
                    AND spe.term = '$term'
                    AND spe.class_level = '$class_level'
                    AND spe.class_no = '$class_no'
                    AND spe.class_letter = '$class_letter'
                    AND spe.age != '0'";
    mysql_query($update);

    return mysql_affected_rows();
}

function Absent2($numbr)
{
    if (($numbr == "0") OR ($numbr == "")) {
        $numbr = "";
    }
    else {
        $numbr = number_format($numbr, 2);
    }

    return $numbr;
}

function Absent($numbr)
{
    if (($numbr == "0") OR ($numbr == "")) {
        $numbr = "--";
    }
    else {
        $numbr = number_format($numbr, 2);
    }

    return $numbr;
}

function Overall_bk($atestmark1, $atestmark2)
{
    if ((($atestmark1 == 0) OR ($atestmark1 == " ")) AND ($atestmark2 > 0)) {
        $overrall = 80;
    }
    elseif ((($atestmark2 == 0) OR ($atestmark2 == " ")) AND ($atestmark1 > 0)) {
        $overrall = 80;
    }
    elseif ((($atestmark2 == 0) OR ($atestmark2 == " ")) AND (($atestmark1 == 0) OR ($atestmark1 == " "))) {
        $overrall = 80;
    }
    else {
        $overrall = 100;
    }

    return $overrall;
}

function Overall($atestmark1, $atestmark2)
{
    if ((int)$atestmark1 > 0 && (int)$atestmark2 > 0) {
        $overrall = 100;
    }
    else {
        $overrall = 80;
    }

    return $overrall;
}

function GetF($subject)
{
    global $admission_no, $term, $yearsession;
    $query1 = "SELECT *
            FROM studentsubjects
            WHERE ss_admission LIKE '%$admission_no%' AND
            s_term = 'firstterm' AND
            s_session = '$yearsession' AND
            subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $ftotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $ftotal = $ftotal / $overrall * 100;
    $ftotal = Absent($ftotal);

    return $ftotal;
}

function GetS($subject)
{
    global $admission_no, $term, $yearsession;
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $stotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $stotal = $stotal / $overrall * 100;
    $stotal = Absent($stotal);

    return $stotal;

}

//for abnormal CA1 and CA2 calculation and Term
function GetTerm($subject, $term)
{
    global $admission_no, $yearsession;
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_term = '$term' AND
	s_session = '$yearsession' AND
	subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal = Absent($mytotal);

    return $mytotal;
}

//for abnormal CA1 and CA2 calculation
function GetN($subject)
{
    global $admission_no, $term, $yearsession;
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_term = '$term' AND
	s_session = '$yearsession' AND
	subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal = Absent($mytotal);

    return $mytotal;
}

function GetNYear($subject)
{
    global $admission_no, $term, $yearsession;
    $query = "SELECT * FROM studentsubjects
                WHERE ss_admission LIKE '%$admission_no%'
                    AND s_session = '$yearsession'
                    AND s_term = 'firstterm'
                    AND subject = '$subject'";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result);
    $atestmark1 = $row["testmark1"];
    $atestmark2 = $row["testmark2"];
    $atestmark3 = $row["testmark3"];
    $atestmark4 = $row["testmark4"];
    $atestmark5 = $row["testmark5"];
    $atestmark6 = $row["testmark6"];
    $aothertest = $row["othertest"];
    $afinalexam = $row["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal1 = Absent2($mytotal);
    $query1 = "SELECT * FROM studentsubjects
                WHERE ss_admission LIKE '%$admission_no%'
                    AND s_session = '$yearsession'
                    AND s_term = 'secondterm'
                    AND subject = '$subject'";
    $result1 = mysql_query($query1);
    $row1 = mysql_fetch_array($result1);
    $atestmark1 = $row1["testmark1"];
    $atestmark2 = $row1["testmark2"];
    $atestmark3 = $row1["testmark3"];
    $atestmark4 = $row1["testmark4"];
    $atestmark5 = $row1["testmark5"];
    $atestmark6 = $row1["testmark6"];
    $aothertest = $row1["othertest"];
    $afinalexam = $row1["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal2 = Absent2($mytotal);
    $query2 = "SELECT * FROM studentsubjects
                WHERE ss_admission LIKE '%$admission_no%'
                    AND s_session = '$yearsession'
                    AND s_term = 'thirdterm'
                    AND subject = '$subject'";
    $result2 = mysql_query($query2);
    $row2 = mysql_fetch_array($result2);
    $atestmark1 = $row2["testmark1"];
    $atestmark2 = $row2["testmark2"];
    $atestmark3 = $row2["testmark3"];
    $atestmark4 = $row2["testmark4"];
    $atestmark5 = $row2["testmark5"];
    $atestmark6 = $row2["testmark6"];
    $aothertest = $row2["othertest"];
    $afinalexam = $row2["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal3 = Absent2($mytotal);
    if (($mytotal1) AND ($mytotal2) AND ($mytotal3)) {
        $div = 3;
    }
    elseif (($mytotal1) AND ($mytotal2) AND (empty($mytotal3))) {
        $div = 2;
    }
    elseif (($mytotal1) AND (empty($mytotal2)) AND ($mytotal3)) {
        $div = 2;
    }
    elseif ((empty($mytotal1)) AND ($mytotal2) AND ($mytotal3)) {
        $div = 2;
    }
    elseif ((empty($mytotal1)) AND (empty($mytotal2)) AND ($mytotal3)) {
        $div = 1;
    }
    elseif ((empty($mytotal1)) AND ($mytotal2) AND (empty($mytotal3))) {
        $div = 1;
    }
    elseif (($mytotal1) AND (empty($mytotal2)) AND (empty($mytotal3))) {
        $div = 2;
    }
    else {
        $div = 3;
    }
    $dmytotal = ($mytotal1 + $mytotal2 + $mytotal3) / $div;

    return $dmytotal;
}

function GetNYearSpecial($subject, $admission_no, $term, $yearsession)
{
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_session = '$yearsession' AND
	s_term = 'firstterm' AND
	subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal1 = Absent2($mytotal);
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_session = '$yearsession' AND
	s_term = 'secondterm' AND
	subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal2 = Absent2($mytotal);
    $query1 = "SELECT *
            FROM studentsubjects
            WHERE ss_admission LIKE '%$admission_no%' AND
            s_session = '$yearsession' AND
            s_term = 'thirdterm' AND
            subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
    $overrall = Overall($atestmark1, $atestmark2);
    $mytotal = $mytotal / $overrall * 100;
    $mytotal3 = Absent2($mytotal);
    if (($mytotal1) AND ($mytotal2) AND ($mytotal3)) {
        $div = 3;
    }
    elseif (($mytotal1) AND ($mytotal2) AND (empty($mytotal3))) {
        $div = 2;
    }
    elseif (($mytotal1) AND (empty($mytotal2)) AND ($mytotal3)) {
        $div = 2;
    }
    elseif ((empty($mytotal1)) AND ($mytotal2) AND ($mytotal3)) {
        $div = 2;
    }
    elseif ((empty($mytotal1)) AND (empty($mytotal2)) AND ($mytotal3)) {
        $div = 1;
    }
    elseif ((empty($mytotal1)) AND ($mytotal2) AND (empty($mytotal3))) {
        $div = 1;
    }
    elseif (($mytotal1) AND (empty($mytotal2)) AND (empty($mytotal3))) {
        $div = 2;
    }
    else {
        $div = 3;
    }
    $dmytotal = ($mytotal1 + $mytotal2 + $mytotal3) / $div;

    return $dmytotal;
}

function GetNYearInsert($subject)
{
    global $admission_no, $term, $yearsession;
    $value1 = GetNYear($subject);
    $create_temp = "CREATE TEMPORARY TABLE IF NOT EXISTS getnyearinsert (
                        id int(5) NOT NULL auto_increment,
                        tadmission VARCHAR(255),
                        tmark2 DECIMAL(6,2),
                        PRIMARY KEY  (id)
                    )";
    $results_c = mysql_query($create_temp) or die(mysql_error());
    $insert = "INSERT INTO getnyearinsert (id, tadmission, tmark2)
                VALUES ('$id', '$admission_no', '$value1')";
    $results_i = mysql_query($insert);
}

function MyYearAvg($admission_no)
{
    global $admission_no, $term, $yearsession;
    $query1 = "SELECT AVG(tmark2) AS tmark2 FROM getnyearinsert";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $tmark2 = $arow["tmark2"];

    return $tmark2;
}

function getSEDStudentCumulativeAverage()
{
    $sql = "SELECT course_id";
}

function getSEDStudentSubjectCumulativeAverage($student_id, $course_id)
{
    global $term, $yearsession;
    if ($term == 'firstterm') {
        $terms = array('firstterm');
    }
    elseif ($term == 'secondterm') {
        $terms = array('firstterm', 'secondterm');
    }
    else {
        $terms = array('firstterm', 'secondterm', 'thirdterm');
    }
    $percent_totals = array();
    foreach ($terms as $_term) {
        $sql = "SELECT ca_1_score, ca_2_score, ca_1_score+ca_2_score+ca_3_score+ca_4_score+exam_score AS total_score
                FROM sed_results
                WHERE student_id = $student_id
                    AND course_id = $course_id
                    AND sch_term = '$_term'
                    AND sch_session = '$yearsession'";
        $resource = mysql_query($sql);
        if (mysql_num_rows($resource) > 0) {
            $row = mysql_fetch_assoc($resource);
            extract($row); //$ca_1_score, $ca_2_score, $total_score
            $overall = Overall($ca_1_score, $ca_2_score);
            $percent_total = ($total_score / $overall) * 100;
            $percent_totals[] = $percent_total;
        }
    }
    if (!empty($percent_totals)) {
        $cumulative_average = array_sum($percent_totals) / count($percent_totals);
    }

    return $cumulative_average;
}

function GetWeigthedAvg($subject)
{
    global $admission_no, $term, $yearsession;
    if ($term == "thirdterm") {
        $query1 = "SELECT AVG(testmark1+testmark2+testmark3+testmark4+testmark5+testmark6+finalexam) AS average
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_session = '$yearsession' AND
	subject = '$subject'";
    }
    elseif ($term == "secondterm") {
        $query1 = "SELECT AVG(testmark1+testmark2+testmark3+testmark4+testmark5+testmark6+finalexam) AS average
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	(s_term = 'firstterm' OR s_term = 'secondterm') AND
	s_session = '$yearsession' AND
	subject = '$subject'";
    }
    else {
        $query1 = "SELECT AVG(testmark1+testmark2+testmark3+testmark4+testmark5+testmark6+finalexam) AS average
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	subject = '$subject'";


    }
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $atestmark1 = $arow["testmark1"];
    $atestmark2 = $arow["testmark2"];
    $atestmark3 = $arow["testmark3"];
    $atestmark4 = $arow["testmark4"];
    $atestmark5 = $arow["testmark5"];
    $atestmark6 = $arow["testmark6"];
    $aothertest = $arow["othertest"];
    $afinalexam = $arow["finalexam"];
    $average = $arow["average"];
    $average = number_format($average, 2);
    $average = Absent($average);

    return $average;
}

function GetGrade($thedwmark)
{
    global $class_no;
    if ($class_no <= 9) {
        if ($thedwmark >= 80) {
            $themygrade = "A";
            $studentcumulativemark = $graderemark1;
        }
        elseif (($thedwmark >= 60) AND ($thedwmark < 80)) {
            $themygrade = "C";
            $studentcumulativemark = $graderemark2;
        }
        elseif (($thedwmark >= 55) AND ($thedwmark < 60)) {
            $themygrade = "P";
            $studentcumulativemark = $graderemark3;
        }
        elseif (($thedwmark >= 0.01) AND ($thedwmark < 55)) {
            $themygrade = "<span style='color: red;'>F</span>";
            $studentcumulativemark = "<span style='color: red;'>$graderemark6</span>";
        }
        else {
            $themygrade = "<span style='color: red;'>--</span>";
            $studentcumulativemark = "<span style='color: red;'>$graderemark7</span>";
        }

    }
    else {
        if ($thedwmark >= 80) {
            $themygrade = "A1";
            $studentcumulativemark = $graderemark1;
        }
        elseif (($thedwmark >= 75) AND ($thedwmark < 80)) {
            $themygrade = "B2";
            $studentcumulativemark = $graderemark2;
        }
        elseif (($thedwmark >= 70) AND ($thedwmark < 75)) {
            $themygrade = "B3";
            $studentcumulativemark = $graderemark3;
        }
        elseif (($thedwmark >= 65) AND ($thedwmark < 70)) {
            $themygrade = "C4";
            $studentcumulativemark = $graderemark4;
        }
        elseif (($thedwmark >= 60) AND ($thedwmark < 65)) {
            $themygrade = "C5";
            $studentcumulativemark = $graderemark5;
        }
        elseif (($thedwmark >= 55) AND ($thedwmark < 60)) {
            $themygrade = "C6";
            $studentcumulativemark = $graderemark5;
        }
        elseif (($thedwmark >= 50) AND ($thedwmark < 55)) {
            $themygrade = "P7";
            $studentcumulativemark = $graderemark5;
        }
        elseif (($thedwmark >= 45) AND ($thedwmark < 50)) {
            $themygrade = "C6";
            $studentcumulativemark = $graderemark5;
        }
        elseif (($thedwmark >= 0.01) AND ($thedwmark < 45)) {
            $themygrade = "<span style='color: red;'>F9</span>";
            $studentcumulativemark = "<span style='color: red;'>$graderemark6</span>";
        }
        else {
            $themygrade = "<span style='color: red;'>--</span>";
            $studentcumulativemark = "<span style='color: red;'>$graderemark7</span>";
        }


    }

    return $themygrade;

}

function GetRemark($thedwmark)
{
    $graderemark1 = "EXCELLENT";
    $graderemark2 = "VERY GOOD";
    $graderemark3 = "GOOD";
    $graderemark4 = "FAIR";
    $graderemark5 = "POOR";
    $graderemark6 = "FAIL";
    $graderemark7 = "ABSENT";
    if ($thedwmark >= 80) {
        $themygrade = A;
        $studentcumulativemark = $graderemark1;
    }
    elseif (($thedwmark >= 70) AND ($thedwmark <= 79.99)) {
        $themygrade = B;
        $studentcumulativemark = $graderemark2;
    }
    elseif (($thedwmark >= 60) AND ($thedwmark <= 69.99)) {
        $themygrade = C;
        $studentcumulativemark = $graderemark3;
    }
    elseif (($thedwmark >= 50) AND ($thedwmark <= 59.99)) {
        $themygrade = D;
        $studentcumulativemark = $graderemark4;
    }
    elseif (($thedwmark >= 40) AND ($thedwmark <= 49.99)) {
        $themygrade = E;
        $studentcumulativemark = $graderemark5;
    }
    elseif (($thedwmark >= 1) AND ($thedwmark <= 39.99)) {
        $themygrade = "<span style='color: red;'>F</span>";
        $studentcumulativemark = "<span style='color: red;'>$graderemark6</span>";
    }
    else {
        $themygrade = "<span style='color: red;'>ABSENT</span>";
        $studentcumulativemark = "<span style='color: red;'>$graderemark7</span>";
    }

    return $studentcumulativemark;

}

function CreatTP($term)
{
    global $admission_no, $yearsession, $class_level, $class_no, $class_letter;
    $thisclass = "$class_no";
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE s_term = '$term' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	testmark1 != '' AND
	testmark2 != '' AND
	finalexam != '' AND
	subject != ''";
    $result1 = mysql_query($query1);
    $num1 = mysql_num_rows($result1);
    for ($i = 1; $i <= $num1; $i ++) {
        $row1 = mysql_fetch_array($result1);
        $atestmark1 = $row1["testmark1"];
        $atestmark2 = $row1["testmark2"];
        $atestmark3 = $row1["testmark3"];
        $atestmark4 = $row1["testmark4"];
        $atestmark5 = $row1["testmark5"];
        $atestmark6 = $row1["testmark6"];
        $aothertest = $row1["othertest"];
        $afinalexam = $row1["finalexam"];
        $s_term = $row1["s_term"];
        $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
        $overrall = Overall($atestmark1, $atestmark2);
        $mytotal = $mytotal / $overrall * 100;
        $stotal = $mytotal;
        $s_id = $row1["s_id"];
        $myad = $row1["ss_admission"];
        $value1 = $stotal;
        $avg = $stotal;
        $create_temp = "CREATE TEMPORARY TABLE IF NOT EXISTS tpercentmc (
id int(5) NOT NULL auto_increment,
o_id int(5) NOT NULL,
tadmission VARCHAR(255),
tterm VARCHAR(255),
tmark2 DECIMAL(6,2),
PRIMARY KEY  (id),
UNIQUE  (o_id)
)";
        $results_c = mysql_query($create_temp) or die(mysql_error());
        $value1 = $value1;
        $avg = $avg;
        $insert = "INSERT INTO tpercentmc (id, o_id, tadmission, tterm, tmark2)
	VALUES ('$id', '$s_id', '$myad', '$s_term', '$value1')";
        $results_i = mysql_query($insert);

    }


}

function CreatTPSpecial($term, $admission_no)
{
    global $admission_no, $term, $yearsession, $class_level, $class_no, $class_letter;
    $thisclass = "$class_no";
    if ($term == firstterm) {
        $query1 = "SELECT AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";

    }
    elseif ($term == secondterm) {
        $query1 = "SELECT AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
    }
    else {
        $query1 = "SELECT AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'thirdterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'thirdterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'thirdterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass' AND
	ss_admission = '$admission_no'";
    }
    $result1 = mysql_query($query1);
    $result2 = mysql_query($query2);
    $result3 = mysql_query($query3);
    $row1 = mysql_fetch_array($result1);
    $row2 = mysql_fetch_array($result2);
    $row3 = mysql_fetch_array($result3);
    $avg1 = $row1["avg1"];
    $avg2 = $row2["avg2"];
    $avg3 = $row3["avg3"];
    $overall = Overall($avg1, $avg2);
    $stotal = $avg1 + $avg2 + $avg3;
    $stotal = $stotal / $overall * 100;

    return number_format($stotal, 2);

}

function CreatTPSpecialHigest($term)
{
    global $admission_no, $term, $yearsession, $class_level, $class_no, $class_letter;
    $thisclass = "$class_no";
    if ($term == firstterm) {
        $query1 = "SELECT ss_admission AS myad,AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";

    }
    elseif ($term == secondterm) {
        $query1 = "SELECT ss_admission AS myad,AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
    }
    else {
        $query1 = "SELECT ss_admission AS myad,AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'thirdterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'thirdterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'thirdterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY ss_admission";
    }
    $result1 = mysql_query($query1);
    $result2 = mysql_query($query2);
    $result3 = mysql_query($query3);
    $num3 = mysql_num_rows($result3);
    for ($i = 0; $i < $num3; $i ++) {
        $row1 = mysql_fetch_array($result1);
        $row2 = mysql_fetch_array($result2);
        $row3 = mysql_fetch_array($result3);
        $avg1 = $row1["avg1"];
        $avg2 = $row2["avg2"];
        $avg3 = $row3["avg3"];
        $myad = $row1["myad"];
        $overall = Overall($avg1, $avg2);
        $stotal = $avg1 + $avg2 + $avg3;
        $stotal = $stotal / $overall * 100;
        $create_temp = "CREATE TEMPORARY TABLE IF NOT EXISTS tpercenthigh (
id int(5) NOT NULL auto_increment,
tadmission VARCHAR(200),
tmark DECIMAL(6,3),
PRIMARY KEY  (id)
)";
        $results_c = mysql_query($create_temp) or die(mysql_error());
        $value1 = $stotal;
        //$avg = number_format ($avg,2);
        $insert = "INSERT INTO tpercenthigh (id, tadmission, tmark)
	VALUES ('$id', '$myad', '$value1')";
        $results_i = mysql_query($insert) or die(mysql_error());

    }

}

function CreatTPSubject($term)
{
    global $admission_no, $term, $yearsession, $class_level, $class_no, $class_letter;
    $thisclass = "$class_no";
    if ($term == firstterm) {
        $query2 = "SELECT  class,subject,s_id AS s_id, ss_admission AS myad,AVG(testmark1+testmark2+testmark3+testmark4+testmark5+testmark6+finalexam) AS avg
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession'
	GROUP BY s_id";
    }
    elseif ($term == secondterm) {
        $query2 = "SELECT  class,subject,s_id AS s_id, ss_admission AS myad,AVG(testmark1+testmark2+testmark3+testmark4+testmark5+testmark6+finalexam) AS avg
	FROM studentsubjects
	WHERE (s_term = 'firstterm' OR s_term = 'secondterm') AND
	s_session = '$yearsession'
	GROUP BY s_id";
    }
    else {
        $query2 = "SELECT  class,subject,s_id AS s_id, ss_admission AS myad,AVG(testmark1+testmark2+testmark3+testmark4+testmark5+testmark6+finalexam) AS avg
	FROM studentsubjects
	WHERE  (s_term = 'firstterm' OR s_term = 'secondterm' OR s_term = 'thirdterm') AND
	s_session = '$yearsession'
	GROUP BY s_id";
    }

}

function CreatTPSubjectTermBase($term)
{
    global $admission_no, $term, $yearsession, $class_level, $class_no, $class_letter;
    $thisclass = "$class_no";
    if ($term == firstterm) {
        $query1 = "SELECT subject,class,s_id AS s_id, ss_admission AS myad,AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'firstterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";

    }
    elseif ($term == secondterm) {
        $query1 = "SELECT subject,class,s_id AS s_id, ss_admission AS myad,AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_term = 'secondterm' AND
	s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
    }
    else {
        /*
        $query1 = "SELECT subject,class,s_id AS s_id, ss_admission AS myad,AVG(testmark1) AS avg1
            FROM studentsubjects
            WHERE s_term = 'thirdterm' AND
            s_session = '$yearsession' AND
            class = '$thisclass'
            GROUP BY s_id";

        $query2 = "SELECT AVG(testmark2) AS avg2
            FROM studentsubjects
            WHERE s_term = 'thirdterm' AND
            s_session = '$yearsession' AND
            class = '$thisclass'
            GROUP BY s_id";

        $query3 = "SELECT AVG(finalexam) AS avg3
            FROM studentsubjects
            WHERE s_term = 'thirdterm' AND
            s_session = '$yearsession' AND
            class = '$thisclass'
            GROUP BY s_id";
        */
        //for thirdterm, remove term condition
        $query1 = "SELECT subject,class,s_term,s_id AS s_id, ss_admission AS myad,AVG(testmark1) AS avg1
	FROM studentsubjects
	WHERE s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
        $query2 = "SELECT AVG(testmark2) AS avg2
	FROM studentsubjects
	WHERE s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
        $query3 = "SELECT AVG(finalexam) AS avg3
	FROM studentsubjects
	WHERE s_session = '$yearsession' AND
	class = '$thisclass'
	GROUP BY s_id";
    }
    $result1 = mysql_query($query1) or die(mysql_error());
    $result2 = mysql_query($query2) or die(mysql_error());
    $result3 = mysql_query($query3) or die(mysql_error());
    $num3 = mysql_num_rows($result3);
    $create_temp = "CREATE TABLE IF NOT EXISTS tpercentsubjectterm (
id int(11) NOT NULL auto_increment,
o_id int(11) NOT NULL,
tadmission VARCHAR(200),
tclass VARCHAR(255),
tsubject VARCHAR(255),
tterm VARCHAR(255),
tmark2 DECIMAL(6,3),
PRIMARY KEY  (id),
UNIQUE  o_id (o_id)
)";
    $results_c = mysql_query($create_temp) or die(mysql_error());
    for ($i = 0; $i < $num3; $i ++) {
        $row1 = mysql_fetch_array($result1);
        $row2 = mysql_fetch_array($result2);
        $row3 = mysql_fetch_array($result3);
        $avg1 = $row1["avg1"];
        $avg2 = $row2["avg2"];
        $avg3 = $row3["avg3"];
        $myad = $row1["myad"];
        $s_id = $row1["s_id"];
        $subject = $row1["subject"];
        $s_term = $row1["s_term"];
        //$dclass = $row1["class"];
        $dclass = $thisclass;
        $overall = Overall($avg1, $avg2);
        $stotal = $avg1 + $avg2 + $avg3;
        $stotal = $stotal / $overall * 100;
        $insert = "INSERT INTO tpercentsubjectterm (id, o_id, tadmission, tclass, tsubject, tterm, tmark2)
	VALUES ('$id', '$s_id', '$myad', '$dclass', '$subject', '$s_term', '$stotal')";
        $results_i = mysql_query($insert);

    }


}

function GetStudentP($term, $admission_no)
{
    CreatTP($term);
    $query3 = "SELECT AVG(tmark2) AS avg
	FROM tpercentmc
	WHERE tadmission = '$admission_no' AND
	tterm = '$term'";
    $result3 = mysql_query($query3);
    if ($result3) {
        $row3 = mysql_fetch_array($result3);
        $avg = $row3["avg"];

        return number_format($avg, 2);
    }
    else return number_format(0, 2);
}

function CreatTPPosition($term)
{
    global $admission_no, $term, $yearsession, $class_level, $class_no, $class_letter;
    $thisclass = "$class_no";
    $create_temp = "CREATE TEMPORARY TABLE IF NOT EXISTS tpercentposition (
id int(11) NOT NULL auto_increment,
tadmission VARCHAR(200),
tclass VARCHAR(255),
tterm VARCHAR(255),
tmark2 DECIMAL(6,2),
PRIMARY KEY  (id),
UNIQUE  (tadmission)
)";
    $results_c = mysql_query($create_temp) or die(mysql_error());
    $queryjj = "SELECT DISTINCT ss_admission,class,s_term
	FROM  studentsubjects
	WHERE s_term = '$term' AND
	s_session = '$yearsession' AND
	class = '$thisclass'";
    $resultjj = mysql_query($queryjj) or die(mysql_error());
    $num = mysql_num_rows($resultjj);
    for ($i = 1; $i <= $num; $i ++) {
        $sbrow = mysql_fetch_array($resultjj);
        $ss_admission = $sbrow["ss_admission"];
        $class = $sbrow["class"];
        $s_term = $sbrow["s_term"];
        $stotal = GetStudentP($term, $ss_admission);
        $myad = $ss_admission;
        $dclass = $class;
        $insert = "INSERT INTO tpercentposition (id, tadmission, tclass, tterm, tmark2)
	VALUES ('$id', '$myad', '$dclass', '$s_term', '$stotal')";
        $results_i = mysql_query($insert);

    }


}

function GetHighestP($term)
{
    CreatTPPosition($term);
    global $class_no;
    $query3 = "SELECT MAX(tmark2) AS max
	FROM tpercentposition
	WHERE tclass = '$class_no'";
    $result3 = mysql_query($query3);
    $row3 = mysql_fetch_array($result3);
    $max = $row3["max"];

    return number_format($max, 2);

}

function GetPositionTest($term)
{
    echo " ";
}

function GetPosition2($term)
{
    $query3 = "SELECT AVG(tmark2) AS tmark2,tadmission
	FROM getnyearinsertall
	WHERE tclass = '$class_no' AND
	tterm = '$term' AND
	tsession = '$yearsession'
	GROUP BY tadmission
	ORDER by tmark2 DESC";
    $result3 = mysql_query($query3);
    $num = mysql_num_rows($result3);
    echo "$num";
}

function GetPosition($term)
{
    global $admission_no, $class_no, $yearsession;
    if ($term == "thirdterm") {
        $query3 = "SELECT DISTINCT tadmission
	FROM getnyearinsertposition
	WHERE tclass = '$class_no' AND
	tterm = '$term' AND
	tsession = '$yearsession'
	ORDER by tmark2 DESC";
        $result3 = mysql_query($query3);
        $num = mysql_num_rows($result3);
        for ($i = 1; $i <= $num; $i ++) {
            $row3 = mysql_fetch_array($result3);
            $user_adm = $row3['tadmission'];
            if ($user_adm == "$admission_no") {
                $rank = $i;
            }
        }
        $mrank = substr($rank, - 1, 1);
        $mmrank = substr($rank, - 2, 2);
        if ($mrank == 1) {
            if ($mmrank == 11) {
                $nth = "th";
                $therank = "$rank$nth";
            }
            else {
                $nth = "st";
                $therank = "$rank$nth";
            }

        }
        elseif ($mrank == 2) {
            if ($mmrank == 12) {
                $nth = "th";
                $therank = "$rank$nth";
            }
            else {
                $nth = "nd";
                $therank = "$rank$nth";
            }

        }
        elseif ($mrank == 3) {
            if ($mmrank == 13) {
                $nth = "th";
                $therank = "$rank$nth";
            }
            else {
                $nth = "rd";
                $therank = "$rank$nth";
            }

        }
        else {
            $nth = "th";
            $therank = "$rank$nth";
        }

        return $therank;


    }
    else {
        CreatTPPosition($term);
        $query3 = "SELECT DISTINCT tadmission
	FROM tpercentposition
	WHERE tclass = '$class_no' AND
	tterm = '$term'
	ORDER by tmark2 DESC";
        $result3 = mysql_query($query3);
        $num = mysql_num_rows($result3);
        for ($i = 1; $i <= $num; $i ++) {
            $row3 = mysql_fetch_array($result3);
            $user_adm = $row3['tadmission'];
            if ($user_adm == "$admission_no") {
                $rank = $i;
            }
        }
        $mrank = substr($rank, - 1, 1);
        $mmrank = substr($rank, - 2, 2);
        if ($mrank == 1) {
            if ($mmrank == 11) {
                $nth = "th";
                $therank = "$rank$nth";
            }
            else {
                $nth = "st";
                $therank = "$rank$nth";
            }

        }
        elseif ($mrank == 2) {
            if ($mmrank == 12) {
                $nth = "th";
                $therank = "$rank$nth";
            }
            else {
                $nth = "nd";
                $therank = "$rank$nth";
            }

        }
        elseif ($mrank == 3) {
            if ($mmrank == 13) {
                $nth = "th";
                $therank = "$rank$nth";
            }
            else {
                $nth = "rd";
                $therank = "$rank$nth";
            }

        }
        else {
            $nth = "th";
            $therank = "$rank$nth";
        }

        return $therank;

    }

}

function GetPositionSubject($term, $subject, $class)
{
    CreatTPsubject($term);
    global $admission_no;
    $query3 = "SELECT DISTINCT tadmission
	FROM tpercentsubject
	WHERE tsubject = '$subject' AND
	tclass = '$class'
	ORDER by tmark2 DESC";
    $result3 = mysql_query($query3) or die(mysql_error());
    $num = mysql_num_rows($result3);
    for ($i = 1; $i <= $num; $i ++) {
        $row3 = mysql_fetch_array($result3);
        $user_adm = $row3['tadmission'];
        if ($user_adm == "$admission_no") {
            $rank = $i;
        }

    }
    $mrank = substr($rank, - 1, 1);
    $mmrank = substr($rank, - 2, 2);
    if ($mrank == 1) {
        if ($mmrank == 11) {
            $nth = "th";
            $therank = "$rank$nth";
        }
        else {
            $nth = "st";
            $therank = "$rank$nth";
        }

    }
    elseif ($mrank == 2) {
        if ($mmrank == 12) {
            $nth = "th";
            $therank = "$rank$nth";
        }
        else {
            $nth = "nd";
            $therank = "$rank$nth";
        }

    }
    elseif ($mrank == 3) {
        if ($mmrank == 13) {
            $nth = "th";
            $therank = "$rank$nth";
        }
        else {
            $nth = "rd";
            $therank = "$rank$nth";
        }

    }
    else {
        $nth = "th";
        $therank = "$rank$nth";
    }

    return "$therank of $num";

}

function GetPositionSubjectTerm($term, $subject, $class)
{
    CreatTPSubjectTermBase($term);
    global $admission_no;
    $query3 = "SELECT DISTINCT tadmission
	FROM tpercentsubjectterm
	WHERE tsubject = '$subject' AND
	tclass = '$class' AND
	tterm = '$term'
	ORDER by tmark2 DESC";
    $result3 = mysql_query($query3) or die(mysql_error());
    $num = mysql_num_rows($result3);
    if ($num == 0) {
        $update = "UPDATE tpercentsubjectterm SET tterm='$term'
		WHERE tsubject='$subject' AND
		tclass = '$class'";
        $results3 = mysql_query($update);
    }
    for ($i = 1; $i <= $num; $i ++) {
        $row3 = mysql_fetch_array($result3);
        $user_adm = $row3['tadmission'];
        if ($user_adm == "$admission_no") {
            $rank = $i;
        }

    }
    $mrank = substr($rank, - 1, 1);
    $mmrank = substr($rank, - 2, 2);
    if ($mrank == 1) {
        if ($mmrank == 11) {
            $nth = "th";
            $therank = "$rank$nth";
        }
        else {
            $nth = "st";
            $therank = "$rank$nth";
        }

    }
    elseif ($mrank == 2) {
        if ($mmrank == 12) {
            $nth = "th";
            $therank = "$rank$nth";
        }
        else {
            $nth = "nd";
            $therank = "$rank$nth";
        }

    }
    elseif ($mrank == 3) {
        if ($mmrank == 13) {
            $nth = "th";
            $therank = "$rank$nth";
        }
        else {
            $nth = "rd";
            $therank = "$rank$nth";
        }

    }
    else {
        $nth = "th";
        $therank = "$rank$nth";
    }

    return "$therank of $num";

}

function GetClassAvg($term)
{
    CreatTPPosition($term);
    global $admission_no, $class_no;
    $query3 = "SELECT AVG(tmark2) AS classavg
	FROM tpercentposition
	WHERE tclass = '$class_no'";
    $result3 = mysql_query($query3);
    $row3 = mysql_fetch_array($result3);
    $classavg = $row3["classavg"];
    $classavg = number_format($classavg, 2);

    return $classavg;

}

function GetClassAvgPerSubject($session, $term, $class_no, $subject)
{
    //die ("$session, $term, $class_level, $class_no, $class_letter");
    //Determine the terms to use depending on the current term.
    $termLimit = "";
    switch ($term) {
        case "firstterm":
            $termLimit = "'firstterm'";
            break;
        case "secondterm":
            $termLimit = "'firstterm', 'secondterm'";
            break;
        default:
            $termLimit = "'firstterm', 'secondterm', 'thirdterm'";
            break;
    }//END switch
    $query = "SELECT ROUND(
                        AVG(testmark1 + testmark2 + testmark3 + testmark4 + finalexam), 2
                     ) AS 'classAverage'
                FROM studentsubjects
                WHERE s_term IN (" . $termLimit . ")				#Term
                    AND s_session = '" . $session . "'				#Session
                    AND `class` = '" . $class_no . "'				#Class
                    AND subject = '" . $subject . "'				#Subject
                    AND testmark1 != '' AND testmark1 != '0'		#Scores (Here Downwards)
                    AND testmark2 != '' AND testmark2 != '0'
                    AND testmark3 != '' AND testmark3 != '0'
                    #AND testmark4 != '' AND testmark4 != '0'
                    AND finalexam != '' AND finalexam != '0'";
    $result = mysql_query($query);
    if (mysql_affected_rows() == 1) {
        $retVal = mysql_fetch_array($result, MYSQL_ASSOC);
        $retVal = $retVal["classAverage"];
    }
    else $retVal = "0.00";

    return $retVal;
}   //END GetClassAvg_new()
function GetR($subject)
{
    global $admission_no, $term, $yearsession;
    $query1 = "SELECT *
	FROM studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_term = '$term' AND
	s_session = '$yearsession' AND
	subject = '$subject'";
    $result1 = mysql_query($query1);
    $arow = mysql_fetch_array($result1);
    $ctremark = $arow["ctremark"];
    if (($ctremark == F) OR ($ctremark == "F9")) {
        $ctremark = "<span style='color: red'>$ctremark</span>";
    }
    else {
        $ctremark = $ctremark;
    }

    return $ctremark;

}

function GetNoInClass($class)
{
    global $admission_no, $term, $yearsession;
    $query1 = "SELECT DISTINCT ss_admission
	FROM studentsubjects
	WHERE s_term = '$term' AND
	s_session = '$yearsession' AND
	class = '$class'";
    $result1 = mysql_query($query1);
    $num = mysql_num_rows($result1);

    return $num;
}

function GetNoFailed()
{
    global $admission_no, $term, $yearsession;
    $tsb = GetWeigthedAvg($subject);
    $queryjj = "SELECT *
	FROM  studentsubjects
	WHERE ss_admission LIKE '%$admission_no%' AND
	s_term = '$term' AND
	s_session = '$yearsession'";
    $resultjj = mysql_query($queryjj) or die(mysql_error());
    $num = mysql_num_rows($resultjj);

    return $num;
}

function GetTotalSubjectAverageAll()
{
    $drop_temp2 = "DROP TABLE IF EXISTS totalpercentperterm";
    $results_d2 = mysql_query($drop_temp2) or die(mysql_error());
    $create_temp2 = "CREATE TABLE totalpercentperterm (
id int(5) NOT NULL auto_increment,
tadmission VARCHAR(255),
tmark2 VARCHAR(50),
tsession YEAR,
tterm VARCHAR(255),
tsubject VARCHAR(255),
tclass INT(11),
PRIMARY KEY  (id)
)";
    $results_c2 = mysql_query($create_temp2) or die(mysql_error());
    echo "table created...<br>";
    $query11 = "SELECT DISTINCT subject, studentsubjects.*
	FROM studentsubjects
	WHERE ss_admission != ' ' OR
	s_session != ' ' OR
	s_term != ' ' OR
	subject != ' ' OR
	(testmark1 != ' ' AND
	testmark2 != ' ' AND
	finalexam != ' ')";
    $result11 = mysql_query($query11);
    $num11 = mysql_num_rows($result11);
    for ($i = 1; $i <= $num11; $i ++) {
        $abrow = mysql_fetch_array($result11);
        $s_id = $abrow["s_id"];
        $query1 = "SELECT *
	FROM studentsubjects
	WHERE s_id = '$s_id'";
        $result1 = mysql_query($query1);
        $arow = mysql_fetch_array($result1);
        $atestmark1 = $arow["testmark1"];
        $atestmark2 = $arow["testmark2"];
        $atestmark3 = $arow["testmark3"];
        $atestmark4 = $arow["testmark4"];
        $atestmark5 = $arow["testmark5"];
        $atestmark6 = $arow["testmark6"];
        $aothertest = $arow["othertest"];
        $afinalexam = $arow["finalexam"];
        $mytotal = $atestmark1 + $atestmark2 + $atestmark3 + $atestmark4 + $atestmark5 + $atestmark6 + $afinalexam;
        $overrall = Overall($atestmark1, $atestmark2);
        $mytotal = $mytotal / $overrall * 100;
        $mytotal1 = Absent2($mytotal);
        $admission_no = $arow["ss_admission"];
        $subject = $arow["subject"];
        $s_term = $arow["s_term"];
        $s_session = $arow["s_session"];
        $class = $arow["class"];
        $insert1 = "INSERT INTO totalpercentperterm (id, tadmission, tmark2, tsession, tterm, tsubject, tclass)
	VALUES ('$id', '$admission_no', '$mytotal1', '$s_session', '$s_term', '$subject', '$class')";
        $results_i1 = mysql_query($insert1);

    }
    $drop_temp2 = "DROP TABLE IF EXISTS getnyearinsertposition";
    $results_d2 = mysql_query($drop_temp2) or die(mysql_error());
    $create_temp2 = "CREATE TABLE getnyearinsertposition (
id int(5) NOT NULL auto_increment,
tadmission VARCHAR(255),
tmark2 DECIMAL(6,3),
tsession YEAR,
tterm VARCHAR(255),
tclass INT(11),
PRIMARY KEY  (id)
)";
    $results_c2 = mysql_query($create_temp2) or die(mysql_error());
    echo "table created...<br>";
    $drop_temp3 = "DROP TABLE IF EXISTS thirdtermposition";
    $results_d3 = mysql_query($drop_temp3) or die(mysql_error());
    $create_temp3 = "CREATE TABLE thirdtermposition (
id int(5) NOT NULL auto_increment,
tadmission VARCHAR(255),
tmark2 DECIMAL(6,3),
tsession YEAR,
tterm VARCHAR(255),
tclass INT(11),
PRIMARY KEY  (id)
)";
    $results_c3 = mysql_query($create_temp3) or die(mysql_error());
    echo "table created...<br>";
    $query33a = "SELECT DISTINCT tadmission
	FROM totalpercentperterm";
    $result33a = mysql_query($query33a) or die(mysql_error());
    $num2a = mysql_num_rows($result33a);
    for ($a = 1; $a <= $num2a; $a ++) {
        $rowd = mysql_fetch_array($result33a);
        $tadmission = $rowd["tadmission"];
        $dterm = thirdterm;
        $dsession = 2007;
        $queryjj = "SELECT *
	FROM  studentsubjects
	WHERE ss_admission = '$tadmission' AND
	s_term = '$dterm' AND
	s_session = '$dsession' AND
	(testmark1 != '' OR
	testmark2 != '') AND
	finalexam != '' AND
	subject != ''
	ORDER BY subject";
        $resultjj = mysql_query($queryjj);
        $num = mysql_num_rows($resultjj);
        for ($b = 1; $b <= $num; $b ++) {
            $sbrow = mysql_fetch_array($resultjj);
            $s_id = $sbrow["s_id"];
            $ss_admission = $sbrow["ss_admission"];
            $subject = $sbrow["subject"];
            $testmark1 = $sbrow["testmark1"];
            $testmark2 = $sbrow["testmark2"];
            $testmark3 = $sbrow["testmark3"];
            $othertest = $sbrow["othertest"];
            $finalexam = $sbrow["finalexam"];
            $s_term = $sbrow["s_term"];
            $s_session = $sbrow["s_session"];
            $ctremark = $sbrow["ctremark"];
            $tclass = $sbrow["class"];
            $s_id = stripslashes($s_id);
            $tadmission = stripslashes($ss_admission);
            $dsubject = stripslashes($subject);
            $testmark1 = stripslashes($testmark1);
            $testmark2 = stripslashes($testmark2);
            $testmark3 = stripslashes($testmark3);
            $othertest = stripslashes($othertest);
            $finalexam = stripslashes($finalexam);
            $s_term = stripslashes($s_term);
            $tsession = stripslashes($s_session);
            $dmark = GetNYearSpecial($dsubject, $tadmission, $dterm, $dsession);
            $insert2 = "INSERT INTO thirdtermposition (id, tadmission, tmark2, tsession, tterm, tclass)
	VALUES ('$id', '$tadmission', '$dmark', '$dsession', '$dterm', '$tclass')";
            $results_i2 = mysql_query($insert2) or die(mysql_error());
        }

    }
    $query33 = "SELECT AVG(tmark2) AS tmark, tadmission, tsession, tterm, tclass
	FROM thirdtermposition
	GROUP BY tadmission";
    $result33 = mysql_query($query33) or die(mysql_error());
    $numt2 = mysql_num_rows($result33);
    for ($a = 1; $a <= $numt2; $a ++) {
        $rowd = mysql_fetch_array($result33a);
        $tadmission = $rowd["tadmission"];
        $row = mysql_fetch_array($result33);
        $tadmission = $row["tadmission"];
        $tterm = $row["tterm"];
        $tsession = $row["tsession"];
        $tclass = $row["tclass"];
        $tmark = $row["tmark"];
        $dterm = thirdterm;
        $insert2 = "INSERT INTO getnyearinsertposition (id, tadmission, tmark2, tsession, tterm, tclass)
	VALUES ('$id', '$tadmission', '$tmark', '$tsession', '$tterm', '$tclass')";
        $results_i2 = mysql_query($insert2) or die(mysql_error());

    }
    echo "...ALL EXECTED";
}

function GetAvgAge($class_no, $class_level, $class_letter, $session, $term)
{
    $query = "SELECT *
	FROM studentprofile_extra,studentprofile
	WHERE studentprofile_extra.yearsession = '$session' AND
	studentprofile_extra.term = '$term' AND
	studentprofile_extra.class_no = '$class_no' AND
	studentprofile_extra.class_level = '$class_level' AND
	studentprofile_extra.class_letter = '$class_letter' AND
	studentprofile_extra.studentprofile_id = studentprofile.studentprofile_id";
    $result = mysql_query($query) or die(mysql_error());
    if ($result) {
        $num = mysql_num_rows($result);
        if ($num > 0) {
            $ages = 0;
            while ($row = mysql_fetch_array($result)) {
                $birthday = $row["birthday"];
                $birthmonth = $row["birthmonth"];
                $birthyear = $row["birthyear"];
                if ($birthyear != '' || $birthyear != 0) {
                    $age = date("Y") - $birthyear;
                    if ($age != 0 && $birthmonth > date("n"))
                        $age = $age - 1;
                }
                $ages = $ages + $age;

            }
            $avg_age = number_format($ages / $num, 0);
            if ($avg_age == 1)
                $avg_age = $avg_age . "yr";
            else $avg_age = $avg_age . "yrs";

            return $avg_age;
        }
    }
}

function get_grade($score, $max_score) {
    $score = ceil($score);
    $sql = "SELECT g.* FROM grades g
            JOIN grade_system gs ON g.grade_id = gs.grade_id AND gs.max_score = {$max_score} AND gs.lower_limit <= {$score} AND {$score} <= gs.upper_limit";
    $resource = mysql_query($sql);
    if (mysql_num_rows($resource) > 0)
        return mysql_fetch_assoc($resource);
    else
        return FALSE;
}
?>