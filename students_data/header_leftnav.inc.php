<?php
include_once("auth.inc.php");
include_once("config.php");
include_once("lang.php");
include_once("fxn_pay2.php");

//die("<pre>" . print_r($_SESSION, 1));
if (empty($stdpagename))
    $stdpagename = "Student's Area";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <title><?php echo $bk_website_name ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta name="Keywords"
          content="Link School,nigerian schools,schools in nigeria,colleges in nigeria,language school,home school,high school,school, public schools,school closings,business school,law school,schools alumnus,test your skills,school supply, private schools, diploma, elementary schools,boarding school,school uniform,online schools,high school football,high school sports,technical schools,christian school,computer school,world class education,online college degree,college,college and university,online college and university,online college and university,college textbook,college book,college essay,christian college,education career education,continuing education, education online, distance education, adult education,continuing education online,department of education,higher education,distance learning,distance learning college,teaching jobs ,teaching abroad,teaching career,teaching math,teaching art,teaching english,teaching high school,teaching foreign language,chemistry, teaching physics,teaching science,teaching college,teacher,teacher lesson plan,learn english,english dictionary,curriculum,home school curriculum,preschool curriculum,curriculum development,sample curriculum vitae,home schooling curriculum,education curriculum">
    <meta name="Description"
          content="Link School is at the top of the top schools in Nigeria, with a mission to deliver world class education to children.">
    <link href="../styles/style2.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        p, td {
            font-family: Tahoma, Verdana, Arial;
            font-size: 11px;
        }
    </style>
    <script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="../js/angular.min.js"></script>
    <script type="text/javascript" src="../js/app.js"></script>
</head>
<body ng-app="App">
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
    <tr>
        <td>
            <img src="../lpsLogo.png" width="950" height="200">
            <table width="950" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
                <tr>
                    <td width="200" valign="top" class="borderright"><?php require("inc/links.php") ?><br/></td>