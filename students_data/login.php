<?php
include_once "config.php";
include_once "../admin/functions.php";

$redirectPage = !empty($_POST['redirect']) && !stristr($_POST['redirect'], "login.php") ? $_POST['redirect'] : "index.php";
if ($redirectPage == "index.php")
    $redirectPage = !empty($_GET['redirect']) && !stristr($_GET['redirect'], "login.php") ? $_GET['redirect'] : "index.php";

$msg = "You must be logged in to view this page";
$redirect = isset($_GET['redirect']) ? $_GET['redirect'] : "index.php";

if ((isset($_POST['submit'])) || (isset($_GET['submit']))) {
    //check if card info exists
    if (isset($_GET['admission_no'])) {
        $admission_no = $_GET['admission_no'];
        $password = $_GET['password'];
    } elseif (isset($_POST['admission_no'])) {
        $admission_no = $_POST['admission_no'];
        $password = $_POST['password'];
    }

    $sql = "SELECT * FROM studentprofile WHERE admission_no = '{$admission_no}' AND password = '{$password}'";

    if ($resource = mysql_query($sql, $connect)) {
        $studentprofile = mysql_fetch_assoc($resource);

        if ($studentprofile['enabled'] == "1") {
            session_start();

            $_SESSION['current_session_term'] = GetCurrentSessionTerm();

            extract($studentprofile);
            $name = "$lastname, $firstname $othernames";
            $_SESSION['logged'] = 4;
            $_SESSION['student_id'] = $id;
            $_SESSION['myuserid'] = $admission_no;
            $_SESSION['myname'] = $name;
            $_SESSION['myfullname'] = $name;
            $_SESSION['admission_no'] = $admission_no;
            $_SESSION['lastname'] = $lastname;
            $_SESSION['firstname'] = $firstname;
            $_SESSION['othernames'] = $othernames;
            $_SESSION['nationality'] = $nationality;

            $rd = (@$_POST['redirect']) ? @$_POST['redirect'] : @$_GET['redirect'];
            header("Refresh: 2; URL=" . $rd . " ");
            echo "You are being redirected to your original page request<br>";
            echo "(If your browser doesn't support this, <a href='" . $rd . "''>click here</a>)";
            exit;
        } else
            $msg = "You have been blocked from accessing the portal.";
    } else
        $msg = "Invalid Admission No and/or Password";
    if (isset($_POST['redirect']))
        $redirect = $_POST['redirect'];
}

include_once("lang.php");
?>

<html>
<head>
    <title><?php print $bk_website_name ?>: Login</title>
    <meta name="Keywords"
          content="Link School,nigerian schools,schools in nigeria,colleges in nigeria,language school,home school,high school,school, public schools,school closings,business school,law school,schools alumnus,test your skills,school supply, private schools, diploma, elementary schools,boarding school,school uniform,online schools,high school football,high school sports,technical schools,christian school,computer school,world class education,online college degree,college,college and university,online college and university,online college and university,college textbook,college book,college essay,christian college,education career education,continuing education, education online, distance education, adult education,continuing education online,department of education,higher education,distance learning,distance learning college,teaching jobs ,teaching abroad,teaching career,teaching math,teaching art,teaching english,teaching high school,teaching foreign language,chemistry, teaching physics,teaching science,teaching college,teacher,teacher lesson plan,learn english,english dictionary,curriculum,home school curriculum,preschool curriculum,curriculum development,sample curriculum vitae,home schooling curriculum,education curriculum">
    <meta name="Description"
          content="Link School is at the top of the top schools in Nigeria, with a mission to deliver world class education to children.">
    <link rel="stylesheet" href="../styles/style.css">
</head>

<body>
<table width="950" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
    <tr>
        <td>
            <img src="../lpsLogo.png" width="950" height="200">
            <table width="950" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
                <tr>
                    <td width="200" valign="top" class="borderright" id="leftnavbg">&nbsp;</td>
                    <td valign="top" style="padding: 5px">
                        <table width="100%" border="0" cellspacing="0" cellpadding="5">
                            <tr>
                                <td class="bgtopic"><?php echo $bk_website_name ?></td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="text">
                            <tr>
                                <td>
                                    <!-- content starts here -->
                                    <?php echo !empty($msg) ? $msg : ""; ?>
                                    <br/><br/>

                                    <form action="login.php" method="post" autocomplete=off>
                                        <p><input type="hidden" name="redirect" value="<?php echo $redirect; ?>"></p>
                                        <table width="400" border="0" cellspacing="0" cellpadding="5" class="text">
                                            <tr>
                                                <td>Registration No</td>
                                                <td><input name="admission_no" type="text" id="admission_no"></td>
                                            </tr>
                                            <tr>
                                                <td>Password</td>
                                                <td><input name="password" type="password" id="password"></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><input type="submit" name="submit" value="Login"></td>
                                            </tr>
                                        </table>

                                        <p align="center"><a href="../index.php" style="color: #000000;">&lt;&lt; Back To
                                                Home</a></p>
                                    </form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="950" border="0" align="center" cellpadding="0" cellspacing="0" class="border">
                <tr>
                    <td><?php require("inc/footer.php") ?></td>
                </tr>
            </table>

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="spaceme">
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>