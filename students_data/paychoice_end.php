<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 10/26/2015
 * Time: 3:32 PM
 */
require("header_leftnav.inc.php");
include_once("config.php");
?>
<style>
    #btn-pay {
        border: 1px solid #EFEFEF;
        padding: 5px 10px;
        background: #EFEFEF;
    }
    #btn-pay:hover {
        color: red
    }
</style>
<td valign="top" style="padding: 5px">
    <h3 class="page-title">Make Payment</h3>
    <?php $trxnID = $_GET['trxnID']; ?>

    <p>Please proceed to make payment at any bank, ATM, or on Quickteller using the details below:</p>

    <p>
        Transaction ID: <?php echo $trxnID ?>
        <br><br>
        Quickteller: <a href="https://www.quickteller.com/LPSI" target="_blank" id="btn-pay">PAY NOW</a>
    </p>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
</body>
</html>
