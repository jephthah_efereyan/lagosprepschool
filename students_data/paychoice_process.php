<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 10/26/2015
 * Time: 3:24 PM
 */

session_start();
require "config.php";
include("../admin/functions.php");

$trxnID = $_GET['trxnID'];

require "../BranchCollect_Client.php";

$transaction = GetTransaction($trxnID);
$total_amount = $transaction['trans_amount'];
$transaction_charge = $transaction['trans_charge'] + $transaction['portal_charge'];
$school_fee = $transaction['trans_amount'] - $transaction_charge;

$admission_no = !empty($_SESSION['admission_no']) ? $_SESSION['admission_no'] : "123456789";
$surname = !empty($_SESSION['lastname']) ? $_SESSION['lastname'] : "";
$firstname = !empty($_SESSION['firstname']) ? $_SESSION['firstname'] : "";
$othernames = !empty($_SESSION['othernames']) ? $_SESSION['othernames'] : "";
$email = !empty($_SESSION['email']) ? $_SESSION['email'] : "info@upperlink.ng";

if ($total_amount > 0) {
    $MerchantID = "120";
    $MerchantCode = "LPS";
    $DEVID = "UPPER-DES34K56L";
    $TransactionID = $trxnID;
    $salt = "$DEVID|$MerchantID|$MerchantCode|$TransactionID";

    $MAC = hash('sha512', $salt);
    $BankID = '13';
    $ItemCode = 'LPS01';
    $ItemDescription = 'Lagos Preparatory School Ikoyi';

    $school_account_id = "735";
    $school_account_name = "LPS";
    $school_account_number = "0640775051";
    $school_share = $school_fee;

    $upperlink_account_id = "736";
    $upperlink_account_name = "Upperlink Limited";
    $upperlink_account_number = "0627813015";
    $upperlink_share = "800";

    $bank_account_id = "737";
    $bank_account_name = "FCMB Income Account";
    $bank_account_number = "FCMB000001";
    $bank_share = "700";

    $string = <<< XML
<?xml version="1.0"  encoding="UTF-8"?>
<BranchCollectRequest>
<MerchantDetails>
    <Merchant DevID="$DEVID" MerchantID="$MerchantID" MerchantCode="$MerchantCode" />
</MerchantDetails>
<TransactionDetails>
    <Transaction TransactionID="$TransactionID" MAC="$MAC" TotalAmount="$total_amount" CustomerID="$admission_no" CustomerSurname="$surname" CustomerFirstname="$firstname" CustomerOthernames="$othernames" CustomerEmail="$email" CustomerGSM="08080772936" UpdateURL="http://www.upltest.com/academy/BranchCollectUpdate_URL.php" UpdateURLThirdParty="http://www.upltest.com/academy/BranchCollectUpdate_URL_ThirdParty.php" />
</TransactionDetails>
<ItemDetails ItemCode="$ItemCode" ItemDescription="$ItemDescription" InstallmentID="1" Split="false" ExpiryDate="2293-12-12">
    <Item ItemName="All Fees" ItemAmount="$school_fee" />
    <Item ItemName="Transaction Charge" ItemAmount="$transaction_charge" />
</ItemDetails>
<CustomFieldDetails>
    <CustomField CustomFieldLabel="Home Address" CustomFieldValue="" />
    <CustomField CustomFieldLabel="Contact Address" CustomFieldValue="" />
    <CustomField CustomFieldLabel="State" CustomFieldValue="" />
    <CustomField CustomFieldLabel="Country" CustomFieldValue="" />
</CustomFieldDetails>
<CollectionBankDetails>
    <CollectionBank CBankID="$BankID" />
</CollectionBankDetails>
<BankAccounts>
<BankDetails BankID="$BankID">
    <AccountDetails AccountID="$school_account_id" AccountName="$school_account_name" AccountNo="$school_account_number" SplitAmount="$school_share" />
    <AccountDetails AccountID="$upperlink_account_id" AccountName="$upperlink_account_name" AccountNo="$upperlink_account_number" SplitAmount="$upperlink_share" />
    <AccountDetails AccountID="$bank_account_id" AccountName="$bank_account_name" AccountNo="$bank_account_number" SplitAmount="$bank_share" />
</BankDetails>
</BankAccounts>
</BranchCollectRequest>
XML;
    // echo "<pre>" . print_r($string, 1);
    // die();
    //Create client
    $client = new BranchCollect_Client();
    //send to Interswitch
    $result = $client->logPayment($string);
    if ($result->RespCode == "00" && !empty($result->TransactionID))
        header("Location: paychoice_end.php?trxnID=$trxnID");
    else
        die(print_r($result));
}
//header("Location:index.php");