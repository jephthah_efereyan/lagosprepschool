<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 5/18/2016
 * Time: 10:28 AM
 */

require("header_leftnav.inc.php");
?>
<link href="../js/dataTables/datatables.min.css" type="text/css" rel="stylesheet">
<link href="../js/dataTables/Buttons-1.1.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet">
<script src="../js/dataTables/datatables.min.js" type="text/javascript"></script>
<script src="../js/dataTables/Buttons-1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="../js/jquery-2.2.3.min.js"></script> -->
<script type="text/javascript" src="../js/jquery.validation/jquery.validate.min.js"></script>
<td valign="top" style="padding: 5px">
    <?php

    include_once '../autoload.php';
    $history = Transaction::studentPaymentHistory($connect, $_SESSION["student_id"]);
    if ($history) {
        ?>
        <table id="myTable" style="background-color:transparent">
            <thead>
            <tr>
                <th align="left">#</th>
                <th align="left">Date/Time</th>
                <th align="left">&#x20a6; Bill Paid</th>
                <th align="left">&#x20a6; Portal Access Fees</th>
                <!--<th align="left">&#x20a6; Charge</th>-->
                <th align="left">&#x20a6; Total</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($history as $i => $row): ?>
                <tr>
                    <td align="left"><?php print $i + 1 ?></td>
                    <td align="left"><?php print $row['trans_date'] ?></td>
                    <td align="left"><?php print number_format(($row['trans_amount'] - $row['portal_charge'] - $row['trans_charge']), 2) ?></td>
                    <td align="left">
					<?php print number_format($row['portal_charge'] + $row['trans_charge'], 2) ?>
                    </td>
                   <!-- <td align="left"><?php print number_format($row['trans_charge'], 2) ?></td>-->
                    <td align="left"><?php print number_format($row['trans_amount'], 2) ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    <?php
    } else {
        ?>
        <h4>No data available</h4>
    <?php
    }
    ?>
</td>
</tr>
</table>
<script>
    $(document).ready(function () {
        $('#myTable').dataTable({});
    });
</script>
<?php require("footer.php"); ?>
</body>
</html>

