<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 3/1/2016
 * Time: 10:27 AM
 */

require_once("auth.inc.php");
require_once("config.php");
require("fxn_pay.php");

$order_ID = isset($_GET['OrderID']) ? $_GET['OrderID'] : $_GET['trans_no'];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title>Payment Receipt: ACADEMY</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../styles/style2.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        <!--
        p, td {
            font-family: Tahoma, Verdana, Arial;
            font-size: 11px;
        }

        -->
    </style>
    <style type="text/css">
        <!--
        .style1 {
            font-family: "Trebuchet MS", Tahoma, Verdana;
            font-weight: bold;
            font-size: 18px;
            color: #000000;
        }

        .style2 {
            font-family: "Trebuchet MS", Tahoma, Verdana;
            font-size: 14px;
        }

        -->
    </style>

    <script>
        function printWindow() {
            bV = parseInt(navigator.appVersion)
            if (bV >= 4) window.print()
        }
    </script>
    <script language=javascript>
        <!--
        if (window.Event)
            document.captureEvents(Event.MOUSEUP);

        function nocontextmenu() {
            event.cancelBubble = true, event.returnValue = false;
            return false;
        }

        function norightclick(e) {
            if (window.Event) {
                if (e.which == 2 || e.which == 3) return false;
            }
            else if (event.button == 2 || event.button == 3) {
                event.cancelBubble = true, event.returnValue = false;
                return false;
            }
        }

        if (document.layers)
            document.captureEvents(Event.MOUSEDOWN);

        document.oncontextmenu = nocontextmenu;
        document.onmousedown = norightclick;
        document.onmouseup = norightclick;
        //-->
    </script>
</head>

<body>
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><br>
            <?php
            # Get the total amount paid
            $sql = "SELECT trans_amount FROM transactions WHERE trans_no = '$order_ID' AND trans_status = 'Paid'";
            $result = mysql_query($sql) or die(mysql_error());
            $total_amount = mysql_result($result, 0);

            $sql = "SELECT 
					transactions.*,
					DATE_FORMAT(transactions.trans_date, '%a %D %b %Y %r') AS trans_date, 
					vpc_transactions.vpc_receiptno AS reference_no ,
					studentprofile.admission_no,
					school_sessions.session_fullname,
					school_terms.term_fullname
					FROM transactions 
					LEFT JOIN vpc_transactions ON transactions.id = vpc_transactions.trans_id
					LEFT JOIN studentprofile ON studentprofile.id = transactions.student_id
					LEFT JOIN session_terms ON session_terms.session_term_id = transactions.session_term_id
					
					LEFT JOIN school_sessions ON school_sessions.session_id = session_terms.session_id
					LEFT JOIN school_terms ON school_terms.term_id = session_terms.term_id
					WHERE transactions.trans_no = '$order_ID' AND transactions.trans_status = 'Paid'";
            
            $result = mysql_query($sql) or die(mysql_error());
            if (mysql_num_rows($result) > 0) {
					include_once '../autoload.php';
					$transaction = mysql_fetch_assoc($result);
					$trans_charge = $transaction['trans_charge'];
					$student = new Student($connect, $admission_no);
                ?>
                <table width="600" border="1" align="left" cellpadding="10" cellspacing="0">
                    <tr>
                        <td colspan=2>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="80" align="center" valign="middle">
                                        <?php
                                        $vowels = array("/", "-");
                                        $picadmission_no = str_replace($vowels, "_", $admission_no);
                                        $myfilename = "../admin_online1/newimages/$picadmission_no.jpg";
                                        if (file_exists($myfilename))
                                            echo "<img src='$myfilename' valign=top align=right>";
                                        else
                                            echo "<img src='admin_images/default.jpg' width='90' height='100' valign=top align=right>";
                                        ?>
                                    </td>
                                    <td align="center">
                                        <span class="style1">ACADEMY</span><br>
                                    </td>
                                    <td width="80" align="center" valign="middle"><img src="../imgs/logo.png"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br><b><u>Payment Receipt</u><br><br></td>
                    </tr>
                    <tr>
                        <td colspan="2" nowrap="nowrap"><b>Fullname</b>:&nbsp;&nbsp;&nbsp;&nbsp;
                            <?php echo $_SESSION['myfullname']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Admission No.</b>&nbsp;&nbsp;&nbsp;<?php echo $_SESSION['admission_no']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" nowrap="nowrap">
                            <b>CURRENT
                                CLASS:</b>&nbsp;&nbsp;<?php echo $student->faculties_name . ' ' .$student->departments_name .$student->programme_name; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Transaction ID</b></td>
                        <td><b>Payment Details</b></td>
                    </tr>
                   
                    <tr valign=top>
                        <td>
                            <b>Transaction Number:</b> <?php echo $transaction["trans_no"] ?><br>
                            (<b>Reference Number</b>: <?php echo $transaction["reference_no"] ?>)<br>
                            (<b>Payment Class</b>: <?php echo $student->faculties_name . ' ' .$student->departments_name .$student->programme_name;  ?>)<br>
                            (<b>date</b>: <?php echo $transaction["trans_date"] ?>)<br>
                            (<b>Session - Term</b>: <?php echo $transaction["session_fullname"] . ' - ' . $transaction['term_fullname'] ?>)
                        </td>
                        <td>
                            <table width="300" border="1" align="left" cellpadding="5" cellspacing="0"
                                   style="border: 1px solid #000000;">
                                <tr>
                                    <td align="right" width=120><b>Item</b></td>
                                    <td align="right"><b>Amount (&#8358;)</b></td>
                                </tr>
        
                                <tr>
                                    <td align="right"><b>Transaction Amount</b></td>
                                    <td align="right">
                                        <b>&#8358;<?php echo number_format($total_amount-$trans_charge, 2) ?></b></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>Transaction Charge</b></td>
                                    <td align="right">
                                        <b>&#8358;<?php echo number_format($trans_charge, 2) ?></b></td>
                                </tr>
                                <tr>
                                    <td align="right"><b>TOTAL:</b></td>
                                    <td colspan=2 align="right">
                                        <b><u>&#8358;<?php echo number_format($total_amount, 2) ?></u></b></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <?php ?>
                    <tr valign=top>
                        <td align=right><b>Grand Total</b></td>
                        <td><b><u>&#8358;<?php echo number_format($total_amount, 2) ?></u></b></td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="bottom">
                            <div style="text-align: center">
                                <br/><br/>

                                <p>__________________________________</p>

                                <p>Authorized Signature / Date</p>
                            </div>

                        </td>
                    </tr>
                </table>
            <?php } ?>

            <br><br></td>
    </tr>
</table>
<br><br>
</td>
</tr>
</table>
<script>
    printWindow();
</script>
</body>
</html>