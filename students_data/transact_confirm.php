<?php
session_start();
include "config.php";
include("function_random.php");
include("../admin/functions.php");
include("../autoload.php");

$trans_no = "3061" . randomString(12);

if (!empty($_POST['payment_option']) and array_sum($_POST['amount']) > 0) {
    $payment_option = $_POST['payment_option'];
    $portal_charge = 1200;
    $trans_charge = ($payment_option == 'BranchCollect') ? 300 : 0;
    $student_id = $_SESSION['student_id'];
    $session_term_id = Terms::currentSessionTerm($connect);
    $bills_amount = array_sum($_POST['amount']);
    $trans_status = 'Pending';
    $exchange_rate = null;

    //if using int'l payment, compute the bills total in Naira to save in db
    if ($payment_option == 'MasterCard') {
        $rate = GetExchangeRate();
        $exchange_rate = $rate['rate'] ?: 1;
        $bills_amount *= $exchange_rate;
    }

    $total_amount = $bills_amount + $portal_charge + $trans_charge;

    //Log the total Transaction Amount
    $sql = "INSERT INTO transactions (session_term_id, trans_no, trans_amount, portal_charge, trans_charge, trans_status, student_id, trans_date, payment_option, exchange_rate)
            VALUES ($session_term_id, '$trans_no', '$total_amount', '$portal_charge', '$trans_charge', '$trans_status', $student_id, NOW(), '{$payment_option}', '{$exchange_rate}')";
    $result = mysql_query($sql) or die(mysql_error());
    $transaction_id = mysql_insert_id();
    
    //Log the transaction details, that is the individual items paid for in this transaction
    $sql = "INSERT INTO transaction_details (transaction_id, item_id, amount, added_on)";
    
    $values = array();
    foreach ($_POST['amount'] as $item_id => $amount) {
        if (!empty($amount)) {
            if ($payment_option == 'MasterCard') {
                $exchange_rate = $rate['rate'] ?: 1;
                $amount *= $exchange_rate;
            }
            $values[] = "({$transaction_id}, {$item_id}, '{$amount}', NOW())";
        }
    }

    $sql .= " VALUES " . implode(", ", $values);
    $result = mysql_query($sql) or die(mysql_error());

    header("Refresh: 1; URL=transact_confirm2.php?trxnID=$trans_no");
    exit;
    $das = date(iYs);
    ?>

    <script>
        window.open("<?php echo "transact_confirm2.php?trxnID=$trans_no" ?>", "<?php echo $das ?>", "width=800,height=700,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no");
    </script>
    <br>
    <center>
        If the next page did not load, that means Javascript is disabled on your browser or there is a pop up blocker,
        <br>please enable Javascript or disable pop up blocker or go and use a Javascript-enabled browser
    </center>

    <?php /* } */
} else
    header("Refresh: 1; URL=fees.php");

exit;
