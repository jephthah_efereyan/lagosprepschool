<?php
require("header_leftnav.inc.php");
include_once("config.php");
include("../admin/functions.php");
?>
<td valign="top" style="padding: 5px">
    <h3 class="page-title">Make Payment</h3>
    <table width="95%" class="t">
        <tr>
            <td>
                <?php
                $transaction = GetTransaction($_GET['trxnID']);
                if ($transaction) {
                    $post_url = $transaction['payment_option'] == 'BranchCollect' ? "paychoice_process.php" : "../vpc/migs-vpc.php";
                    $post_url .= "?trxnID={$_GET['trxnID']}";

                    $trans_amount = $transaction['trans_amount'];
                    $portal_charge = $transaction['portal_charge'];
                    $trans_charge = $transaction['trans_charge'];
                    $currency = '&#8358;';

                    if ($transaction['payment_option'] == 'MasterCard') {
                        $rate = GetExchangeRate();
                        $exchange_rate = $rate['rate'] ?: 1;

                        $trans_amount /= $exchange_rate;
                        $portal_charge /= $exchange_rate;
                        $currency = '$';
                    }
                    ?>
                    <h4>Please confirm the amount you are paying.</h4>
                    <form name="form" action="<?php echo $post_url ?>" method="post">
                        <table width="500px" class="t">
                            <tr>
                                <th>FEE ITEM DESCRIPTION</th>
                                <th class="align-right">AMOUNT (<?php print $currency ?>)</th>
                            </tr>

                            <tr>
                                <th>Bills Total Amount</th>
                                <th class="align-right"><?php echo number_format($trans_amount - $portal_charge - $trans_charge, 2); ?></th>
                            </tr>
                            <tr>
                                <th>Portal Access Fee</th>
                                <th class="align-right"><?php echo number_format($portal_charge + $trans_charge, 2); ?></th>
                            </tr>
                            <tr>
                                <th>TOTAL</th>
                                <th class="align-right"><?php echo number_format($trans_amount, 2); ?></th>
                            </tr>
                        </table>
                        <div style="margin: 10px 0; text-align: center">
                            <input type="submit" name="Submit" value="PAY" style="padding: 10px 20px">
                        </div>
                    </form>
                <?php } ?>
            </td>
        </tr>
    </table>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
</body>
</html>
