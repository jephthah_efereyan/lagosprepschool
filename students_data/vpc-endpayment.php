<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 2/29/2016
 * Time: 6:46 PM
 */

//@session_start();
require("header_leftnav.inc.php");
include_once("config.php");
$vpc_id = $_GET['vpc_id'];

$sql = "SELECT vpc.*, t.trans_no FROM vpc_transactions vpc JOIN transactions t ON vpc.trans_id = t.id WHERE vpc.id = {$vpc_id}";
$result = mysql_query($sql);
$transaction = mysql_fetch_assoc($result);
?>
<td valign="top" style="padding: 5px">
    <h3 class="page-title">VPC Payment Receipt</h3>
    <?php
    if ($transaction['vpc_txnresponsecode'] == '0') {
        $msg = '<p>Your payment was SUCCESSFUL.</p>
                <p>Note the following details for Reference Purposes</p>
                <p>Transaction ID: ' . $transaction['trans_no'] . '</p>
                <p>Receipt Number: ' . $transaction['vpc_receiptno'] . '</p>
                <p><a href="print-receipt.php?trans_no=' . $transaction['trans_no'] .'" target="_blank">Print Receipt</a></p>';
    } else {
        $msg = '<p>Your payment was NOT SUCCESSFUL.</p>
                <p>Reason: ' . $transaction['vpc_message'] . '</p>
                <p>Please <a href="fees.php">click here</a> to attempt the payment again.</p>';
    }

    echo $msg;
    print mysql_error();
    ?>
</td>
</tr>
</table>
<?php require("footer.php"); ?>
</body>
</html>
