<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 2/29/2016
 * Time: 12:59 PM
 */

session_start();
require "../students_data/config.php";
require 'vpc-constants.php';
require "../admin/functions.php";
require "../students_data/function_random.php";

$trxnID = $_GET['trxnID'];

$transaction = GetTransaction($trxnID);
$trans_id = $transaction['id'];
$amount = $transaction['trans_amount'];

$x_rate = GetExchangeRate();
$rate	= $x_rate['rate'];
$total_amount = ($amount / (int)$rate) * 100; //in lowest currency

//Prepare the request
$return_url =  "http://lagosprepschool.com/vpc/pay-response.php"; // "http://localhost/lagosprepschool/vpc/pay-response.php";

$vpc_AccessCode = VPC_ACCESSCODE;
$vpc_Amount = round($total_amount);
$vpc_Command = 'pay';
$vpc_Currency = 'USD';
$vpc_Locale = VPC_LOCALE;
$vpc_MerchTxnRef = $trxnID . randomString(6);
$vpc_Merchant = VPC_MERCHANTID;
$vpc_OrderInfo = $trxnID . randomString(4);
$vpc_ReturnURL = $return_url;
$vpc_SecureHashType = 'SHA256';
$vpc_Version = '1';

$data = array(
    'vpc_AccessCode' => $vpc_AccessCode,
    'vpc_Amount' => $vpc_Amount,
    'vpc_Command' => $vpc_Command,
    'vpc_Currency' => $vpc_Currency,
    'vpc_Locale' => $vpc_Locale,
    'vpc_MerchTxnRef' => $vpc_MerchTxnRef,
    'vpc_Merchant' => $vpc_Merchant,
    'vpc_OrderInfo' => $vpc_OrderInfo,
    'vpc_ReturnURL' => $vpc_ReturnURL,
    'vpc_Version' => $vpc_Version,
);

$data_to_post = "";
foreach ($data as $key => $value)
    $data_to_post .= $key . '=' . $value . '&';
$data_to_post = rtrim($data_to_post, '&');

$hex_decoded_secret_key = pack('H*', VPC_SECUREHASHSECRET);
$vpc_SecureHash = hash_hmac('SHA256', $data_to_post, $hex_decoded_secret_key);

$data_to_post .= "&vpc_SecureHash={$vpc_SecureHash}&vpc_SecureHashType={$vpc_SecureHashType}";

//Log the request
$sql = "INSERT INTO vpc_transactions SET trans_id = {$trans_id}, vpc_request = '{$data_to_post}', vpc_orderinfo = '{$vpc_OrderInfo}', exchange_rate = '{$rate}', added_on = NOW()";
$result = mysql_query($sql) or die(mysql_error());

$post_to = VPC_GATEWAYURL . '?' . $data_to_post;
header("Location:" . $post_to);
die('END');