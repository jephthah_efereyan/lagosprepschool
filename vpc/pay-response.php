<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 2/29/2016
 * Time: 3:36 PM
 */
session_start();
require "../students_data/config.php";

$vpc_Command = $_GET['vpc_Command'];
$vpc_MerchTxnRef = !empty($_GET['vpc_MerchTxnRef']) ? $_GET['vpc_MerchTxnRef'] : '';
$vpc_Merchant = $_GET['vpc_Merchant'];
$vpc_OrderInfo = $_GET['vpc_OrderInfo'];
$vpc_Amount = $_GET['vpc_Amount'];
$vpc_Currency = $_GET['vpc_Currency'];
$vpc_Message = $_GET['vpc_Message'];
$vpc_TxnResponseCode = $_GET['vpc_TxnResponseCode'];
$vpc_ReceiptNo = !empty($_GET['vpc_ReceiptNo']) ? $_GET['vpc_ReceiptNo'] : '';
$vpc_AcqResponseCode = !empty($_GET['vpc_AcqResponseCode']) ? $_GET['vpc_AcqResponseCode'] : '';
$vpc_TransactionNo = !empty($_GET['vpc_TransactionNo']) ? $_GET['vpc_TransactionNo'] : '';
$vpc_BatchNo = !empty($_GET['vpc_BatchNo']) ? $_GET['vpc_BatchNo'] : '';
$vpc_AuthorizeId = !empty($_GET['vpc_AuthorizeId']) ? $_GET['vpc_AuthorizeId'] : '';
$vpc_Card = !empty($_GET['vpc_Card']) ? $_GET['vpc_Card'] : '';
$vpc_SecureHash = $_GET['vpc_SecureHash'];
$vpc_SecureHashType = $_GET['vpc_SecureHashType'];
$vpc_CardNum = !empty($_GET['vpc_CardNum']) ? $_GET['vpc_CardNum'] : NULL;

$response_string = "";
foreach ($_GET as $key => $value)
    $response_string .= $key . '=' . $value . '&';
$response_string = rtrim($response_string, '&');

$data = array(
    "vpc_response = '{$response_string}'",
    "vpc_command = '{$vpc_Command}'",
    "vpc_merchtxnref = '{$vpc_MerchTxnRef}'",
    "vpc_merchant = '{$vpc_Merchant}'",
    "vpc_amount = '{$vpc_Amount}'",
    "vpc_currency = '{$vpc_Currency}'",
    "vpc_message = '{$vpc_Message}'",
    "vpc_txnresponsecode = '{$vpc_TxnResponseCode}'",
    "vpc_receiptno = '{$vpc_ReceiptNo}'",
    "vpc_acqresponsecode = '{$vpc_AcqResponseCode}'",
    "vpc_transactionno = '{$vpc_TransactionNo}'",
    "vpc_batchno = '{$vpc_BatchNo}'",
    "vpc_authorizeid = '{$vpc_AuthorizeId}'",
    "vpc_card = '{$vpc_Card}'",
    "vpc_securehash = '{$vpc_SecureHash}'",
    "vpc_securehashtype = '{$vpc_SecureHashType}'",
    "vpc_CardNum = '{$vpc_CardNum}'",
);


//Update the vpc transaction
$sql = "UPDATE vpc_transactions SET " . implode(", ", $data) . " WHERE vpc_orderinfo = '{$vpc_OrderInfo}'";
mysql_query($sql) or die(mysql_error());

//Fetch the trans_id and update our transaction table
$sql = "SELECT trans_id, id FROM vpc_transactions WHERE vpc_orderinfo = '{$vpc_OrderInfo}'";
$result = mysql_query($sql) or die(mysql_error());
$trans_id = mysql_result($result, 0, 0);
$vpc_id = mysql_result($result, 0, 1);

if ($vpc_TxnResponseCode == '0') {
    $sql = "UPDATE transactions SET trans_status = 'Paid' WHERE id = {$trans_id}";
    mysql_query($sql) or die(mysql_error());
}

header("Location: ../students_data/vpc-endpayment.php?vpc_id=$vpc_id");
die(print_r($_GET,1));
