<?php
/**
 * Created by PhpStorm.
 * User: jephthah.efereyan
 * Date: 2/29/2016
 * Time: 4:15 PM
 */

# TEST CONFIG PARAMS
/*define("MERCHANTID", "2547916");
define("SERVICETYPEID", "4430731");
define("APIKEY", "1946");
define("GATEWAYURL", "http://www.remitademo.net/remita/ecomm/v2/init.reg");
define("GATEWAYRRRPAYMENTURL", "http://www.remitademo.net/remita/ecomm/finalize.reg");
define("CHECKSTATUSURL", "http://www.remitademo.net/remita/ecomm");
define("PATH", 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));*/

# LIVE CONFIG PARAMS
define("VPC_MERCHANTID", "3214LAFCMB1M");
define("VPC_MERCHANTNAME", "LAGOS PREPARATORY SCHOOL IKOYI, LAGOS");
define("VPC_ACCESSCODE", "D77455B5");
define("VPC_LOCALE", "English (Nigeria)");
define("VPC_SECUREHASHSECRET", "1A2F2AB15025EC62DD66B13E75657913");
define("VPC_GATEWAYURL", "https://migs.mastercard.com.au/vpcpay");